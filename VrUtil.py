import datetime
import hashlib
import os
import sys

#=======================================================================================================================

ModuleName          = "VrUtil"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 0

#=======================================================================================================================

def absPath(path) :
    """Return the absolute path of a path.
    """
    return os.path.abspath(path)

#----------------------------------------------------------

def errorBeep(message) :
    """Write a message to sys.stderr and beep if it is the console.
    """
    print("{beep:s}{message:s}".format(beep = "\a" if sys.stdout.isatty() else "",
                                        message = message,
                                        file = sys.stderr,
                                        end = ''))

#----------------------------------------------------------

def errorMessage(message) :
    """Write a message to sys.stderr.
    """
    print(message, file = sys.stderr, end = '')

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return makeModuleInformation(ModuleName, __file__, ModuleVersionMajor, ModuleVersionMinor, ModuleVersionMicro)

#----------------------------------------------------------

def hashOf(path) :
    """Return hash of a file.
    """
    with open(path, 'rb') as file :
        hash = hashlib.md5(file.read()).hexdigest()
    return hash[0 : 8] + hash[-9: -1]

#----------------------------------------------------------

def makeModuleInformation(name, file, major, minor, micro) :
    """Return the module information.
    """
    path = os.path.abspath(file)
    return {"Name"          : name,
            "FilePath"      : path,
            "FileHash"      : hashOf(path),
            "LastModified"  : utcOf(path),
            "VersionMajor"  : major,
            "VersionMinor"  : minor,
            "VersionMicro"  : micro}

#----------------------------------------------------------

def message(message) :
    """Write a message to sys.stdout.
    """
    print(message, file = sys.stdout, end = '')

#----------------------------------------------------------

def messageBeep(message) :
    """Write a message to sys.stdout and beep if it is the console.
    """
    print("{beep:s}{message:s}".format(beep = "\a" if sys.stdout.isatty() else "",
                                        message = message,
                                        file = sys.stdout,
                                        end = ''))

#----------------------------------------------------------

def utcNow() :
    """Get the current UTC time.
    """
    return datetime.datetime.utcnow()

#----------------------------------------------------------

def utcNowStr(length = 19) :
    """Get the current UTC time as a string.
    """
    return utcNow().isoformat(sep = ' ')[0 : length]

#----------------------------------------------------------

def utcOf(path, length = 16) :
    """Return the UTC last modification time of a path as a string.
    """
    return datetime.datetime.utcfromtimestamp(os.path.getmtime(path)).isoformat(sep = ' ')[0 : length]
