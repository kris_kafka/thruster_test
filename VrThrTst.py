import math
import Vr
import VrThr
import VrUtil
from VrTst import *

#=======================================================================================================================

SubModuleName           = "VrThrTst"

# TODO: Update version number.
SubModuleVersionMajor   = 1
SubModuleVersionMinor   = 5
SubModuleVersionMicro   = 5

#=======================================================================================================================

# Gizmo name
GizmoNamePluralCap      = "Thrusters"
GizmoNamePluralLwr      = "thrusters"
GizmoNameSingularCap    = "Thruster"
GizmoNameSingularLwr    = "thruster"

#=======================================================================================================================

def findGizmos(appInf, port) :
    """Find all of the thruster gizmos.
    """
    return VrGizmo.findGizmos(port,
                                Vr.Devices.Thruster,
                                VrThr.Gizmo.getMotorId,
                                lambda gizmo, port, appInf = appInf : \
                                            Gizmo(appInf,
                                                    nodeId = gizmo.nodeId,
                                                    groupId = gizmo.groupId,
                                                    serialNumber = gizmo.serialNumber,
                                                    port = port))

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(SubModuleName,
                                        __file__,
                                        SubModuleVersionMajor,
                                        SubModuleVersionMinor,
                                        SubModuleVersionMicro)

#=======================================================================================================================

class Gizmo(VrThr.Gizmo, VrGizmo) :
    """VideoRay test thruster gizmo information and functions.
    """
    ParseSize       = VrThr.ReplyPropulsion.ParseSize
                        # RPM, Voltage, Current, Temp, Faults
    Initialized     = False
    FaultCauses     = [
                            "RPM",
                            "Voltage",
                            "Current",
                            "Temperature",
                            "Faults",
                        ]

    #----------------------------------------------------------

    def __init__(self, appInf, nodeId = -1, groupId = -1, serialNumber = "", gizmoId = -1, port = None) :
        """Initialize a thruster test gizmo object.

        Arguments:
            appInf         Application information.
            nodeId          Optional node ID.
            groupId         Optional group ID
            serialNumber    Optional serial number.
            gizmoId         Optional LED ID.
            port            Optional serial port associated with this device.
        """
        self._initPollSubrs()
        #
        VrThr.Gizmo.__init__(self, nodeId, groupId, serialNumber, gizmoId)
        VrGizmo.__init__(self, appInf, port)
        #
        self.errorCounts    = 5 * [0]

    #----------------------------------------------------------

    def _initMinMax(self, test) :
        """Set the min/max valid status values for a given test.

        Arguments:
            test    Test data.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        #
        self.minValid = [
                            test[gzmCfg.ValidRpmMinimum],
                            test[gzmCfg.ValidVoltageMinimum],
                            test[gzmCfg.ValidCurrentMinimum],
                            test[gzmCfg.ValidTempMinimum],
                            test[gzmCfg.ValidFaultsErrorMask],
                        ]
        self.maxValid = [
                            test[gzmCfg.ValidRpmMaximum],
                            test[gzmCfg.ValidVoltageMaximum],
                            test[gzmCfg.ValidCurrentMaximum],
                            test[gzmCfg.ValidTempMaximum],
                            test[gzmCfg.ValidFaultsOkayValue],
                        ]

    #----------------------------------------------------------

    def _initPid(self, test) :
        """Initialize the PID for a given test.

        Arguments:
            test    Test data.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        #
        self.percentage = test[gzmCfg.Percentage]
        pid = appInf.VrPidModule.VrPid(self.percentage, str(self.nodeId))
        if test[gzmCfg.SeekToTarget] :
            pid.set(enabled = True,
                    dt = test[gzmCfg.PidDt],
                    integralMaximum = test[gzmCfg.PidIntegralMax],
                    integralMinimum = test[gzmCfg.PidIntegralMin],
                    kp = test[gzmCfg.PidKp],
                    ki = test[gzmCfg.PidKi],
                    kd = test[gzmCfg.PidKd],
                    controlMaximum = test[gzmCfg.PidControlMax],
                    controlMinimum = test[gzmCfg.PidControlMin],
                    scale = test[gzmCfg.PidScale],
                    startupDelay = test[gzmCfg.PidStartupDelay],
                    showDebug = test[gzmCfg.PidShowDebug])
            if test[gzmCfg.Target] is not None :
                pid.set(target = test[gzmCfg.Target])
            if test[gzmCfg.TargetLower] is not None and test[gzmCfg.TargetUpper] is not None :
                pid.set(targetLower = test[gzmCfg.TargetLower], targetUpper = test[gzmCfg.TargetUpper])
            errors = pid.start()
            if errors :
                VrUtil.errorBeep("PID error(s):\n" + "\n".join(errors))
                exit()
        return pid

    #----------------------------------------------------------
    @classmethod
    def _initPollSubrs(cls) :
        """Initialize the poll() subrs.
        """
        if cls.Initialized :
            return
        #
        chkAbsFp    = [lambda t : (not math.isnan(t[0])) and t[1] <= abs(t[0]) and abs(t[0]) <= t[2]]
        chkFloat    = [lambda t : (not math.isnan(t[0])) and t[1] <= t[0] and t[0] <= t[2]]
        chkUint     = [lambda t : t[2] == t[0] & t[1]]
        maxFloat    = [lambda t : max(t) if not math.isnan(t[1]) else t[0]]
        maxUint     = [any]
        minFloat    = [lambda t : min(t) if not math.isnan(t[1]) else t[0]]
        minUint     = [all]
        addFloat    = [lambda t : sum(t) if not math.isnan(t[1]) else t[0]]
        addUint     = [any]
        pckFloat    = [lambda t : t[1] if t[0] else t[2]]
        pckUint     = pckFloat
        #                       RPM         Voltage     Current     Temp        Faults
        cls.CheckSubrs      =   chkAbsFp +  chkAbsFp +  chkAbsFp +  chkFloat +  chkUint
        cls.MaximumSubrs    =   maxFloat +  maxFloat +  maxFloat +  maxFloat +  maxUint
        cls.MinimumSubrs    =   minFloat +  minFloat +  minFloat +  minFloat +  minUint
        cls.AdditionSubrs   =   addFloat +  addFloat +  addFloat +  addFloat +  addUint
        cls.PickSubrs       =   pckFloat +  pckFloat +  pckFloat +  pckFloat +  pckUint
        #
        cls.Initialized = True

    #----------------------------------------------------------

    def initializeForTest(self, test) :
        """Initialize for a given test.

        Arguments:
            test    Test data.
        """
        self.pid = self._initPid(test)
        self._initMinMax(test)

    #----------------------------------------------------------

    def poll(self, isStartup, testNumber, test) :
        """Poll a thruster.

        Arguments:
            isStartup   True during test startup time period.
            testNumber  Number [1, ...] of the test.
            test        Test info.
        """
        if self.isFault :
            return
        #
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmLog = appInf.GizmoLogModule
        #
        now = VrUtil.utcNow()
        self.percentage = self.pid.get()
        reply = self.propulsion(self.port, self.percentage)
        #
        if reply :
            current = reply.parse()
            if current[VrThr.ReplyPropulsion.ReplyFaults] :
                self.updateErrorCounts()
            gzmLog.Log(appInf) \
                .recordDetailFor(self, now, isStartup, current, self.errorCounts, testNumber, test)
        if isStartup :
            return
        if reply is not None :
            self.pollCount      += 1
            self.totalPollCount += 1
            self.lastReplyTime  = now
            okay                = VrGizmo._applyAll(self.CheckSubrs, current, self.minValid, self.maxValid)
            self.lastOkTime     = VrGizmo._applyAll(self.PickSubrs,
                                                    okay,
                                                    VrThr.ReplyPropulsion.ParseSize * [now],
                                                    self.lastOkTime)
            self.okayTest       = VrGizmo._applyOne(sum, self.okayTest, okay)
            #
            okay                = [test[gzmCfg.KeepOutOfRange] or ok for ok in okay]
            self.cntTest        = VrGizmo._applyOne(sum, self.cntTest, okay)
            self.cntInterval    = VrGizmo._applyOne(sum, self.cntInterval, okay)
            self.sumInterval    = VrGizmo._applyIf(self.AdditionSubrs, okay, self.sumInterval, current)
            self.sumTest        = VrGizmo._applyIf(self.AdditionSubrs, okay, self.sumTest, current)
            self.minimum        = VrGizmo._applyChk(self.MinimumSubrs, okay, self.minimum, current)
            self.maximum        = VrGizmo._applyChk(self.MaximumSubrs, okay, self.maximum, current)
            self.failureCause   = ", ".join([cause for lastOk, cause in zip(self.lastOkTime, self.FaultCauses)
                                                    if (now - lastOk).total_seconds() > test[gzmCfg.FaultPeriod]])
            if okay[reply.ReplyRpm] :
                self.pid.update(current[reply.ReplyRpm], now)
        elif (now - self.lastReplyTime).total_seconds() > test[gzmCfg.FaultPeriod] :
            self.failureCause = "Not responding"
        #
        if self.failureCause :
            self.stopGizmo()
            self.isFault        = True
            self.failureTime    = now
            VrUtil.errorBeep(self.GizmoFailure \
                                    .format(serialNumber = self.serialNumber,
                                            nodeId = self.nodeId,
                                            groupId = self.groupId,
                                            gizmoId = self.gizmoId,
                                            cause = self.failureCause))

    #----------------------------------------------------------

    def stopGizmo(self) :
        """Stop the gizmo.
        """
        if self.isFault :
            return
        self.stopMotor(self.port)
