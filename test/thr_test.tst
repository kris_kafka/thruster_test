TestRegime            '1.5.2 Test'

#### Configuration values

CsvDestTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}_final.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest        no
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex        no
LogCsrHexAtEnd       no
LogCsrHexAtStart     no
LogDestTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}_final.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.log'
CsvMoveToDest        no
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
ResetGizmos          no
ResetPause            5.000
ShowConfiguration    no
ShowNonDefault      yes
ShowTests            no
SummaryLogFile      '~/QA_LOGS/70503-Thruster/!thr_summary.txt'
SummaryLogUpdate    yes

#### Default test control values:

Duration                 5
FaultPeriod              3.0
Interval                 0.25
KeepOutOfRange         yes
PauseAfterTest           1.0
PollInterval             0.0
StopAfterTest          yes
TestStartupTime          0.5

LogAverages            yes
LogBadCounts           yes
LogFirstPoll           yes
LogMaximums            yes
LogMinimums            yes

ValidCurrentMaximum      1.000
ValidCurrentMinimum      0.000
ValidFaultsErrorMask  0x00
ValidFaultsOkayValue  0x00
ValidRpmMaximum       2000.0
ValidRpmMinimum          0.0
ValidTempMaximum        75.0
ValidTempMinimum         5.0
ValidVoltageMaximum     50.0
ValidVoltageMinimum     45.0

SetDefaults

#### QA Tests

test
duration =     1
percentage =   0.0
end

test
percentage =  10.0
end

test
percentage = -10.0
end
