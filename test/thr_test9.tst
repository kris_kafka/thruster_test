#### Test control values defaults:

Duration              3600
Interval                60
PauseAfterTest           1.0
StopAfterTest          yes
TestStartupTime          0.500
FaultPeriod              3.000
KeepOutOfRange         yes
PollInterval             0.000

LogAverages            yes
LogBadCounts           yes
LogFirstPoll            no
LogMaximums            yes
LogMinimums            yes

ValidFaultsErrorMask  0xff
ValidFaultsOkayValue  0x00
ValidCurrentMinimum      0.010
ValidCurrentMaximum      1.000
ValidRpmMinimum          0.0
ValidRpmMaximum       2000.0
ValidTempMinimum        10.0
ValidTempMaximum        55.0
ValidVoltageMinimum     45.0
ValidVoltageMaximum     50.0

Duration                 2
Interval                 0.25

SetDefaults

#### QA Tests

test
duration =     1
percentage =   0.0
end

test
percentage =  10.0
end

test
percentage = -10.0
end

#### Configuration values

CsvDestTemplate     '~/out/thr_{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     'logs/thr_{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest       yes
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex        no
LogCsrHexAtEnd       no
LogCsrHexAtStart     no
LogDestTemplate     '~/out/thr_{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     'logs/thr_{serialNumber:s}_{startTime:s}.log'
LogModuleVersions   yes
LogMoveToDest       yes
LogPollCounts       yes
LogRepeatInfoAtEnd   1
PauseAfterLastTest   no
PortName            ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration    no
ShowTests            no
SummaryLogFile      '~/out/thr_summary.txt'
SummaryLogUpdate    yes
TestRegime          '1.5.2 Test 9'
UserName            ''
