######## Test control values defaults:

Duration              3600
Interval                60.0
PauseAfterTest           1.0
StopAfterTest          yes
TestStartupTime          0.500
FaultPeriod              3.000
KeepOutOfRange         yes
PollInterval             0.000

LogAverages            yes
LogBadCounts           yes
LogFirstPoll            no
LogMaximums            yes
LogMinimums            yes

ValidFaultsErrorMask  0xff
ValidFaultsOkayValue  0x00
ValidCurrentMaximum      1.000
ValidCurrentMinimum      0.010
ValidSetpointMaximum   100.0
ValidSetpointMinimum     0.0
ValidTempMaximum        65.0
ValidTempMinimum        10.0
ValidVoltageMaximum     50.0
ValidVoltageMinimum     45.0

SetDefaults

#### Configuration values.

CsvDest                 "~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.csv"     # Minimal form
CsvDestTemplate         "~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.csv"
CsvError                False                                           # Minimal form
CsvErrorCounts          True
CsvGenerate             True
CsvMake                 "~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.csv"    # Minimal form
CsvMakeTemplate         "~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.csv"
CsvMove                 no                                              # Minimal form
CsvMoveToDest           yes
LogCsrDataAtEnd         True
LogCsrDataAtStart       True
LogCsrDataHex           False
LogCsrHexAtEnd          True
LogCsrHexAtStart        True
LogDest                 "~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.log"     # Minimal form
LogDestTemplate         "~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.log"
LogDetails              False                                           # Minimal form
LogError                False                                           # Minimal form
LogErrorCounts          True
LogGenerate             True
LogMake                 "~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.log"    # Minimal form
LogMakeTemplate         "~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.log"
LogModuleVer            False                                           # Minimal form
LogModuleVersions       True
LogMove                 no                                              # Minimal form
LogMoveToDest           yes
LogPoll                 False                                           # Minimal form
LogPollCounts           True
LogRepeat               False                                           # Minimal form
LogRepeatInfoAtEnd      True
PauseAfterLastTest      True
Port                    'com1'                                          # Minimal form
PortName                'com4'
ResetGizmos             False
ResetPause              3
ShowConfig              False
ShowConfiguration       True
ShowTests               True
SummaryLogFile          '~/Projects/ttt/out/led_summary.txt'
SummaryLogUpdate        yes
TestRegime              ''
User                    'VideoRay'                                      # Minimal form
UserName                'VideoRay'

#### Test control values.

Set1                    3                                               # Minimal form
Setpoint1               4
Set2                    5                                               # Minimal form
Setpoint2               6
Set3                    7                                               # Minimal form
Setpoint3               8
Dur                     2                                               # Minimal form
Duration                3
Int                     2.0                                             # Minimal form
Interval                1.0
PauseAfterTest          0.5
StopAfterTest           no
FaultPeriod             2.5
KeepOutOfRange          yes
LogAve                  False                                           # Minimal form
LogAverages             True
LogBad                  False                                           # Minimal form
LogBadCounts            True
LogFirst                False                                           # Minimal form
LogFirstPoll            True
LogMax                  False                                           # Minimal form
LogMaximums             True
LogMin                  False                                           # Minimal form
LogMinimums             True
PollInt                 0.25                                            # Minimal form
PollInterval            0.5
TestStartup             0.5                                             # Minimal form
TestStartupTime         0.75
ValidCurrentMax         2.1                                             # Minimal form
ValidCurrentMaximum     2.2
ValidCurrentMin         -.010                                           # Minimal form
ValidCurrentMinimum     -0.2
ValidFaultsError        0x12                                            # Minimal form
ValidFaultsErrorMask    0xff
ValidFaultsOkay         0x34                                            # Minimal form
ValidFaultsOkayValue    0x00
ValidSetpointMax        123.4                                           # Minimal form
ValidSetpointMaximum    100.0
ValidSetpointMin        -123.4                                          # Minimal form
ValidSetpointMinimum    0.0
ValidTempMax            123.4                                           # Minimal form
ValidTempMaximum        125.0
ValidTempMin            456.7                                           # Minimal form
ValidTempMinimum        -10.0
ValidVoltageMax         34.5                                            # Minimal form
ValidVoltageMaximum     51.2
ValidVoltageMin         67.8                                            # Minimal form
ValidVoltageMinimum     41.2

Setpoint                1
test dur = 1| end

test    set1 = 10.0     set2 = 40.0     set3 = 70.0     dur = 2         int = 1 pause = 1       end     ; Test 1
test    set1 = 70.0,    set2 = 40.0,    set3 = 10.0,    dur = 2,        int = 1                 end     ; Test 2

setpoint = 10
test #3
        set3=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=0
        end

setpoint=20
test #4
        set2=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=0
        end

setpoint = 30
test #5
        set1=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=n
        end

setpoint = 40
test #6
        set3=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=n
        end

setpoint=50
test #7
        set2=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=false
        end

setpoint=60
test #8
        set1=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=false
        end

setpoint=70
test #9
        set3=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=No
        end

setpoint=80
test #10
        set2=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=No
        end

setpoint=90
test#11
        set1=0,
        dur=1,
        int=1,
        pause=0
        stopAfter=F
        end

setpoint=100
test#13
        dur=1,
        int=1,
        pause=0,
        stopAfter=F
        end

setpoint=10
test #14
        dur=1,int=1,pause=3
        end

ShowTests=1
ShowConfiguration = 1
TestRegime '1.5.2 Test #2'
