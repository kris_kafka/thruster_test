#### Test control values defaults:

Duration                3600
Interval                  60
PauseAfterTest             1.0
StopAfterTest            yes
TestStartupTime            0.500
FaultPeriod                3.000
KeepOutOfRange           yes
PollInterval               0.000

LogAverages              yes
LogBadCounts             yes
LogFirstPoll              no
LogMaximums              yes
LogMinimums              yes

ValidFaultsErrorMask    0xff
ValidFaultsOkayValue    0x00
ValidCurrentMinimum        0.010
ValidCurrentMaximum        1.000
ValidRpmMinimum         -250.0
ValidRpmMaximum          250.0
ValidTempMinimum          10.0
ValidTempMaximum          60.0
ValidVoltageMinimum       45.0
ValidVoltageMaximum       50.0

Duration                   5
Interval                   0.3

PidControlMin            -15.0
PidControlMax             15.0
PidDt                      0.250
PidKp                      1.0
PidKi                      0.0
PidKd                      0.0
PidScale                   0.01
SeekToTarget              no
PidStartupDelay            0.000
PidShowDebug              no

SetDefaults

#### QA Tests

test
percentage      3.1
SeekToTarget  yes
Target        125.0
TargetLower   120.0
TargetUpper   130.0
end

test
percentage     -3.1
SeekToTarget  yes
Target       -125.0
TargetLower  -130.0
TargetUpper  -120.0
end

#### Configuration values

CsvDestTemplate     '~/Projects/ttt/out/thr_{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/Projects/ttt/logs/thr_{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest       yes
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex        no
LogCsrHexAtEnd       no
LogCsrHexAtStart     no
LogDestTemplate     '~/Projects/ttt/out/thr_{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/Projects/ttt/logs/thr_{serialNumber:s}_{startTime:s}.log'
LogModuleVersions   yes
LogMoveToDest       yes
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
PortName            ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration     no
ShowTests             no
SummaryLogFile      '~/Projects/ttt/out/thr_summary.txt'
SummaryLogUpdate    yes
TestRegime          '1.5.5 Test 2'
UserName            ''
