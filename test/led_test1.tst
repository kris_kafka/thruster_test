#### Default test control values:

Duration              3600
Interval                60.0
PauseAfterTest           1.0
StopAfterTest          yes
TestStartupTime          0.500
FaultPeriod              3.000
KeepOutOfRange         yes
PollInterval             0.000

LogAverages            yes
LogBadCounts           yes
LogMaximums            yes
LogMinimums            yes
LogFirstPoll            no

ValidFaultsErrorMask  0xff
ValidFaultsOkayValue  0x00
ValidCurrentMaximum      1.000
ValidCurrentMinimum      0.010
ValidSetpointMaximum   100.0
ValidSetpointMinimum     0.0
ValidTempMaximum        55.0
ValidTempMinimum        10.0
ValidVoltageMaximum     50.0
ValidVoltageMinimum     45.0

duration                 2
interval                 0.25
PauseAfterTest           0.0
StopAfterTest           no
PollInterval             0.25
ValidCurrentMaximum      0.500
ValidCurrentMinimum      0.050
ValidSetpointMinimum     0.0
ValidSetpointMaximum   100.0

SetDefaults

#### QA Tests

test
Setpoint                 0.0
ValidSetpointMinimum     0.0
ValidSetpointMaximum     0.1
end

test
Setpoint                10.0
ValidSetpointMinimum     9.9
ValidSetpointMaximum    10.1
end

test
Setpoint                20.0
ValidSetpointMinimum    19.9
ValidSetpointMaximum    20.1
end

#test
#Setpoint                30.0
#ValidSetpointMinimum    29.9
#ValidSetpointMaximum    30.1
#end
#
#test
#Setpoint                40.0
#ValidSetpointMinimum    39.9
#ValidSetpointMaximum    40.1
#end
#
#test
#Setpoint                50.0
#ValidSetpointMinimum    49.9
#ValidSetpointMaximum    50.1
#end
#
#test
#Setpoint                60.0
#ValidSetpointMinimum    59.9
#ValidSetpointMaximum    60.1
#end
#
#test
#Setpoint                70.0
#ValidSetpointMinimum    69.9
#ValidSetpointMaximum    70.1
#end
#
#test
#Setpoint                80.0
#end
#
#test
#Setpoint                90.0
#end
#
#test
#Setpoint               100.0
#end

#### Configuration values

CsvDestTemplate     '~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest       yes
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex         1
LogCsrHexAtEnd       no
LogCsrHexAtStart      1
LogDestTemplate     '~/Projects/ttt/out/led_{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/Projects/ttt/logs/led_{serialNumber:s}_{startTime:s}.log'
LogModuleVersions   yes
LogMoveToDest       yes
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
PortName            ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration   YES
ShowTests           YES
SummaryLogFile      '~/Projects/ttt/out/led_summary.txt'
SummaryLogUpdate    yes
TestRegime          '1.5.5 Test 1'
UserName            ''
