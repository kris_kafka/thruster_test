TestRegime          '1.5.5 Burn-inCSV'

#### Configuration values

CsvDestTemplate         '/mnt/production/QA_LOGS/70503-Thruster/Burn-in/{serialNumber:s}_{startTime:s}.csv_burn_in'
CsvErrorCounts          yes
CsvGenerate             yes
CsvMakeTemplate         '~/QA_LOGS/70503-Thruster/Burn-in/{serialNumber:s}_{startTime:s}.csv_burn_in'
CsvMoveToDest           yes
LogCsrDataAtEnd          no
LogCsrDataAtStart       yes
LogCsrDataHex           yes
LogCsrHexAtEnd           no
LogCsrHexAtStart         no
LogDestTemplate         '/mnt/production/QA_LOGS/70503-Thruster/Burn-in/{serialNumber:s}_{startTime:s}.log_burn_in'
LogErrorCounts           no
LogGenerate             yes
LogMakeTemplate         '~/QA_LOGS/70503-Thruster/Burn-in/{serialNumber:s}_{startTime:s}.log_burn_in'
LogMoveToDest            no
LogPollCounts           yes
LogRepeatInfoAtEnd       no
PauseAfterLastTest       no
ResetGizmos              no
ResetPause                5.000
ShowConfiguration        no
ShowNonDefault          yes
ShowTests                no
SummaryLogFile          '/mnt/production/QA_LOGS/70503-Thruster/Burn-in/!thr_burn_in.txt'
SummaryLogUpdate        yes

#### Test control values defaults:

Duration                  10
Interval                   0.5
PauseAfterTest             1.0
StopAfterTest            yes
TestStartupTime            0.500
FaultPeriod                3.000
KeepOutOfRange           yes
PollInterval               0.000

LogAverages             yes
LogBadCounts            yes
LogFirstPoll            yes
LogMaximums             yes
LogMinimums             yes

ValidFaultsErrorMask    0x00
ValidFaultsOkayValue    0x00
ValidCurrentMinimum        0.000
ValidCurrentMaximum        0.500
ValidRpmMinimum           75.0
ValidRpmMaximum          300.0
ValidTempMinimum          5.0
ValidTempMaximum          75.0
ValidVoltageMinimum       35.0
ValidVoltageMaximum       50.0

PidControlMin           -15.0
PidControlMax            15.0
PidDt                     0.250
PidKp                     1.0
PidKi                     0.0
PidKd                     0.0
PidScale                  0.01
PidShowDebug             no
PidStartupDelay           0.000
SeekToTarget            yes

SetDefaults

#### QA Tests

test
percentage        3.5
#Target         175.0
TargetLower     150.0
TargetUpper     200.0
end

test
percentage        -3.5
#Target         -175.0
TargetLower     -200.0
TargetUpper     -150.0
end
