#### Test control values defaults:

Duration                3600
Interval                  60
PauseAfterTest             1.0
StopAfterTest            yes
TestStartupTime            0.500
FaultPeriod                3.000
KeepOutOfRange           yes
PollInterval               0.000

LogAverages              yes
LogBadCounts             yes
LogFirstPoll              no
LogMaximums              yes
LogMinimums              yes

ValidFaultsErrorMask    0xff
ValidFaultsOkayValue    0x00
ValidCurrentMinimum        0.010
ValidCurrentMaximum        1.000
ValidRpmMinimum       -10000.0
ValidRpmMaximum        10000.0
ValidTempMinimum          10.0
ValidTempMaximum          55.0
ValidVoltageMinimum       45.0
ValidVoltageMaximum       50.0

Duration                   2
Interval                   0.25

SetDefaults

#### QA Tests

test
duration       1
percentage     0.0
end

test
percentage    10.0
end

test
percentage   -10.0
end

#### Configuration values

CsvDestTemplate     '~/Projects/ttt/out/thr_{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/Projects/ttt/logs/thr_{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest       yes
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex         1
LogCsrHexAtEnd       no
LogCsrHexAtStart      1
LogDestTemplate     '~/Projects/ttt/out/thr_{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/Projects/ttt/logs/thr_{serialNumber:s}_{startTime:s}.log'
LogModuleVersions   yes
LogMoveToDest       yes
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
PortName            ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration    no
ShowTests            no
SummaryLogFile      '~/Projects/ttt/out/thr_summary.txt'
SummaryLogUpdate    yes
TestRegime          '1.5.2 Test 1'
UserName            ''
