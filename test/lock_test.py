import fasteners
import os
import sys

#=======================================================================================================================

# Note:  Times values are in given in seconds.
#
LockDelay       = 0.25      # Delay between attempts to attempt to acquire the summary log lock.
LockMaxDelay    = 0.25      # Maximum delay between attempts to attempt to acquire the summary log lock.
LockTimeout     = 5.0       # Maximum time in seconds to attempt to acquire the summary log lock.

if os.name == 'nt' :
    LockPathDefault = "//fs1/dat/VideoRay/out/led_summary.lck"
else:
    LockPathDefault = "/mnt/fs1_dat/VideoRay/out/led_summary.lck"

#=======================================================================================================================

def messageBeep(message) :
    """Write a message to sys.stdout and beep if it is the console.
    """
    print("{beep:s}{message:s}".format(beep = "\a" if sys.stdout.isatty() else "",
                                        message = message,
                                        file = sys.stdout,
                                        end = ''))

#----------------------------------------------------------

def appMmain() :
    """Application main function.
    """
    lockPath = LockPathDefault if 2 > len(sys.argv) else sys.argv[1]
    lck = fasteners.InterProcessLock(lockPath)
    isLocked = False
    print("Lock path: '{}'".format(lockPath))
    #
    while True :
        if not lck.exists() :
            print("Lock DOES NOT EXIST")
        prompt = "Unlock or Exit: " if isLocked else "Lock or Exit: "
        answer = (input(prompt).upper() + ' ')[0]
        if 'E' == answer or 'X' == answer or 'Q' == answer :
            messageBeep("Goodbye\n")
            exit(1)
        elif isLocked and 'U' == answer :
            lck.release()
            isLocked = False
        elif (not isLocked) and 'L' == answer :
            isLocked = lck.acquire(blocking = False,
                                        delay = LockDelay,
                                        max_delay = LockMaxDelay,
                                        timeout = LockTimeout)
            if not isLocked :
                messageBeep("\nUnable to acquire the lock!\n")
        elif ' ' != answer :
            messageBeep("")

#=======================================================================================================================

if __name__ == "__main__":
    appMmain()
