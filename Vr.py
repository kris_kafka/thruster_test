import binascii
import math
import random
import serial
import struct
import time
import VrUtil

#=======================================================================================================================

ModuleName          = "Vr"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 3

#=======================================================================================================================

class _Config :
    """CSR protocol configuration parameters.
    """
    # Note:
    #   Times values are in given in seconds.
    #
    # VideoRay COmmunicatons Protocol parameters
    EnumerateMinSee     = 3         # Minimum number of times to see each device when enumerating devices.
    EnumerateRetry      = 25        # Maximum number of attempts to enumerate the devices.
    EnumerateTimeMin    = 0.200     # Minimum time to wait for devices to announce themselves.
    EnumerateTimeMax    = 1.000     # Maximum time to wait for devices to announce themselves.
    RebootPauseTime     = 0.050     # Time to pause between sending reboot commands.
    RebootSendCount     = 5         # Number of time to send a reboot command.
    #
    # Serial port control values
    SerialSleepTime     = 0.005     # How long to sleep while waiting for incoming serial data.
    BaudRate            = 115200    # Serial port baud rate.
    ReadTimeout         = 0.000     # Serial port read timeout in seconds.
    WriteTimeout        = 0.050     # Serial port write timeout in seconds.
    InterByteTimeout    = None      # Serial port inter byte timeout in seconds.

#=======================================================================================================================

class _Debug :
    """Debugging configuration parameters.
    """
    ShowBadAddress          = False
    ShowBadFlags            = False
    ShowBadMaxLength        = False
    ShowBadMinLength        = False
    ShowBadNetworkId        = False
    ShowBadPayloadCrc       = False
    ShowBadPayloadData      = False
    ShowCommands            = False
    ShowDiscarded           = False
    ShowFound               = False
    ShowNoReply             = False
    ShowReceived            = False
    ShowReplies             = False
    ShowSeenCounts          = False

#=======================================================================================================================

class _Text :
    FloatAtError            = "Invalid float size {size}, offset={offset}"
    SIntAtError             = "Invalid sInt size {size}, offset={offset}"
    UIntAtError             = "Invalid uInt size {size}, offset={offset}"
    #
    FirmwareVersion1        = "?.?.{data}"
    FirmwareVersion3        = "{major:d}.{minor:d}.{build:d}"
    #
    PacketHeader            = "Syn={Sync1:02X}.{Sync2:02X}, " \
                                "ID={NetworkId:02X}, " \
                                "Flags={Flags:02X}, " \
                                "Address={Address:02X}, " \
                                "Length={Length:02X}, " \
                                "Crc={Crc[0]:02X}{Crc[1]:02X}.{Crc[2]:02X}{Crc[3]:02X}"
    PacketPayload           =", " \
                                "Payload={Payload:s}, " \
                                "Crc={Crc[0]:02X}{Crc[1]:02X}.{Crc[2]:02X}{Crc[3]:02X}"
    #
    ShowBadAddress          = "Bad address: want={want:d}, got={got:d}"
    ShowBadMaxLength        = "Bad length: max={want:d}, got={got:d}"
    ShowBadMinLength        = "Bad length: min={want:d}, got={got:d}"
    ShowBadFlags            = "Bad flags: want={want:d}, got={got:d}"
    ShowBadNetworkId        = "Bad networkId: want={want:d}, got={got:d}"
    ShowBadPayloadCrc       = "Bad payload CRC, length={len:d}, rcvd={rcvd}, calc={calc}"
    ShowCommands            = "Sending: {command:s}"
    ShowDiscardedByte       = "Discarding {:02X}"
    ShowDiscardedSync       = "Discarding {data[0]:02X}.{data[1]:02X}"
    ShowFound               = "Found: deviceType={deviceType:02X}, nodeId={nodeId:d}, groupId={groupId:d}, " \
                                "serialNumber='{serialNumber:s}'"
    ShowReceived            = "Received ({len:d}): {data:s}"
    ShowReplies             = "Reply: {data}"
    ShowSeenCounts          = "Saw: SerialNumber='{serialNumber:s}': index={index:d}, count={count:d}"

#=======================================================================================================================

def crcOf(data) :
    """Calculate the CRC of data.

    Arguments:
        data    bytearray of data.
    """
    return int.to_bytes(binascii.crc32(data), Csr.CrcSize, byteorder = Bytes.ByteOrder)

#----------------------------------------------------------

def enumerateGizmos(port) :
    """Enumerate all of the gizmos.

    Arguments:
        port    Serial port.

    Return (Status, Gizmos)
        Status  True if okay.
        Gizmos  List of the found gizmos.
    """
    # ToDo: Improve this algorithm!
    result = []
    counts = []
    retries = _Config.EnumerateRetry
    while retries :
        for gizmo in enumerateOnce(port) :
            index = None
            for idx, giz in enumerate(result) :
                if giz.deviceType == gizmo.deviceType \
                        and giz.nodeId == gizmo.nodeId \
                        and giz.groupId == gizmo.groupId \
                        and giz.serialNumber == gizmo.serialNumber :
                    index = idx
            if index is not None :
                counts[index] = counts[index] + 1
            else:
                index = len(result)
                result.append(gizmo)
                counts.append(1)
            if _Debug.ShowSeenCounts :
                print(_Text.ShowSeenCounts \
                        .format(serialNumber = gizmo.serialNumber, index = index, count = counts[index]))
        minCount = -1
        for count in counts :
            if minCount < 0 or minCount > count :
                minCount = count
        if minCount >= _Config.EnumerateMinSee :
            return True, result
        retries -= 1
    return False, result

#----------------------------------------------------------

def enumerateOnce(port) :
    """Enumerate all of the gizmos.

    Arguments:
        port    Serial port.

        Return a list of the found gizmos.
    """
    CommandEnumerate().send(port)
    reply = ReplyEnumerate()
    replies = []
    timer = TimeoutTimer(random.uniform(_Config.EnumerateTimeMin, _Config.EnumerateTimeMax))
    while timer.isActive() :
        status = reply.getReply(port, timeout = timer.remaining())
        if status is None :
            return replies
        if status :
            deviceType, nodeId, groupId, serialNumber = reply.parse()
            if _Debug.ShowFound :
                print(_Text.ShowFound.format(deviceType, nodeId, groupId, serialNumber))
            replies.append(Gizmo(nodeId, groupId, serialNumber, deviceType))
    return replies

#----------------------------------------------------------

def csrFormatField(csr, fieldInfo, beforeSize, valueSize, tooBig = False) :
    """Partially format a field of the CSR.
    Arguments:
        csr         CSR data.
        fieldInfo   Field information (offset, size, unpacking format, width, decimal digits, prefix,
                    format character, name).
        beforeSize  Number of characters before decimal point in field value.
        valueSize   Maximum number of characters in formatted field value.
        tooBig      Replace overflows with '---TooBig---'.

    Return Field offset, Field name, Formatted field data, Field data in hex
    """
    offset, size, unpackStr, width, decimal, prefix, fmtChr, validate, name = fieldInfo
    bytes = csr[offset : offset + size]
    #
    if unpackStr is not None :
        value = struct.unpack(unpackStr, bytes)[0]
        if validate is not None and not validate(value) :
            value = "BAD".center(valueSize, '-')
        elif math.isinf(value) :
            value = "INF".center(valueSize, '-' if value < 0 else '+')
        elif math.isnan(value) :
            value = "NAN".center(valueSize, '-')
        elif decimal is not None :
            value = " " * (beforeSize - (width - decimal - (decimal > 0))) \
                    + ("{value:" + prefix + str(width) + "." + str(decimal) + fmtChr + "}") \
                        .format(value = value)
        else:
            value = ("{value:" + prefix + str(width) + fmtChr + "}") \
                    .format(value = struct.unpack(unpackStr, bytes)[0]) \
                    .rjust(beforeSize)
    else:
        value = ""
    if tooBig and (len(value) > valueSize or value.find(".") > beforeSize) :
        value = "TooBig".center(valueSize, '-')
    hex = " ".join(["{:02X}".format(byte) for byte in bytes])
    return offset, name, value, hex

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#=======================================================================================================================

class Bytes :
    #
    # Conversion constants
    ByteOrder   = "little"
    #
    Real4       = "<f"
    Real8       = "<d"
    SInt1       = "<b"
    SInt2       = "<h"
    SInt4       = "<i"
    SInt8       = "<q"
    UInt1       = "<B"
    UInt2       = "<H"
    UInt4       = "<I"
    UInt8       = "<Q"

#=======================================================================================================================

class Csr :
    """Common VideoRay constants.
    """
    #
    #
    AckReplyFlags       = 0x01
    CsrReplyFlags       = 0x80
    CsrMaxReplyLength   = 0xF0
    NoReplyFlags        = 0x00
    RestOfCsrFlags      = 0x80
    #
    # Common CSR addresses
    CustomCommand       = 0xF0
    FactoryService      = 0xF4
    ConfigurationSize   = 0xF8
    ConfigurationData   = 0xFA
    FirmwareVersion     = 0xFB
    NodeId              = 0xFC
    GroupId             = 0xFD
    UtilityCommand      = 0xFE
    #
    # Field sizes
    CrcSize             = 4

    #----------------------------------------------------------

    def deviceTypeSize(self) :
        """Return the size of the device type.
        """
        return 1

#=======================================================================================================================

class Devices :
    """VideoRay device types.
    """
    Thruster    = 0x81
    Power75V    = 0x82
    Led         = 0x83
    Spam        = 0xC0
    CommsHub    = 0xC1
    Attitude    = 0xC2
    Power420V   = 0xC3
    Camera      = 0xC4

#=======================================================================================================================

class SerialPortWriteError(serial.SerialException) :
    """Exception raised when a serial port write did not write all of the provided data.

    Attributes:
        written Number of bytes written.
        size    Original size of data.
    """
    #----------------------------------------------------------

    def __init__(self, written, size) :
        self.written = written
        self.size = size

#-----------------------------------------------------------------------------------------------------------------------

class Port :
    """Serial port I/O interface.
    """

    #----------------------------------------------------------

    def __init__(self, name) :
        """Open and initialize serial port.

        Arguments:
            name    Name of the port to open.
        """
        self.port = serial.Serial(port = name,
                                    baudrate = _Config.BaudRate,
                                    timeout = _Config.ReadTimeout,
                                    write_timeout = _Config.WriteTimeout,
                                    inter_byte_timeout = _Config.InterByteTimeout)

    #----------------------------------------------------------

    def read(self, size = 1) :
        """Read up to size bytes from the serial port.

        Returns the data from from the serial port.
        """
        return self.port.read(size)

    #----------------------------------------------------------

    def write(self, data) :
        """Write data to the serial port.
        """
        self.port.reset_input_buffer()
        size = len(data)
        written = self.port.write(data)
        if size != written :
            raise SerialPortWriteError(written, size)

#=======================================================================================================================

class TimeoutTimer :
    """Timeout timer.
    """
    #----------------------------------------------------------

    def __init__(self, timeout) :
        """Initialize a TimeoutTimer.
        """
        self.endTime    = timeout + time.monotonic()
        self.timeLeft   = timeout

    #----------------------------------------------------------

    def remaining(self) :
        """Return the time remaining.
        """
        return self.timeLeft

    #----------------------------------------------------------

    def isActive(self) :
        """Determine if the timeout has expired.
        """
        self.timeLeft = self.endTime - time.monotonic()
        return self.timeLeft >= 0

    #----------------------------------------------------------

    def isExpired(self) :
        """Determine if the timeout has expired.
        """
        self.timeLeft = self.endTime - time.monotonic()
        return self.timeLeft < 0

#=======================================================================================================================

class Packet(Csr) :
    """Common VideoRay protocol packet information and functions.
    """
    # Special network ID.
    BroadcastNetId          = 0xFF
    # Offsets and sizes
    Sync1Offset             = 0
    Sync2Offset             = 1
    NetworkIdOffset         = 2
    FlagsOffset             = 3
    AddressOffset           = 4
    LengthOffset            = 5
    HeaderSize              = 6
    SyncSize                = 2
    HeaderCrcOffset         = HeaderSize
    HeaderAndCrcSize        = HeaderSize + Csr.CrcSize
    PayloadOffset           = HeaderAndCrcSize
    PayloadCrcBaseOffset    = HeaderSize + 2 * Csr.CrcSize

    #----------------------------------------------------------

    def __init__(self, packet = None) :
        """Initialize a generic packet.
        """
        if packet is None :
            packet = bytearray()
        self.packet = packet

    #----------------------------------------------------------

    def address(self) :
        """Return the address of the packet or None.
        """
        return self.byteAt(self.AddressOffset)

    #----------------------------------------------------------

    def asStr(self) :
        """Convert a packet to a string.
        """
        answer = _Text.PacketHeader \
                        .format(Sync1 = self.sync1(),
                                Sync2 = self.sync1(),
                                NetworkId = self.networkId(),
                                Flags = self.flags(),
                                Address = self.address(),
                                Length = self.length(),
                                Crc = self.headerCrc())
        if self.havePayload() :
            answer += _Text.PacketPayload \
                            .format(Payload = " ".join(["{:02X}".format(byte) for byte in self.payload()]),
                                    Crc = self.payloadCrc())
        return answer

    #----------------------------------------------------------

    def byteAt(self, index) :
        """Get a byte from a packet.

        Arguments:
            index   Index of the byte.

        Return the byte if the index is valid else None.
        """
        return self.packet[index] if index < len(self.packet) else None

    #----------------------------------------------------------

    def bytesAt(self, index, length) :
        """Get bytes from a packet.

        Arguments:
            index   Index of the first byte.
            length  Number of bytes to return.

        Return the bytes if the index and length are valid else None.
        """
        return self.packet[index : index + length] if (index + length) <= len(self.packet) else None

    #----------------------------------------------------------

    def checkHeaderCrc(self) :
        """Determine if the packet's header CRC is valid.
        """
        return self.headerCrc() == crcOf(self.header())

    #----------------------------------------------------------

    def checkPayloadCrc(self) :
        """Determine if the packet's payload CRC is valid.
        """
        return self.payloadCrc() == crcOf(self.payload())

    #----------------------------------------------------------

    def flags(self) :
        """Return the flags field of the packet or None.
        """
        return self.byteAt(self.FlagsOffset)

    #----------------------------------------------------------

    def header(self) :
        """Return the header of the packet or None.
        """
        return self.bytesAt(0, self.HeaderSize)

    #----------------------------------------------------------

    def headerAndCrc(self) :
        """Return the header of the packet or None.
        """
        return self.bytesAt(0, self.HeaderAndCrcSize)

    #----------------------------------------------------------

    def havePayload(self) :
        """Determine the if packet has a payload.
        """
        length = self.length()
        return False if length is None else length > 0

    #----------------------------------------------------------

    def headerCrc(self) :
        """Return the header CRC of the packet or None.
        """
        return self.bytesAt(self.HeaderSize, Csr.CrcSize)

    #----------------------------------------------------------

    def isHeaderCrcValid(self) :
        """Determine if the packet's header is valid.
        """
        # Check the header's CRC if we have it.
        return len(self.packet) >= self.HeaderAndCrcSize \
            and self.checkHeaderCrc()

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return True

    #----------------------------------------------------------

    def isPayloadCrcValid(self) :
        """Determine if the packet's payload is valid.
        """
        # Done if there is no payload.
        length = self.length()
        if length == 0 :
            return True
        # Check the payload's CRC if we have it.
        return len(self.packet) >= (length + self.PayloadCrcBaseOffset) \
            and self.checkPayloadCrc()

    #----------------------------------------------------------

    def isValid(self) :
        """Determine if the packet is valid.
        """
        return self.isHeaderCrcValid() \
            and self.isPayloadCrcValid() \
            and self.isPayloadDataValid()

    #----------------------------------------------------------

    def length(self) :
        """Return the length field of the packet or None.
        """
        return self.byteAt(self.LengthOffset)

    #----------------------------------------------------------

    def networkId(self) :
        """Return the network ID field of the packet or None.
        """
        return self.byteAt(self.NetworkIdOffset)

    #----------------------------------------------------------

    def payload(self) :
        """Return the payload of the packet or None.
        """
        length = self.length()
        if length is None or length == 0 or length > (len(self.packet) - self.HeaderAndCrcSize):
            return None
        return self.packet[self.HeaderAndCrcSize : self.HeaderAndCrcSize + length]

    #----------------------------------------------------------

    def payloadCrc(self) :
        """Return the payload CRC of the packet or None.
        """
        length = self.length()
        if length is None or length == 0:
            return None
        return self.bytesAt(self.HeaderAndCrcSize + length, Csr.CrcSize)

    #----------------------------------------------------------

    def sync1(self) :
        """Return the first sync field of the packet or None.
        """
        return self.byteAt(self.Sync1Offset)

    #----------------------------------------------------------

    def sync2(self) :
        """Return the second sync field of the packet or None.
        """
        return self.byteAt(self.Sync2Offset)

#=======================================================================================================================

class Command(Packet) :
    """Common VideoRay protocol command packet information and functions.
    """
    # Sync bytes
    Sync1       = 0xF5
    Sync2       = 0x5F
    FlagReadCsr = 0x80

    #----------------------------------------------------------

    def __init__(self, networkId, address, flags, payload = None) :
        """Initialize a command packet.

        Parameters:
            networkId   The network ID of the packet.
            address     The CSR address.
            flags       The packet flags.
            payload     The payload of the packet.
        """
        size = 0 if payload is None else len(payload)
        header  = bytearray([Command.Sync1, Command.Sync2, networkId, flags, address, size])
        if size :
            pl = bytearray(payload)
            self.packet = bytearray().join([header, crcOf(header), pl, crcOf(pl)])
        else:
            self.packet = header + crcOf(header)

    #----------------------------------------------------------

    def send(self, port) :
        """Send a packet.

        Parameters: 
            port    Serial port.
        """
        if _Debug.ShowCommands :
            print(_Text.ShowCommands.format(command = self.asStr()))
        port.write(self.packet)

#=======================================================================================================================

class CommandEnumerate(Command) :
    """VideoRay protocol enumerate devices command packets.
    """
    Flags   = 0x7F
    Payload = [0x49, 0x44, 0x00, 0x00]

    #----------------------------------------------------------

    def __init__(self) :
        """Initialize an enumerate devices command packet.
        """
        super().__init__(self.BroadcastNetId,
                            self.UtilityCommand,
                            CommandEnumerate.Flags,
                            CommandEnumerate.Payload)

#-----------------------------------------------------------------------------------------------------------------------

class CommandFirmwareVersion(Command) :
    """VideoRay protocol get firmware version command packets.
    """
    Flags   = 0x81

    #----------------------------------------------------------

    def __init__(self, networkId) :
        """Initialize get firmware version command packet.
        """
        super().__init__(networkId,
                            self.FirmwareVersion,
                            CommandFirmwareVersion.Flags)

#-----------------------------------------------------------------------------------------------------------------------

class CommandCsrRead(Command) :
    """VideoRay protocol CSR read command packets.
    """

    #----------------------------------------------------------

    def __init__(self, networkId, address, length) :
        """Initialize a CSR read command packet.

        Returns the generated packet.
        """
        super().__init__(networkId, address, self.FlagReadCsr | length, bytearray())

#-----------------------------------------------------------------------------------------------------------------------

class CommandCsrWrite(Command) :
    """VideoRay protocol CSR write command packets.
    """

    #----------------------------------------------------------

    def __init__(self, networkId, address, data, length = 0) :
        """Initialize a CSR write command packet.

        Returns the generated packet.
        """
        flags = 0 if length == 0 else self.FlagReadCsr | length
        super().__init__(networkId, address, flags, data)

#-----------------------------------------------------------------------------------------------------------------------

class CommandReboot(Command) :
    """VideoRay protocol reboot gizmo command packets.
    """
    Payload = bytearray([0xAD, 0xDE])

    #----------------------------------------------------------

    def __init__(self, networkId) :
        """Initialize a reboot command packet.

        Returns the generated packet.
        """
        super().__init__(networkId, self.UtilityCommand, self.NoReplyFlags, self.Payload)

#-----------------------------------------------------------------------------------------------------------------------

class CommandSetIds(Command) :
    """VideoRay protocol set gizmo node ID and group ID command packets.
    """

    #----------------------------------------------------------

    def __init__(self, serialNumber, newNodeId, newGroupId) :
        """Initialize a reboot command packet.

        Returns the generated packet.
        """
        # ToDo: Test this code.
        payload = bytearray(serialNumber, 'latin1') + bytearray([0x00, newNodeId, newGroupId])
        super().__init__(networkId, self.UtilityCommand, self.CommandSetIds, payload)

#=======================================================================================================================

class Reply(Packet) :
    """VideoRay protocol reply packet information and functions.
    """
    # Sync bytes
    Sync1           = 0xF0
    Sync2           = 0x0F
    SyncBytes       = bytearray([Sync1, Sync2])

    #----------------------------------------------------------

    def __init__(self, data = None) :
        """Initialize a reply packet.
        """
        if data is None :
            data = bytearray()
        self.packet = data

    #----------------------------------------------------------

    def deviceType(self) :
        """Return the device type field from the packet's payload or None.
        """
        self.byteAt(self.HeaderAndCrcSize)

    #----------------------------------------------------------

    def getReply(self,
                    port,
                    timeout = 0,
                    replyId = None,
                    replyAddress = None,
                    replyFlags = None,
                    replyMinLength = None,
                    replyMaxLength = None) :
        """Get a reply packet.

        Arguments:
            port            Serial port.
            timeout         Maximum time to wait for the reply packet.
            replyId         Network ID field of reply packet.
            replyAddress    Address field of reply packet.
            replyFlags      Flags field of reply packet.
            replyMinLength  Minimum value of length field of reply packet.
            replyMaxLength  Maximum value of length field of reply packet.

        Returns:
            None:   Timeout, any data received is in packet
            False   Received packet is not valid
            True    Received packet is valid
        """
        self.packet = bytearray()
        bytesNeeded = self.HeaderAndCrcSize
        timer = TimeoutTimer(timeout)
        while timer.isActive() :
            # Get more data if needed.
            if bytesNeeded > 0 :
                data = port.read(bytesNeeded)
                if 0 == len(data) :
                    if _Config.SerialSleepTime :
                        time.sleep(_Config.SerialSleepTime)
                    continue
                if _Debug.ShowReceived :
                    print(_Text.ShowReceived \
                            .format(len = len(data),
                                    data = " ".join(["{:02X}".format(byte) for byte in data])))
                self.packet += data
            # Ensure the packet starts with the sync characters.
            while self.SyncSize <= len(self.packet) \
                    and self.SyncBytes != self.packet[0 : 2] :
                if _Debug.ShowDiscarded :
                    print(_Text.ShowDiscardedByte.format(self.packet[0]))
                del self.packet[0]
            # Must have at least a header.
            bytesNeeded = self.HeaderAndCrcSize - len(self.packet)
            if bytesNeeded > 0 :
                continue
            # Ensure the header is valid.
            if not self.checkHeaderCrc() :
                if _Debug.ShowDiscarded :
                    print(_Text.ShowDiscardedSync.format(data = self.packet[0 : 2]))
                del self.packet[0 : 2]
                bytesNeeded += 2
                continue
            # Done if no payload.
            length = self.length()
            if 0 == length :
                if _Debug.ShowReplies :
                    print(_Text.ShowReplies.format(data = self.asStr()))
                return True
            # Need more data?
            bytesNeeded = (length + self.PayloadCrcBaseOffset) - len(self.packet)
            if bytesNeeded > 0 :
                continue
            answer = self.checkPayloadCrc()
            if not answer and _Debug.ShowBadPayloadCrc :
                print(_Text.ShowBadPayloadCrc \
                        .format(len = length, rcvd = self.payloadCrc(), calc = crcOf(self.payload())))
            else:
                answer = self.isPayloadDataValid()
                if not answer and _Debug.ShowBadPayloadData :
                    print("Bad payload data")
            if answer and replyId is not None :
                answer = self.networkId() == replyId
                if not answer and _Debug.ShowBadNetworkId :
                    print(_Text.ShowBadNetworkId.format(want = replyId, got = self.networkId()))
            if answer and replyAddress is not None :
                answer = self.address() == replyAddress
                if not answer and _Debug.ShowBadAddress :
                    print(_Text.ShowBadAddress.format(want = replyAddress, got = self.address()))
            if answer and replyFlags is not None :
                answer = self.flags() == replyFlags
                if not answer and _Debug.ShowBadFlags :
                    print(_Text.ShowBadFlags.format(want = replyFlags, got = self.flags()))
            if answer and replyMinLength is not None :
                answer = self.length() >= (replyMinLength + self.deviceTypeSize())
                if not answer and _Debug.ShowBadMinLength :
                    print(_Text.ShowBadMinLength.format(want = replyMinLength, got = self.length()))
            if answer and replyMaxLength is not None :
                answer = self.length() <= (replyMaxLength + self.deviceTypeSize())
                if not answer and _Debug.ShowBadMaxLength :
                    print(_Text.ShowBadMaxLength.format(want = replyMaxLength, got = self.length()))
            if answer and _Debug.ShowReplies :
                print(_Text.ShowReplies.format(data = self.asStr()))
            return answer
        # Indicate a timeout occurred.
        if _Debug.ShowNoReply :
            print("No reply")
        return None

    #----------------------------------------------------------

    def payloadData(self) :
        """Return the payload data (without the device type prefix).
        """
        length = self.length()
        return self.bytesAt(self.HeaderAndCrcSize + self.deviceTypeSize(), length - self.deviceTypeSize()) \
                if length else None

    #----------------------------------------------------------

    def floatAt(self, offset, size) :
        """Return a float from the payload.

        Arguments:
            offset  Offset into the payload data.
            size    Size of the float.
        """
        if size != 4 and size != 8 :
            VrUtil.errorBeep(_Text.FloatAtError.format(size = size, offset = offset))
            exit(255)
        return self.float4At(offset) if size == 4 else self.float8At(offset)

    #----------------------------------------------------------

    def float4At(self, offset) :
        """Return a four-byte float from the payload.

        Arguments:
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 4)
        return struct.unpack(Bytes.Real4, data)[0]

    #----------------------------------------------------------

    def float8At(self, offset) :
        """Return an eight-byte float from the payload.

        Arguments:
            offset  Offset into the payload data.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 8)
        return struct.unpack(Bytes.Real8, data)[0]

    #----------------------------------------------------------

    def sIntAt(self, offset, size) :
        """Return a signed integer from the payload.

        Arguments:
            offset  Index of the byte.
            size    Size of the signed integer.
        """
        if size != 1 and size != 2 and size != 4 and size != 8 :
            VrUtil.errorBeep(_Text.SIntAtError.format(size = size, offset = offset))
            exit(255)
        return self.sInt1At(offset) if size == 1 else \
                self.sInt2At(offset) if size == 2 else \
                self.sInt4At(offset) if size == 4 else \
                self.sInt8At(offset)

    #----------------------------------------------------------

    def sInt1At(self, offset) :
        """Return a one-byte signed integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 1)
        return struct.unpack(Bytes.SInt1, data)[0]

    #----------------------------------------------------------

    def sInt2At(self, offset) :
        """Return a two-byte signed integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 2)
        return struct.unpack(Bytes.SInt2, data)[0]

    #----------------------------------------------------------

    def sInt4At(self, offset) :
        """Return a four-byte signed integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 4)
        return struct.unpack(Bytes.SInt4, data)[0]

    #----------------------------------------------------------

    def sInt8At(self, offset) :
        """Return a four-byte signed integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 8)
        return struct.unpack(Bytes.SInt8, data)[0]

    #----------------------------------------------------------

    def uIntAt(self, offset, size) :
        """Return an unsigned integer from the payload.

        Arguments:
            offset  Index of the byte.
            size    Size of the unsigned integer.
        """
        if size != 1 and size != 2 and size != 4 and size != 8 :
            VrUtil.errorBeep(_Text.UIntAtError.format(size = size, offset = offset))
            exit(255)
        return self.uInt1At(offset) if size == 1 else \
                self.uInt2At(offset) if size == 2 else \
                self.uInt4At(offset) if size == 4 else \
                self.uInt8At(offset)

    #----------------------------------------------------------

    def uInt1At(self, offset) :
        """Return a one-byte unsigned integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 1)
        return struct.unpack(Bytes.UInt1, data)[0]

    #----------------------------------------------------------

    def uInt2At(self, offset) :
        """Return a two-byte unsigned integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 2)
        return struct.unpack(Bytes.UInt2, data)[0]

    #----------------------------------------------------------

    def uInt4At(self, offset) :
        """Return a four-byte unsigned integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 4)
        return struct.unpack(Bytes.UInt4, data)[0]

    #----------------------------------------------------------

    def uInt8At(self, offset) :
        """Return an eight-byte unsigned integer from the payload.

        Arguments:
            offset  Index of the byte.
        """
        data = self.bytesAt(offset + self.HeaderAndCrcSize + self.deviceTypeSize(), 8)
        return struct.unpack(Bytes.UInt8, data)[0]

#=======================================================================================================================

class ReplyCsr(Reply) :
    """VideoRay CSR read/write reply information and functions.
    """

#-----------------------------------------------------------------------------------------------------------------------

class ReplyEnumerate(Reply) :
    """VideoRay device enumeration reply information and functions.
    """
    # Payload must have: device type, node ID, group ID, device type, serial number, '\0'
    MinimumLength = 6

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return super().isPayloadDataValid() \
            and self.length() >= self.MinimumLength

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data: DeviceType, NodeId, GroupId, SerialNumber
        """
        payload = self.payload()
        return payload[0], \
                payload[1], \
                payload[2], \
                payload[4 :].decode(encoding = 'latin1').rstrip("\0")

#-----------------------------------------------------------------------------------------------------------------------

class ReplyFirmwareVersion(Reply) :
    """VideoRay thruster propulsion reply information and functions.
    """
    MajorOffset     = 0
    MajorSize       = 1
    MinorOffset     = MajorSize + MajorOffset
    MinorSize       = 1
    BuildOffset     = MinorSize + MinorOffset
    BuildSize       = 1
    MinimumLength   = BuildSize + BuildOffset
    OldLength       = 1

    #----------------------------------------------------------

    def isOldFirmware(self) :
        """Determine if the old firmware version.
        """
        return self.length() == (self.OldLength + self.deviceTypeSize())

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        answer = super().isPayloadDataValid()
        if answer and not self.isOldFirmware() :
            answer = self.length() >= (self.MinimumLength + self.deviceTypeSize())
        return answer

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data: MajorVersion, MinorVersion, BuildVersion
        """
        if self.isOldFirmware() :
            return _Text.FirmwareVersion1.format(data = self.uInt1At(self.MajorOffset))
        else:
            # ToDo: Check firmware version support with firmware that supports getting the firmware version.
            return _Text.FirmwareVersion3 \
                    .format(major = self.uInt1At(self.MajorOffset),
                            minor = self.uInt1At(self.MinorOffset),
                            build = self.uInt1At(self.BuildOffset))

#=======================================================================================================================

class ReplySetIds(Reply) :
    """VideoRay set IDs reply information and functions.
    """

#=======================================================================================================================

class Gizmo(Csr) :
    """Generic VideoRay gizmo information and functions.
    """
    # ID limits
    MinNodeId       = 0x00  #   0
    MaxNodeId       = 0x7F  # 127
    MinGroupId      = 0x80  # 128
    MaxGroupId      = 0xFE  # 254
    MinDeviceType   = 0x00
    MaxDeviceType   = 0xFF
    #
    # VideoRay Communications Protocol parameters
    CsrReadRetry    = 5         # Maximum number of attempts to perform a CSR read command.
    CsrReadTimeout  = 0.075     # Maximum time to wait for a reply to a CSR read command.
    CsrWriteRetry   = 5         # Maximum number of attempts to perform a CSR write command.
    CsrWriteTimeout = 0.075     # Maximum time to wait for a reply to a CSR write command.
    #
    FirmwareVersionRetry    = CsrReadRetry
    FirmwareVersionTimeout  = CsrReadTimeout
    #
    SetIdsRetry         = CsrReadRetry
    SetIdsTimeout       = CsrReadTimeout
    #
    # FieldInfo indexes.
    FieldOffset     = 0
    FieldSize       = 1
    FieldUnpack     = 2
    FieldWidth      = 3
    FieldDecimal    = 4
    FieldPrefix     = 5
    FieldFormat     = 6
    FieldValidate   = 7
    FieldName       = 8

    #----------------------------------------------------------

    def __init__(self, nodeId = -1, groupId = -1, serialNumber = "", deviceType = -1, firmwareVersion = "-.-.-") :
        """Initialize a generic Gizmo.

        Arguments:
            nodeId          Optional node ID.
            groupId         Optional group ID
            serialNumber    Optional serial number.
            deviceType      Optional device type.
            firmwareVersion Optional firmware version.
        """
        # Device information
        self.nodeId             = nodeId
        self.groupId            = groupId
        self.deviceType         = deviceType
        self.serialNumber       = serialNumber
        self.firmwareVersion    = firmwareVersion
        self.ioError            = False

    #----------------------------------------------------------

    def checkIfOkay(self) :
        """Check if the gizmo information is okay.

        Returns list of reasons why the device is not okay.
        """
        answer = []
        if self.ioError :
            answer.append("I/O error")
        if self.nodeId < self.MinNodeId or self.MaxNodeId < self.nodeId :
            answer.append("Node ID")
        if self.groupId < self.MinGroupId or self.MaxGroupId < self.groupId :
            answer.append("Group ID")
        if self.deviceType < self.MinDeviceType or self.MaxDeviceType < self.deviceType :
            answer.append("Device Type")
        if 0 == len(self.serialNumber) :
            answer.append("Serial Number")
        return answer

    #----------------------------------------------------------

    def csrFormat(self, csr, includeHex = False, tooBig = False) :
        """Return formatted CSR text.
        Arguments:
            csr         CSR data.
            includeHex  Include hex format of data fields.
            tooBig      Replace overflows with '---TooBig---'.

        Return Maximum name size, Maximum formatted value size, Formatted CSR text
        """
        allFields   = self.getFieldInfo()
        nameSize    = max([len(t[self.FieldName]) for t in allFields])
        beforeSize  = max([(t[self.FieldWidth] or 0) - (t[self.FieldDecimal] or 0) - ((t[self.FieldDecimal] or 0) > 0)
                            for t in allFields])
        afterSize   = max([t[self.FieldDecimal] if t[self.FieldDecimal] is not None else 0 for t in allFields])
        valueSize   = beforeSize + 1 + afterSize
        dataFmt = "{offset:>3d}  {name:s}  {value:" \
                + ((str(valueSize) + "s}   {hex:s}") if includeHex else "s}")
        rsvdFmt = "{offset:>3d}  {name:s}  {value:" + str(valueSize) + "s}   {hex:s}"
        lines = []
        for fieldInfo in allFields :
            offset, name, value, hex = csrFormatField(csr, fieldInfo, beforeSize, valueSize, tooBig)
            lines.append((dataFmt if fieldInfo[self.FieldUnpack] else rsvdFmt)
                                .format(offset = offset,
                                        name = name.ljust(nameSize),
                                        value = value,
                                        hex = ((offset % 8) * "   ") + hex))
        return nameSize, valueSize, "\n".join(lines)

    #----------------------------------------------------------

    def csrRead(self, port, address, length, minReplyLength = None, reply = None) :
        """Read bytes from the CSR.

        Arguments:
            port    Serial port.
            address Offset of first byte of the CSR to read.
            length  Number of bytes of the CSR to read.
            reply   Reply packet.

        Returns the reply packet or None.
        """
        if self.ioError :
            return None
        if reply is None :
            reply = ReplyCsr()
        if minReplyLength is None :
            minReplyLength = length
        command = CommandCsrRead(self.nodeId, address, length)
        retriesLeft = self.CsrReadRetry
        while retriesLeft :
            command.send(port)
            if reply.getReply(port,
                                timeout = self.CsrReadTimeout,
                                replyId = self.nodeId,
                                replyAddress = address,
                                replyFlags = self.CsrReplyFlags,
                                replyMinLength = minReplyLength) :
                return reply
            retriesLeft -= 1
        self.ioError = True
        return None

    #----------------------------------------------------------

    def csrWrite(self, port, address, length, data, networkId = None, reply = None) :
        """Write bytes to the CSR.

        Arguments:
            port    Serial port.
            address Offset of first byte of the CSR to write.
            length  Number of bytes to read from the CSR.
            data    Bytes to write to the CSR.
            reply   Reply packet.

        Returns the reply packet or None.
        """
        if self.ioError :
            return None
        if reply is None :
            reply = ReplyCsr()
        if networkId is None :
            networkId = self.nodeId
        command = CommandCsrWrite(self.nodeId, address, data, length)
        retriesLeft = self.CsrWriteRetry
        while retriesLeft :
            command.send(port)
            if reply.getReply(port,
                                timeout = self.CsrWriteTimeout,
                                replyId = self.nodeId,
                                replyAddress = address,
                                replyFlags = self.CsrReplyFlags,
                                replyMinLength = length) :
                return reply
            retriesLeft -= 1
        self.ioError = True
        return None

    #----------------------------------------------------------

    def getFieldInfo(self) :
        """Get the gizmo's CSR field info.
        """
        #FieldInfo  =   [
        #               # Offset Size   Unpack          Width   Decimal Prefix  Format  Valid,  Name
        #                   (238,   2,  Bytes.UInt2,    4,      None,   "0",    "X",    None,   "save_settings")
        #               ]
        VrUtil.errorBeep("getFieldInfo must be defined in subclass of Vr.Gizmo")
        exit(255)

    #----------------------------------------------------------

    def getFirmwareVersion(self, port) :
        """Get the firmware version.

        Arguments:
            port    Serial port.

        Return True if no errors else False.
        """
        if self.ioError :
            return None
        command = CommandFirmwareVersion(self.nodeId)
        retriesLeft = self.FirmwareVersionRetry
        reply = ReplyFirmwareVersion()
        while retriesLeft :
            command.send(port)
            if reply.getReply(port,
                                timeout = self.FirmwareVersionTimeout,
                                replyId = self.nodeId,
                                replyAddress = self.FirmwareVersion) :
                self.firmwareVersion = reply.parse()
                return True
            retriesLeft -= 1
        VrUtil.errorBeep("Error getting firmware version from nodeId={nodeId:d}, serialNumber='{serialNumber:s}'" \
                                    .format(nodeId = self.nodeId, serialNumber = self.serialNumber))
        self.ioError = True
        return False

    #----------------------------------------------------------

    def reboot(self, port) :
        """Reboot the gizmo.

        Arguments:
            port    Serial port.
        """
        command = CommandReboot(self.nodeId)
        sendCount = _Config.RebootSendCount
        while sendCount :
            sendCount -= 1
            command.send(port)
            if sendCount and _Config.RebootPauseTime > 0.0 :
                time.sleep(_Config.RebootPauseTime)

    #----------------------------------------------------------

    def setIds(self, port, newNodeId, newGroupId) :
        """Set the node ID and group ID of the gizmo.

        Arguments:
            port        Serial port.
            newNodeId   New node ID.
            newGroupId  New group ID.

        Returns True on success else False.
        """
        # ToDo: Test this code.
        if self.ioError :
            return None
        command     = CommandSetIds(self.serialNumber, newNodeId, newGroupId)
        reply       = ReplySetIds()
        retiesLeft  = self.SetIdsRetry
        while retriesLeft :
            command.send(port)
            if reply.getReply(port,
                                timeout = self.SetIdsTimeout,
                                replyId = self.newNodeId,
                                replyAddress = self.UtilityCommand,
                                replyFlags = self.AckReplyFlags,
                                replyMaxLength = 0) :
                return True
            retriesLeft -= 1
        self.ioError = True
        return False
