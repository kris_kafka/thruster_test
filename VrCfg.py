import os
import re
import VrUtil
import ply.lex as lex

#=======================================================================================================================

ModuleName          = "VrCfg"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 1

#=======================================================================================================================

class _Debug :
    """Debugging configuration parameters.
    """
    ShowParsedTestData  = False
    ShowParsedTokens    = False

#=======================================================================================================================

# Configuration dictionary keys
CsvDestTemplate         = "CsvDestTemplate"
CsvErrorCounts          = "CsvErrorCounts"
CsvMakeTemplate         = "CsvMakeTemplate"
CsvMoveToDest           = "CsvMoveToDest"
LogCsrDataAtEnd         = "LogCsrDataAtEnd"
LogCsrDataAtStart       = "LogCsrDataAtStart"
LogCsrDataHex           = "LogCsrDataHex"
LogCsrHexAtEnd          = "LogCsrHexAtEnd"
LogCsrHexAtStart        = "LogCsrHexAtStart"
LogDestTemplate         = "LogDestTemplate"
CsvGenerate             = "CsvGenerate"
LogErrorCounts          = "LogErrorCounts"
LogGenerate             = "LogGenerate"
LogMakeTemplate         = "LogMakeTemplate"
LogModuleVersions       = "LogModuleVersions"
LogMoveToDest           = "LogMoveToDest"
LogPollCounts           = "LogPollCounts"
LogRepeatInfoAtEnd      = "LogRepeatInfoAtEnd"
PauseAfterLastTest      = "PauseAfterLastTest"
PortName                = "PortName"
ResetGizmos             = "ResetGizmos"
ResetPause              = "ResetPause"
ShowConfiguration       = "ShowConfiguration"
ShowNonDefault          = "ShowNonDefault"
ShowTests               = "ShowTests"
SummaryLogFile          = "SummaryLogFile"
SummaryLogUpdate        = "SummaryLogUpdate"
TestHash                = "TestHash"
TestRegime              = "TestRegime"
TestSource              = "TestSource"
TestTimeStamp           = "TestTimeStamp"
UserName                = "UserName"

# Test dictionary keys
Duration                = "Duration"
FaultPeriod             = "FaultPeriod"
Interval                = "Interval"
KeepOutOfRange          = "KeepOutOfRange"
LogAverages             = "LogAverages"
LogBadCounts            = "LogBadCounts"
LogFirstPoll            = "LogFirstPoll"
LogMaximums             = "LogMaximums"
LogMinimums             = "LogMinimums"
PauseAfterTest          = "PauseAfterTest"
PollInterval            = "PollInterval"
StopAfterTest           = "StopAfterTest"
TestStartupTime         = "TestStartupTime"
ValidCurrentMaximum     = "ValidCurrentMaximum"
ValidCurrentMinimum     = "ValidCurrentMinimum"
ValidFaultsErrorMask    = "ValidFaultsErrorMask"
ValidFaultsOkayValue    = "ValidFaultsOkayValue"
ValidTempMaximum        = "ValidTempMaximum"
ValidTempMinimum        = "ValidTempMinimum"
ValidVoltageMaximum     = "ValidVoltageMaximum"
ValidVoltageMinimum     = "ValidVoltageMinimum"

#=======================================================================================================================

_allTests       = []    # List of test values.
_configuration  = {}    # Configuration values.
_testDefaults   = {}    # Test default values.

#----------------------------------------------------------

def config(key) :
    """Get a configuration value.
    """
    global _configuration
    return _configuration[key]

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#----------------------------------------------------------

def testDefaults() :
    """Return the test defaults.
    """
    global _testDefaults
    return _testDefaults

#----------------------------------------------------------

def tests() :
    """Return the tests.
    """
    global _allTests
    return _allTests

#=======================================================================================================================

class VrOptions :
    """Generic command line option processing.
    """

    #----------------------------------------------------------

    def __init__(self, appInf, gzmCfgPath) :
        """Initialize a VrOptions object.
        """
        self.appInf = appInf
        self.gizmoConfigPath = VrUtil.absPath(gzmCfgPath)

    #----------------------------------------------------------

    def getFormattedConfiguration(self) :
        """Format the global configuration.

        Return a list of formatted values.
        """
        global _configuration
        return [
                    "CsvDestTemplate     '{:s}'".format(_configuration[CsvDestTemplate]),
                    "CsvErrorCounts      {:d}".format(_configuration[CsvErrorCounts]),
                    "CsvGenerate         {:d}".format(_configuration[CsvGenerate]),
                    "CsvMakeTemplate     '{:s}'".format(_configuration[CsvMakeTemplate]),
                    "CsvMoveToDest       {:d}".format(_configuration[CsvMoveToDest]),
                    "LogDestTemplate     '{:s}'".format(_configuration[LogDestTemplate]),
                    "LogGenerate         {:d}".format(_configuration[LogGenerate]),
                    "LogMakeTemplate     '{:s}'".format(_configuration[LogMakeTemplate]),
                    "LogMoveToDest       {:d}".format(_configuration[LogMoveToDest]),
                    "PortName            '{:s}'".format(_configuration[PortName]),
                    "UserName            '{:s}'".format(_configuration[UserName]),
                    "ResetPause          {:.1f}".format(_configuration[ResetPause]),
                    "LogCsrDataAtEnd     {:d}".format(_configuration[LogCsrDataAtEnd]),
                    "LogCsrDataAtStart   {:d}".format(_configuration[LogCsrDataAtStart]),
                    "LogCsrDataHex       {:d}".format(_configuration[LogCsrDataHex]),
                    "LogCsrHexAtEnd      {:d}".format(_configuration[LogCsrHexAtEnd]),
                    "LogCsrHexAtStart    {:d}".format(_configuration[LogCsrHexAtStart]),
                    "LogErrorCounts      {:d}".format(_configuration[LogErrorCounts]),
                    "LogModuleVersions   {:d}".format(_configuration[LogModuleVersions]),
                    "LogPollCounts       {:d}".format(_configuration[LogPollCounts]),
                    "LogRepeatInfoAtEnd  {:d}".format(_configuration[LogRepeatInfoAtEnd]),
                    "PauseAfterLastTest  {:d}".format(_configuration[PauseAfterLastTest]),
                    "ResetGizmos         {:d}".format(_configuration[ResetGizmos]),
                    "ShowConfiguration   {:d}".format(_configuration[ShowConfiguration]),
                    "ShowNonDefault      {:d}".format(_configuration[ShowNonDefault]),
                    "ShowTests           {:d}".format(_configuration[ShowTests]),
                    "SummaryLogFile      '{:s}'".format(_configuration[SummaryLogFile]),
                    "SummaryLogUpdate     {:d}".format(_configuration[SummaryLogUpdate]),
                    "TestRegime          '{:s}'".format(_configuration[TestRegime]),
                    "TestSource          '{:s}'".format(_configuration[TestSource]),
                    "TestHash            {:s}".format(_configuration[TestHash]),
                    "TestTimeStamp       {:s}".format(_configuration[TestTimeStamp]),
                ]

    #----------------------------------------------------------

    def getUpdatedOptions(self, options) :
        """Return the updated command line options.
        """
        if config(UserName) :
            options.userName = config(UserName)
        if config(PortName) :
            options.portName = config(PortName)
        return options

    #----------------------------------------------------------

    def process(self, parser, options) :
        """Get the global configuration options and tests.

        Arguments:
            parser      VrParser class.
            options     Program options.

        Returns updated program options.
        """
        global _allTests, _configuration
        # Get the global configuration options and tests.
        if not options.controlFile :
            _configuration[TestSource] = self.gizmoConfigPath
            parser().parse(self.getDefaultTests())
        else:
            _configuration[TestSource] = VrUtil.absPath(options.controlFile)
            file = open(options.controlFile, "r")
            parser().parse(file.read())
            file.close()
        # Cleanup the TestRegime value.
        _configuration[TestRegime] = _configuration[TestRegime].strip()
        # Fatal error if TestRegime not supplied or too long.
        if 0 == len(_configuration[TestRegime]) :
            VrUtil.errorBeep("Missing TestRegime in " + _configuration[TestSource])
            exit(1)
        if 16 < len(_configuration[TestRegime]) :
            VrUtil.errorBeep("TestRegime too long in " + _configuration[TestSource])
            exit(1)
        # Save the UTC last modified time stamp of the test soruce.
        _configuration[TestHash] = VrUtil.hashOf(_configuration[TestSource])
        _configuration[TestTimeStamp] = VrUtil.utcOf(_configuration[TestSource])
        # Ignore the pause after the last test?
        if not config(PauseAfterLastTest) :
            _allTests[-1][PauseAfterTest] = 0.0
        # Show the tests?
        if config(ShowTests) :
            print("\nTests:")
            for number, test in enumerate(_allTests, start = 1) :
                print(self.getFormatedTest(number, test))
        # Show the global configuration?
        if config(ShowConfiguration) :
            print("\nConfiguration:")
            for line in sorted(self.getFormattedConfiguration()) :
                print("    {:s}".format(line))
        # Update the command line options if needed.
        return self.getUpdatedOptions(options)

#=======================================================================================================================

class VrParser :
    """Generic configuration file parsing.
    """
    # LEX control values

    # Token types
    SidTypTest      = 0x000001
    SidTypConfig    = 0x000002
    SidTypValue     = 0x000004
    SidTypEqual     = 0x000008
    SidTypDelim     = 0x000010
    SidTypBegin     = 0x000020
    SidTypEnd       = 0x000040
    SidTypDefault   = 0x000080
    SidTypStop      = 0x000100

    # Individual value types
    SidValHex       = 0x010000
    SidValUInt      = 0x020000
    SidValSInt      = 0x040000
    SidValUReal     = 0x080000
    SidValSReal     = 0x100000
    SidValBool      = 0x200000
    SidValString    = 0x400000

    # Combined value types
    SidTypBool      = SidTypValue | SidValBool
    SidValMask      = SidValString | SidValBool | SidValSReal | SidValUReal | SidValSInt | SidValUInt | SidValHex
    SidValBoolean   = SidValBool | SidValUInt
    SidValBits      = SidValUInt | SidValHex
    SidValUNum      = SidValUReal | SidValUInt
    SidValSNum      = SidValSReal | SidValUReal | SidValSInt | SidValUInt

    #----------------------------------------------------------

    def getFilePrefix(self) :
        """Return the CSV and log file name prefix.
        """
        return ""

    #----------------------------------------------------------

    def makeConfigId(self, name, type, default, minLength = None, suffix = "") :
        """Define a configuration value ID.
        """
        global _configuration
        _configuration[name + suffix] = default
        self.makeSpecialId(name, self.SidTypConfig | type, minLength = minLength, suffix = suffix)

    #----------------------------------------------------------

    def makeSpecialId(self, tknNam, tknTyp, minLength = None, suffix = "") :
        """Define a special ID.
        """
        head = []
        tail = [suffix]
        if minLength is None :
            minLength = len(tknNam)
        for idx, chr in enumerate(tknNam, 1) :
            head.append(r"(?:")
            head.append(chr)
            tail.append(r")?" if idx > minLength else r")")
        head.extend(reversed(tail))
        regExp = re.compile("".join(head), flags=re.IGNORECASE)
        self.specialIds.append((regExp, tknNam + suffix, tknTyp))

    #----------------------------------------------------------

    def makeSpecialIds(self) :
        """Define the standard special ID.
        """
        # Commands
        self.makeSpecialId("test",          self.SidTypBegin)
        self.makeSpecialId("end",           self.SidTypEnd)
        self.makeSpecialId("setDefaults",   self.SidTypDefault)
        self.makeSpecialId("stop",          self.SidTypStop)
        #
        # Boolean symbols
        self.makeSpecialId("yes",   self.SidTypBool)
        self.makeSpecialId("y",     self.SidTypBool)
        self.makeSpecialId("no",    self.SidTypBool)
        self.makeSpecialId("n",     self.SidTypBool)
        self.makeSpecialId("true",  self.SidTypBool)
        self.makeSpecialId("t",     self.SidTypBool)
        self.makeSpecialId("false", self.SidTypBool)
        self.makeSpecialId("f",     self.SidTypBool)
        #
        # Test values
        self.makeTestId(Duration,               self.SidValUInt,        3600,   minLength = 3)
        self.makeTestId(FaultPeriod,            self.SidValUNum,        3.0)
        self.makeTestId(Interval,               self.SidValUNum,        60.0,   minLength = 3)
        self.makeTestId(KeepOutOfRange,         self.SidValBoolean,     True)
        self.makeTestId(LogAverages,            self.SidValBoolean,     True,   minLength = 6)
        self.makeTestId(LogBadCounts,           self.SidValBoolean,     True,   minLength = 6)
        self.makeTestId(LogFirstPoll,           self.SidValBoolean,     True,   minLength = 8)
        self.makeTestId(LogMaximums,            self.SidValBoolean,     True,   minLength = 6)
        self.makeTestId(LogMinimums,            self.SidValBoolean,     True,   minLength = 6)
        self.makeTestId(PauseAfterTest,         self.SidValUNum,        1.0,    minLength = 5)
        self.makeTestId(PollInterval,           self.SidValUNum,        0.000,  minLength = 7)
        self.makeTestId(StopAfterTest,          self.SidValBoolean,     True,   minLength = 9)
        self.makeTestId(TestStartupTime,        self.SidValUNum,        0.000,  minLength = 11)
        self.makeTestId(ValidCurrentMaximum,    self.SidValSNum,        6.000,  minLength = 15)
        self.makeTestId(ValidCurrentMinimum,    self.SidValSNum,        0.000,  minLength = 15)
        self.makeTestId(ValidFaultsErrorMask,   self.SidValBits,        0x00,   minLength = 16)
        self.makeTestId(ValidFaultsOkayValue,   self.SidValBits,        0x00,   minLength = 15)
        self.makeTestId(ValidTempMaximum,       self.SidValUNum,        75.0,   minLength = 12)
        self.makeTestId(ValidTempMinimum,       self.SidValSNum,        5.0,    minLength = 12)
        self.makeTestId(ValidVoltageMaximum,    self.SidValUNum,        50.0,   minLength = 15)
        self.makeTestId(ValidVoltageMinimum,    self.SidValUNum,        35.0,   minLength = 15)
        #
        # Configuration values
        #
        makeDir = "logs/"
        destDir = "out/"
        sumDir = "out/"
        filePrefix = self.getFilePrefix()
        csvFile = filePrefix + "{serialNumber:s}_{startTime:s}.csv"
        logFile = filePrefix + "{serialNumber:s}_{startTime:s}.log"
        sumFile = filePrefix + "summary"
        sumLog = ".txt"
        #
        self.makeConfigId(CsvDestTemplate,      self.SidValString,  destDir + csvFile,          minLength = 7)
        self.makeConfigId(CsvErrorCounts,       self.SidValBoolean, True,                       minLength = 8)
        self.makeConfigId(CsvMakeTemplate,      self.SidValString,  makeDir + csvFile,          minLength = 7)
        self.makeConfigId(CsvMoveToDest,        self.SidValBoolean, False,                      minLength = 7)
        self.makeConfigId(LogCsrDataAtEnd,      self.SidValBoolean, False)
        self.makeConfigId(LogCsrDataAtStart,    self.SidValBoolean, True)
        self.makeConfigId(LogCsrDataHex,        self.SidValBoolean, False)
        self.makeConfigId(LogCsrHexAtEnd,       self.SidValBoolean, False)
        self.makeConfigId(LogCsrHexAtStart,     self.SidValBoolean, False)
        self.makeConfigId(LogDestTemplate,      self.SidValString,  destDir + logFile,          minLength = 7)
        self.makeConfigId(CsvGenerate,          self.SidValBoolean, True)
        self.makeConfigId(LogErrorCounts,       self.SidValBoolean, True,                       minLength = 8)
        self.makeConfigId(LogGenerate,          self.SidValBoolean, True)
        self.makeConfigId(LogMakeTemplate,      self.SidValString,  makeDir + logFile,          minLength = 7)
        self.makeConfigId(LogMoveToDest,        self.SidValBoolean, False,                      minLength = 7)
        self.makeConfigId(LogModuleVersions,    self.SidValBoolean, True,                       minLength = 12)
        self.makeConfigId(LogPollCounts,        self.SidValBoolean, True,                       minLength = 7)
        self.makeConfigId(LogRepeatInfoAtEnd,   self.SidValBoolean, False,                      minLength = 9)
        self.makeConfigId(PauseAfterLastTest,   self.SidValBoolean, False,                      minLength = 14)
        self.makeConfigId(PortName,             self.SidValString,  "",                         minLength = 4)
        self.makeConfigId(ResetGizmos,          self.SidValBoolean, False)
        self.makeConfigId(ResetPause,           self.SidValUNum,    5.0)
        self.makeConfigId(ShowConfiguration,    self.SidValBoolean, False,                      minLength = 10)
        self.makeConfigId(ShowNonDefault,       self.SidValBoolean, True,                       minLength = 10)
        self.makeConfigId(ShowTests,            self.SidValBoolean, False)
        self.makeConfigId(SummaryLogFile,       self.SidValString,  sumDir + sumFile + sumLog)
        self.makeConfigId(SummaryLogUpdate,     self.SidValBoolean, True)
        self.makeConfigId(TestRegime,           self.SidValString,  "")
        self.makeConfigId(UserName,             self.SidValString,  "",                         minLength = 4)

    #----------------------------------------------------------

    def makeTestId(self, name, type, default, minLength = None, suffix = "") :
        """Define a test control value ID.
        """
        global _testDefaults
        _testDefaults[name + suffix] = default
        self.makeSpecialId(name, self.SidTypTest | type, minLength = minLength, suffix = suffix)

    #----------------------------------------------------------

    def parse(self, data) :
        """Parse the data.

        Returns configuration values and test values.
        """
        global _allTests, _configuration, _testDefaults
        # Create the special lexer IDs.
        self.specialIds = []
        self.makeSpecialIds()
        # Create the lexer.
        self.isSavedToken = False
        self.savedToken = None
        self.lexer = lex.lex(module=self)
        #
        testDefault = dict(_testDefaults)
        testData = dict(testDefault)
        inTest = False
        testDataChanged = False
        testDefaultChange = False
        defaultsSet = False
        doMore = True
        #
        self.lexer.input(data)
        token = self._nextToken()
        while doMore and token :
            # Start of test definition.
            if token.tknTyp & self.SidTypBegin :
                if inTest :
                    self._parseError("Duplicate", token)
                inTest = True
                testDefaultChange = False
                testData = dict(testDefault)
                testDataChanged = False
            # End of test definition.
            elif token.tknTyp & self.SidTypEnd :
                if not inTest :
                    self._parseError("Missing 'test'", token)
                if not testDataChanged :
                    self._parseError("Empty test", token)
                _allTests.append(testData)
                inTest = False
                if _Debug.ShowParsedTestData :
                    print("Test data:\n" + str(testData))
            # Set defaults command.
            elif token.tknTyp & self.SidTypDefault :
                # Must have change the default test control values and not allowed after any tests defined.
                if defaultsSet or inTest or _allTests or not testDefaultChange :
                    self._parseError("Syntax error", token)
                _testDefaults = dict(testDefault)
                testDefaultChange = False
                defaultsSet = True
            # End of input (stop) command.
            elif token.tknTyp & self.SidTypStop :
                doMore = False
            # Configuration data.
            elif token.tknTyp & self.SidTypConfig :
                _configuration = self._parseValue(_configuration, token)
            # Test data.
            elif token.tknTyp & self.SidTypTest :
                if inTest :
                    # Modify the current test data.
                    testDataChanged = True
                    testData = self._parseValue(testData, token)
                else:
                    # Modify the default test data.
                    testDefaultChange = True
                    testDefault = self._parseValue(testDefault, token)
            # Syntax error!
            else:
                self._parseError("Syntax error", token)
            token = self._nextToken()
        # Ensure there are no unsaved changes.
        if inTest :
            VrUtil.errorBeep("Unterminated test.")
            exit(1)
        if testDefaultChange :
            VrUtil.errorBeep("Defaults changed after last test.")
            exit(1)
        #del self.isSavedToken, self.lexer, self.savedToken, self.specialIds

    #----------------------------------------------------------

    def setValue(self, data, name, value) :
        """Set a symbol's value.
        """
        data[name] = value
        return data

    #----------------------------------------------------------
    # List of token names.
    tokens = (
        "VAL_HEX",
        "VAL_UINT",
        "VAL_SINT",
        "VAL_UREAL",
        "VAL_SREAL",
        "VAL_STRING",
        "EQUAL",
        "DELIM",
        "ID",
    )

    #----------------------------------------------------------

    def t_EQUAL(self, token) :
        r"[=:]"
        token.tknTyp    = self.SidTypEqual
        token.tknNam    = token.value

    #----------------------------------------------------------

    def t_DELIM(self, token) :
        r"[,|]"
        token.tknTyp    = self.SidTypDelim
        token.tknNam    = token.value

    #----------------------------------------------------------

    def t_VAL_STRING(self, token) :
        r"(?:\".*?\")|(?:'.*?')|(?:`.*?`)"  #   "..." or '...' or `...`
        token.tknTyp    = self.SidTypValue | self.SidValString
        token.tknNam    = token.value
        # Remove delimiters, expand environment variables and home directory references.
        token.value     = os.path.expanduser(os.path.expandvars(token.value[1 : -1]))
        return token

    #----------------------------------------------------------

    def t_VAL_HEX(self, token) :
        r"0[xX][\dA-Fa-f]+"
        token.tknTyp    = self.SidTypValue | self.SidValHex
        token.tknNam    = token.value
        token.value     = int(token.value, 0)
        return token

    #----------------------------------------------------------

    def t_VAL_SREAL(self, token) :
        r"-(?:(?:\d*\.\d+)|(?:\d+\.\d*))"
        token.tknTyp    = self.SidTypValue | self.SidValUReal
        token.tknNam    = token.value
        token.value     = float(token.value)
        return token

    #----------------------------------------------------------

    def t_VAL_UREAL(self, token) :
        r"(?:\d*\.\d+)|(?:\d+\.\d*)"
        token.tknTyp    = self.SidTypValue | self.SidValUReal
        token.tknNam    = token.value
        token.value     = float(token.value)
        return token

    #----------------------------------------------------------

    def t_VAL_SINT(self, token) :
        r"-\d+"
        token.tknTyp    = self.SidTypValue | self.SidValSInt
        token.tknNam    = token.value
        token.value     = int(token.value)
        return token

    #----------------------------------------------------------

    def t_VAL_UINT(self, token) :
        r"\d+"
        token.tknNam    = token.value
        token.value     = int(token.value)
        token.tknTyp    = self.SidTypValue | self.SidValUInt
        if token.value == 0 or token.value == 1 :
            token.tknTyp |= self.SidValBool
        return token

    #----------------------------------------------------------

    def t_ID(self, token) :
        r"[A-Za-z_][A-Za-z_\d]*"
        for re, tknNam, tknTyp in self.specialIds :
            if re.fullmatch(token.value) :
                token.tknTyp    = tknTyp
                token.tknNam    = tknNam
                if tknTyp == self.SidTypBool :
                    token.value = token.value[0] in "tTyY"
                return token
        else:
            self._error("Invalid symbol", token, token.value)

    #----------------------------------------------------------

    def t_comment(self, token) :
        r"[;#].*"
        pass
        # No return value. Token discarded

    #----------------------------------------------------------
    # Define a rule so we can track line numbers

    def t_newline(self, token) :
        r"\n+"
        token.lexer.lineno += len(token.value)
        # No return value. Token discarded

    #----------------------------------------------------------
    # A string containing ignored characters (spaces and tabs)

    t_ignore = " \t"

    #----------------------------------------------------------
    # Error handling rule

    def t_error(self, token) :
        self._error("Illegal character", token, token.value[0])

    #----------------------------------------------------------

    def _error(self, type, token, text) :
        """Report errors.
        """
        lastNewLine = token.lexer.lexdata.rfind("\n", 0, token.lexpos)
        if lastNewLine < 0 :
            lastNewLine = 0
        VrUtil.errorBeep("{type:s} '{text:}' at line={line:d}, offset={offset:d}" \
                            .format(type = type,
                                    text = text,
                                    line = token.lineno,
                                    offset = token.lexpos - lastNewLine))
        exit(1)

    #----------------------------------------------------------

    def _nextToken(self) :
        """Get the next token.
        """
        if self.isSavedToken :
            answer = self.savedToken
            self.isSavedToken = False
        else:
            answer = self.lexer.token()
            if _Debug.ShowParsedTokens :
                if answer :
                    print("Token: tknNam='{tknNam:}', tknTyp={tknTyp:04X}, value={value:}" \
                            .format(tknNam = answer.tknNam,
                                    tknTyp = answer.tknTyp,
                                    value = answer.value))
                else:
                    print("Token: EOF")
        return answer

    #----------------------------------------------------------

    def _parseError(self, type, token = None) :
        """Handle parsing errors.
        """
        if token :
            self._error(type, token, token.value)
        else:
            VrUtil.errorBeep(type)
            exit(1)

    #----------------------------------------------------------

    def _parseValue(self, data, idToken) :
        """Handle parsing a ID's value.
        """
        # Get value, skipping equal delimiter if needed.
        token = self._nextToken()
        if token.tknTyp & self.SidTypEqual :
            token = self._nextToken()
        # Error if not a value.
        if not token.tknTyp & self.SidTypValue :
            self._parseError("Value expected", token)
        # Error if the wrong type of value for the ID.
        if not (self.SidValMask & idToken.tknTyp & token.tknTyp) :
            self._parseError("Invalid value error", token)
        # Save the named value.
        self.setValue(data, idToken.tknNam, token.value)
        # Skip the delimiter if it is present.
        token = self._nextToken()
        if not token or not (token.tknTyp & self.SidTypDelim) :
            self._push(token)
        return data

    #----------------------------------------------------------

    def _push(self, token) :
        """Push back a token.
        """
        self.isSavedToken = True
        self.savedToken = token
