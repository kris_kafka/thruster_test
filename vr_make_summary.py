import argparse
import atexit
import glob
import operator
import os
import re
import sys
import VrUtil

#=======================================================================================================================

ModuleName          = "vr_make_summary"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 4

AppName             = "Summary Log File Rebuild Tool"
#
#   ProgramStartMessage = "\n{time:s} - Starting summary log file rebuild tool"
#   ProgramExitMessage  = "\n{time:s} - Exiting summary log file rebuild tool\n"
ProgramStartMessage = ""
ProgramExitMessage  = ""

#=======================================================================================================================

# statusCount keys:

SC_Files    = "Files"
SC_Gizmo    = "Gizmo"
SC_Okay     = "Okay"
SC_Open     = "Open"
SC_Read     = "Read"
SC_Status   = "Status"
SC_Test     = "Test"
SC_Time     = "Time"

# summaryData keys:

SD_FailCauses       = "FailCauses"
SD_FirmwareVersion  = "FirmwareVersion"
SD_SerialNumber     = "SerialNumber"
SD_StartTime        = "StartTime"
SD_Status           = "Status"
SD_TestHash         = "TestHash"
SD_TestModified     = "TestModified"
SD_TestVersion      = "TestVersion"

#=======================================================================================================================

class _Config :
    """Application configuration values.
    """
    ShowOpenErrors      = True
    ShowGizmoErrors     = False
    ShowReadErrors      = True
    ShowStatusErrors    = False
    ShowTimeErrors      = False
    #
    FrontSize           = 512       # How much of the initial portion of file to read/parse.
    TailSize            = 512       # How much of the final portion of the file to read/parse.
    #FrontSize          = None      # Read/parse the entire file, also ignores TrailSize.
    #
    CausesNone          = ""
    CausesPrefix        = "  "
    NoTestHash          = "N/A"
    NoTestModified      = "N/A"
    NoTestVersion       = "N/A"
    StatusPass          = "Pass"
    StatusFail          = "FAIL"
    #
    SortKey             = SD_StartTime      # How to sort the summary data, see summaryData keys above.
#   SortKey             = SD_SerialNumber   # How to sort the summary data, see summaryData keys above.

#=======================================================================================================================

# Summary log file information format.
SummaryDetailFormat = "".join([     "{" + SD_StartTime + ":<16s}  ",
                                    "{" + SD_SerialNumber + ":<11s}  ",
                                    "{" + SD_FirmwareVersion + ":<9s}  ",
                                    "{" + SD_TestVersion + ":<16s}  ",
                                    "{" + SD_TestModified + ":<16s}  ",
                                    "{" + SD_TestHash + ":<16s}  ",
                                    "{" + SD_Status + ":<4s}",
                                    "{" + SD_FailCauses + ":s}",
                                    "\n"
                                ])

# Summary log file header.
SummaryHeader = "\n".join([
                "Test Date / Time  Serial #     Version    Test Regime       Last Modified     Hash              Stat  Cause(s)",
                "---------- -----  -----------  ---------  ----------------  ----------------  ----------------  ----  --------",
                ""
            ])

#=======================================================================================================================
# Log file parsing patterns.

EndOfLinePat        = b"[\n\r]+"
EndTimePat          = b"([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}):[0-9]{2}"
FirmwareVersionPat  = b"([?0-9]+\.[?0-9]+\.[0-9]+)"
HashPat             = b"([0-9a-zA-Z]{16})"
RestOfLinePat       = b"([^\n\r]+)"
StartTimePat        = EndTimePat
TestTimePat         = b"([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2})"
WhitespacePat       = b"[\t ]+"

#   Ended:            2017-11-21 16:38:19
#   
#   FAILED at 2017-11-21 16:38:18
#   Cause(s): RPM
FailInfoPat =   EndOfLinePat + \
                b"Ended:" + WhitespacePat + EndTimePat + EndOfLinePat + \
                b"FAILED at[\t0-9: -]+" + EndOfLinePat + \
                b"Cause\(s\):" + WhitespacePat + RestOfLinePat + EndOfLinePat

#   Serial number:      THR000333
#   Node ID:            1
#   Group ID:           128
#   Motor ID:           1
#   Firmware version:   ?.?.0
GizmoInfoPat =  EndOfLinePat + \
                b"Serial number:" + WhitespacePat + RestOfLinePat + EndOfLinePat + \
                b"Node ID:[\t 0-9]+" + EndOfLinePat + \
                b"Group ID:[\t 0-9]+" + EndOfLinePat + \
                b"Motor ID:[\t 0-9]+" + EndOfLinePat + \
                b"Firmware version:" + WhitespacePat + FirmwareVersionPat + EndOfLinePat

#   Ended:            2018-03-08 15:35:34
#   
#   Passed all tests
PassInfoPat =   EndOfLinePat + \
                b"Ended:" + WhitespacePat + EndTimePat + EndOfLinePat + \
                b"Passed all tests" + EndOfLinePat

#   Started:           2018-03-08 15:35:15
StartInfoPat =  EndOfLinePat + \
                b"Started:" + WhitespacePat + StartTimePat + EndOfLinePat

#   Test regime:
#      Version:         1.4.0 Test 1
#      Source:          C:\Users\Kris.Kafka\Projects\ttt\test\thr_test1.txt
#      Last modified:   2018-03-07 19:40
#      Hash:            9da5238813ded46a
TestInfoPat =   EndOfLinePat + \
                b"Test regime:[\t\n\r ]+" + \
                b"Version:" + WhitespacePat + RestOfLinePat + EndOfLinePat +\
                WhitespacePat + b"Source:[^\n\r]+" + EndOfLinePat + \
                WhitespacePat + b"Last modified:" + WhitespacePat + TestTimePat + EndOfLinePat + \
                WhitespacePat + b"Hash:[^\n\r]+" + HashPat + EndOfLinePat

FailInfoRe  = re.compile(FailInfoPat)
GizmoInfoRe = re.compile(GizmoInfoPat)
PassInfoRe  = re.compile(PassInfoPat)
StartInfoRe = re.compile(StartInfoPat)
TestInfoRe  = re.compile(TestInfoPat)

#=======================================================================================================================

class _CsrArgumentParser(argparse.ArgumentParser) :
    """Command line/parameter file argument parser.
    """

    def convert_arg_line_to_args(self, argLine) :
        """Split an indirect file line into separate fields.
        """
        return argLine.split()

#=======================================================================================================================

def appMmain() :
    logPattern, summaryPath, sortBy = commandLineParse()
    #
    atexit.register(notifyExit)
    print(ProgramStartMessage.format(time = VrUtil.utcNowStr()))
    print("Log file pattern: {logPattern:s}\nSummary log file: {summaryPath:s}" \
            .format(logPattern = logPattern, summaryPath = summaryPath))
    #
    statusCounts = {    SC_Files    : 0,
                        SC_Gizmo    : 0,
                        SC_Okay     : 0,
                        SC_Open     : 0,
                        SC_Read     : 0,
                        SC_Status   : 0,
                        SC_Test     : 0,
                        SC_Time     : 0,
                    }
    summaryData = []
    #
    for logPath in glob.iglob(logPattern) :
        readLogFile(logPath, summaryData, statusCounts)
    #
    makeSummaryFile(summaryPath, summaryData, sortBy)
    #
    fmt = "\n".join([
                        "Processed {" + SC_Okay + ":d} of {"+ SC_Files + ":d} log files ({" + \
                                SC_Test + ":d} were old style)",
                        "Skipped:",
                        "   Open error:  {" + SC_Open + ":d}",
                        "   Read error:  {" + SC_Read + ":d}",
                        "   No time:     {" + SC_Time +":d}",
                        "   No gizmo:    {" + SC_Gizmo + ":d}",
                        "   No status:   {" + SC_Status + ":d}",
                    ])
    print(fmt.format(**statusCounts))
    #
    exit(0)

#----------------------------------------------------------

def checkValue(option, key) :
    keyLower = key.lower()
    if len(option) > len(key) :
        return False
    return option == key[0 : len(option)].lower()

#----------------------------------------------------------

def commandLineParse() :
    """Parse and extract command line options.
    """
    parser = _CsrArgumentParser(fromfile_prefix_chars = '@',
                                description = AppName)
    parser.add_argument('-l', '--logPattern',
                        dest = 'logPattern',
                        help = 'Log files pattern, for example: K:\\QA_LOGS\\70503-Thruster\\thr*.log',
                        metavar = 'logPattern',
                        default = '*.log')
    parser.add_argument('-p', '--summaryPath',
                        dest = 'summaryPath',
                        help = 'Summary log file, for example: K:\\QA_LOGS\\70503-Thruster\\summary.txt',
                        metavar = 'summaryPath',
                        default = 'summary.txt')
    parser.add_argument('-s', '--sortBy',
                        dest = 'sortBy',
                        help = 'Sort order for summary log file, valid values: ' + \
                                SD_SerialNumber + ', ' + \
                                SD_StartTime + ', ' + \
                                SD_Status + ', ' + \
                                SD_FirmwareVersion + ', ' + \
                                SD_TestVersion + ', ' + \
                                SD_TestModified + ', ' + \
                                SD_TestHash + ', or ' + \
                                SD_FailCauses,
                        metavar = 'sortBy',
                        default = SD_StartTime)
    parser.add_argument('--version',
                        action = 'store_true',
                        dest = 'showVersion',
                        help = 'Show version information')
    #
    options = parser.parse_args(sys.argv[1:])
    #
    if options.showVersion :
        showVersion()
        exit(1)
    #
    sortBy = options.sortBy.lower()
    if checkValue(sortBy, SD_FailCauses) :
        sortByKey = SD_FailCauses
    elif checkValue(sortBy, SD_FirmwareVersion) :
        sortByKey = SD_FirmwareVersion
    elif checkValue(sortBy, SD_SerialNumber) :
        sortByKey = SD_SerialNumber
    elif checkValue(sortBy, SD_StartTime) :
        sortByKey = SD_StartTime
    elif checkValue(sortBy, SD_Status) :
        sortByKey = SD_Status
    elif checkValue(sortBy, SD_TestHash) :
        sortByKey = SD_TestHash
    elif checkValue(sortBy, SD_TestModified) :
        sortByKey = SD_TestModified
    elif checkValue(sortBy, SD_TestVersion) :
        sortByKey = SD_TestVersion
    else:
        VrUtil.errorBeep("Invalid sortBy value '{}'\n".format(options.sortBy))
        exit(1)
    #
    return options.logPattern, options.summaryPath, sortByKey

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#----------------------------------------------------------

def makeSummaryFile(summaryPath, summaryData, sortBy = _Config.SortKey) :
    """Process a single log file.

    Arguments:
        summaryPath     Summary log file path.
        summaryData     Accumulated Summary data.
        sortBy          Name of field to sort by.
    """
    # Create an empty file.
    try:
        summaryFile = open(summaryPath, 'w')
    except (IOError, OSError) as error :
        VrUtil.errorBeep("Error creating summary log file '{}'\n{}\n".format(summaryPath, error))
        exit(1)
    # Write the header.
    print(SummaryHeader, end = '', file = summaryFile)
    # Update the summary log file.
    try:
        for details in sorted(summaryData, key=operator.itemgetter(sortBy)) :
            print(SummaryDetailFormat.format(**details), end = '', file = summaryFile)
    except (IOError, OSError) as error :
        VrUtil.errorBeep("Error updating the summary log file\n{}\n".format(error))
        exit(1)
    #
    try:
        summaryFile.close()
    except (IOError, OSError) as error :
        VrUtil.errorBeep("Error closing the summary log file\n{}\n".format(error))
        exit(1)
    return

#----------------------------------------------------------

def notifyExit() :
    """Notify when the application exits.
    """
    VrUtil.messageBeep(ProgramExitMessage.format(time = VrUtil.utcNowStr()))

#----------------------------------------------------------

def readLogFile(logPath, summaryData, statusCounts) :
    """Process a single log file.

    Arguments:
        logPath         File/path of the log file to process.
        summaryData     Accumulated Summary data.
        statusCounts    Processing status counts.
    """
    statusCounts[SC_Files] += 1
    # Open the log file.
    try:
        logFile = open(logPath, 'rb')
    except (IOError, OSError) as error :
        statusCounts[SC_Open] += 1
        if _Config.ShowOpenErrors :
            VrUtil.errorMessage("Error opening log file: {}\n{}\n".format(logPath, error))
        return
    # Read the log file.
    try:
        frontText = logFile.read(_Config.FrontSize)
        if _Config.FrontSize :
            logFile.seek(-_Config.TailSize, 2)
            tailText = logFile.read(_Config.TailSize)
        else:
            tailText = frontText
    except (IOError, OSError) as error :
        statusCounts[SC_Read] += 1
        if _Config.ShowReadErrors :
            VrUtil.errorMessage("Error reading log file: {}\n{}\n".format(logPath, error))
        logFile.close()
        return
    logFile.close()
    # Parse the file data.
    details = {}
    # Extract the start time.
    startInfo   = StartInfoRe.search(frontText)
    if startInfo is None :
        statusCounts[SC_Time] += 1
        if _Config.ShowTimeErrors :
            VrUtil.errorMessage("No start time found in {}\n".format(logPath))
        return
    details[SD_StartTime] = startInfo.group(1).decode()
    # Extract the gizmo information exists.
    gizmoInfo   = GizmoInfoRe.search(frontText)
    if gizmoInfo is None :
        statusCounts[SC_Gizmo] += 1
        if _Config.ShowGizmoErrors :
            VrUtil.errorMessage("No gizmo found in {}\n".format(logPath))
        return
    details[SD_SerialNumber]    = gizmoInfo.group(1).decode()
    details[SD_FirmwareVersion] = gizmoInfo.group(2).decode()
    # Extract the status information.
    passInfo    = PassInfoRe.search(tailText)
    failInfo    = FailInfoRe.search(tailText)
    if passInfo is not None :
        details[SD_Status]      = _Config.StatusPass
        details["endTime"]      = passInfo.group(1).decode()
        details[SD_FailCauses]  = _Config.CausesNone
    elif failInfo is not None :
        details[SD_Status]      = _Config.StatusFail
        details["endTime"]      = failInfo.group(1).decode()
        details[SD_FailCauses]  = _Config.CausesPrefix + failInfo.group(2).decode()
    else:
        statusCounts[SC_Status] += 1
        if _Config.ShowStatusErrors :
            VrUtil.errorMessage("No status found in {}\n".format(logPath))
        return
    # Extract the test regime information.
    testInfo = TestInfoRe.search(frontText)
    if testInfo is not None :
        details[SD_TestVersion]     = testInfo.group(1).decode()
        details[SD_TestModified]    = testInfo.group(2).decode()
        details[SD_TestHash]        = testInfo.group(3).decode()
    else:
        details[SD_TestVersion]     = _Config.NoTestVersion
        details[SD_TestModified]    = _Config.NoTestModified
        details[SD_TestHash]        = _Config.NoTestHash
        statusCounts[SC_Test] += 1
    #
    summaryData.append(details)
    statusCounts[SC_Okay] += 1
    return

#----------------------------------------------------------

def showVersion() :
    """Show the application version information.
    """
    print("{appName:s} {majorVersion:d}.{minorVersion:d}.{microVersion:d}" \
            .format(appName = AppName,
                        majorVersion = ModuleVersionMajor,
                        minorVersion = ModuleVersionMinor,
                        microVersion = ModuleVersionMicro))
    #
    modules = []
    modules.append(getModuleInformation())
    modules.append(VrUtil.getModuleInformation())

    #
    fmt = "{Name:<" + str(max([len(module["Name"]) for module in modules ])) + "s}" + \
        "   {VersionMajor:d}.{VersionMinor:d}.{VersionMicro:d}" + \
        "  {FileHash:s}  {LastModified:s}  {FilePath:s}"
    print("Module versions:")
    for module in sorted(modules, key = lambda module : module["Name"]) :
        print(fmt.format(**module))

#=======================================================================================================================

if __name__ == "__main__":
    appMmain()
