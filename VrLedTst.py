import math
import Vr
import VrLed
import VrUtil
from VrTst import *

#=======================================================================================================================

SubModuleName           = "VrLedTst"

# TODO: Update version number.
SubModuleVersionMajor   = 1
SubModuleVersionMinor   = 5
SubModuleVersionMicro   = 5

#=======================================================================================================================

# Gizmo name
GizmoNamePluralCap      = "LEDs"
GizmoNamePluralLwr      = "LEDs"
GizmoNameSingularCap    = "LED"
GizmoNameSingularLwr    = "LED"

#=======================================================================================================================

def findGizmos(appInf, port) :
    """Find all of the LED gizmos.
    """
    return VrGizmo.findGizmos(port,
                                Vr.Devices.Led,
                                VrLed.Gizmo.getLedId,
                                lambda gizmo, port, appInf = appInf : \
                                            Gizmo(appInf,
                                                    nodeId = gizmo.nodeId,
                                                    groupId = gizmo.groupId,
                                                    serialNumber = gizmo.serialNumber,
                                                    port = port))

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(SubModuleName,
                                        __file__,
                                        SubModuleVersionMajor,
                                        SubModuleVersionMinor,
                                        SubModuleVersionMicro)

#=======================================================================================================================

class Gizmo(VrLed.Gizmo, VrGizmo) :
    """VideoRay test LED gizmo information and functions.
    """
    ParseSize       = VrLed.ReplySetLevel.ParseSize
    LevelCount      = VrLed.ReplySetLevel.LevelCount
    DevicesType     = Vr.Devices.Led
    FaultCauses     = [
                            "Voltage",
                            "Current",
                            "Temperature",
                            "Faults",
                            "Actual1",
                            "Actual2",
                            "Actual3",
                        ]
    Initialized     = False

    #----------------------------------------------------------

    def __init__(self, appInf, nodeId = -1, groupId = -1, serialNumber = "", gizmoId = -1, port = None) :
        """Initialize a LED test gizmo object.

        Arguments:
            appInf         Application information.
            nodeId          Optional node ID.
            groupId         Optional group ID
            serialNumber    Optional serial number.
            gizmoId         Optional LED ID.
            port            Optional serial port associated with this device.
        """
        self._initPollSubrs()
        #
        VrLed.Gizmo.__init__(self, nodeId, groupId, serialNumber, gizmoId)
        VrGizmo.__init__(self, appInf, port)
        #
        self.errorCounts    = 4 * [0]

    #----------------------------------------------------------
    @classmethod
    def _initPollSubrs(cls) :
        """Initialize the poll() subrs.
        """
        if cls.Initialized :
            return
        #
        chkAbsFp    = [lambda t : (not math.isnan(t[0])) and t[1] <= abs(t[0]) and abs(t[0]) <= t[2]]
        chkFloat    = [lambda t : (not math.isnan(t[0])) and t[1] <= t[0] and t[0] <= t[2]]
        chkUint     = [lambda t : t[2] == t[0] & t[1]]
        maxFloat    = [lambda t : max(t) if not math.isnan(t[1]) else t[0]]
        maxUint     = [any]
        minFloat    = [lambda t : min(t) if not math.isnan(t[1]) else t[0]]
        minUint     = [all]
        addFloat    = [lambda t : sum(t) if not math.isnan(t[1]) else t[0]]
        addUint     = [any]
        pckFloat    = [lambda t : t[1] if t[0] else t[2]]
        pckUint     = pckFloat
        #                       Voltage     Current     Temp        Faults      Actuals
        cls.CheckSubrs      =   chkAbsFp +  chkAbsFp +  chkFloat +  chkUint +   chkFloat * cls.LevelCount
        cls.MaximumSubrs    =   maxFloat +  maxFloat +  maxFloat +  maxUint +   maxFloat * cls.LevelCount
        cls.MinimumSubrs    =   minFloat +  minFloat +  minFloat +  minUint +   minFloat * cls.LevelCount
        cls.AdditionSubrs   =   addFloat +  addFloat +  addFloat +  addUint +   addFloat * cls.LevelCount
        cls.PickSubrs       =   pckFloat +  pckFloat +  pckFloat +  pckUint +   pckFloat * cls.LevelCount
        #
        cls.Initialized = True

    #----------------------------------------------------------

    def initializeForTest(self, test) :
        """Set the min/max valid status values for a given test.

        Arguments:
            test    Test data.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        #
        self.minValid = [
                            test[gzmCfg.ValidVoltageMinimum],
                            test[gzmCfg.ValidCurrentMinimum],
                            test[gzmCfg.ValidTempMinimum],
                            test[gzmCfg.ValidFaultsErrorMask],
                        ]
        self.minValid.extend(self.LevelCount * [test[gzmCfg.ValidSetpointMinimum]])
        self.maxValid = [
                            test[gzmCfg.ValidVoltageMaximum],
                            test[gzmCfg.ValidCurrentMaximum],
                            test[gzmCfg.ValidTempMaximum],
                            test[gzmCfg.ValidFaultsOkayValue],
                        ]
        self.maxValid.extend(self.LevelCount * [test[gzmCfg.ValidSetpointMaximum]])

    #----------------------------------------------------------

    def poll(self, isStartup, testNumber, test) :
        """Poll a LED gizmo.

        Arguments:
            isStartup   True during test startup time period.
            testNumber  Number [1, ...] of the test.
            test        Test info.
        """
        if self.isFault :
            return
        #
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmLog = appInf.GizmoLogModule
        #
        now = VrUtil.utcNow()
        reply = self.setLevel(self.port,
                                setpoint1 = test[gzmCfg.Setpoint1],
                                setpoint2 = test[gzmCfg.Setpoint2],
                                setpoint3 = test[gzmCfg.Setpoint3])
        #
        if reply :
            current = reply.parse()
            if current[VrLed.ReplySetLevel.ReplyFaults] :
                self.updateErrorCounts()
            gzmLog.Log(self.appInf) \
                    .recordDetailFor(self, now, isStartup, current, self.errorCounts, testNumber, test)
        if isStartup :
            return
        if reply is not None :
            self.pollCount      += 1
            self.totalPollCount += 1
            self.lastReplyTime  = now
            okay                = VrGizmo._applyAll(self.CheckSubrs, current, self.minValid, self.maxValid)
            self.lastOkTime     = VrGizmo._applyAll(self.PickSubrs,
                                                    okay,
                                                    self.ParseSize * [now],
                                                    self.lastOkTime)
            self.okayTest       = VrGizmo._applyOne(sum, self.okayTest, okay)
            #
            okay                = [test[gzmCfg.KeepOutOfRange] or ok for ok in okay]
            self.cntTest        = VrGizmo._applyOne(sum, self.cntTest, okay)
            self.cntInterval    = VrGizmo._applyOne(sum, self.cntInterval, okay)
            self.sumInterval    = VrGizmo._applyIf(self.AdditionSubrs, okay, self.sumInterval, current)
            self.sumTest        = VrGizmo._applyIf(self.AdditionSubrs, okay, self.sumTest, current)
            self.minimum        = VrGizmo._applyChk(self.MinimumSubrs, okay, self.minimum, current)
            self.maximum        = VrGizmo._applyChk(self.MaximumSubrs, okay, self.maximum, current)
            self.failureCause   = ", ".join([cause for lastOk, cause in zip(self.lastOkTime, self.FaultCauses)
                                                    if (now - lastOk).total_seconds() > test[gzmCfg.FaultPeriod]])
        elif (now - self.lastReplyTime).total_seconds() > test[gzmCfg.FaultPeriod] :
            self.failureCause = "Not responding"
        #
        if self.failureCause :
            self.stopGizmo()
            self.isFault        = True
            self.failureTime    = now
            VrUtil.errorBeep(self.GizmoFailure \
                                    .format(serialNumber = self.serialNumber,
                                            nodeId = self.nodeId,
                                            groupId = self.groupId,
                                            gizmoId = self.gizmoId,
                                            cause = self.failureCause))

    #----------------------------------------------------------

    def stopGizmo(self) :
        """Stop the gizmo.
        """
        if self.isFault :
            return
        self.stopLed(self.port)
