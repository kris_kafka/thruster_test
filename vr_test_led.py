import importlib
import sys
import VrUtil
from VrApp import *

#=======================================================================================================================

ModuleName  = "vr_test_led"
GizmoPrefix = "VrLed"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 4
ModuleVersionMicro  = 0

# References to other modules.

MainModule      = sys.modules[__name__]
AppModule       = importlib.import_module("VrApp")
VrModule        = importlib.import_module("Vr")
VrUtilModule    = importlib.import_module("VrUtil")

GizmoModule     = importlib.import_module(GizmoPrefix)
GizmoCfgModule  = importlib.import_module(GizmoPrefix + "Cfg")
GizmoLogModule  = importlib.import_module(GizmoPrefix + "Log")
GizmoTstModule  = importlib.import_module(GizmoPrefix + "Tst")

CfgModule       = importlib.import_module("VrCfg")
LogModule       = importlib.import_module("VrLog")
TstModule       = importlib.import_module("VrTst")

# Application information.

AppName             = GizmoTstModule.GizmoNameSingularCap + " Test Tool"
ProgramStartMessage = "\n{time:s} - Starting " + GizmoTstModule.GizmoNameSingularLwr + " test tool"
ProgramExitMessage  = "\n{time:s} - Exiting " + GizmoTstModule.GizmoNameSingularLwr + " test tool\n"

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#=======================================================================================================================

if __name__ == "__main__":
    VrApp(MainModule).main()
