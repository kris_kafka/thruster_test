import struct
import Vr
import VrUtil

#=======================================================================================================================

GizmoModuleName         = "VrThr"

# TODO: Update version number.
GizmoModuleVersionMajor = 1
GizmoModuleVersionMinor = 4
GizmoModuleVersionMicro = 0

#=======================================================================================================================

class _Config :
    """Thruster configuration values.
    """
    # Note:
    #   Times values are in given in seconds.
    #   Temperature values are in given in degrees celsius.
    #
    # Bad temperature processing.
    KeepBadTemps        = False         # Keep invalid temperatures?
    BadTempReplacement  = float("nan")  # Replacement for invalid temperatures.
    Mcp9800MinTemp      = -55.0         # Minimum temperature from Microchip MCP9800 data sheet.
    Mcp9800MaxTemp      = 125.0         # Maximum temperature from  Microchip MCP9800 data sheet.
    Mcp9800Resolution   = 0.0625        # Temperature resolution from Microchip MCP9800 data sheet.
    #
    PropulsionRetry     = Vr.Gizmo.CsrWriteRetry    # Maximum number of attempts to perform a propulsion command.
    PropulsionTimeout   = Vr.Gizmo.CsrWriteTimeout  # Maximum time to wait for a reply to a propulsion command.
    StopRetry           = 5                         # Maximum number of times to attempt to stop a thruster.

#=======================================================================================================================

def fromGizmo(gizmo) :
    """Return a VrThr.Gizmo created from a generic Vr.Gizmo
    """
    return Gizmo(gizmo.nodeId, gizmo.groupId, gizmo.serialNumber)

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(GizmoModuleName,
                                        __file__,
                                        GizmoModuleVersionMajor,
                                        GizmoModuleVersionMinor,
                                        GizmoModuleVersionMicro)

#=======================================================================================================================

def _checkTemp(value) :
    """Check if value is a valid temperature.
    """
    # Is the temperature something we can get from the Microchip MCP9800?
    return _Config.Mcp9800MinTemp <= value \
        and value <= _Config.Mcp9800MaxTemp \
        and 0 == value % _Config.Mcp9800Resolution

#=======================================================================================================================

class CommandPropulsion(Vr.Command) :
    """VideoRay thruster custom command packets.
    """
    CommandPropulsion   = 0xAA
    PropulsionFlags     = 0x02

    #----------------------------------------------------------

    def __init__(self, networkId, replierId, motorCount, percentage) :
        """Initialize a propulsion command packet.

        Returns the generated packet.
        """
        payload = bytearray([CommandPropulsion.CommandPropulsion, replierId]) \
                + struct.pack(Vr.Bytes.Real4, percentage / 100) * motorCount
        super().__init__(networkId, self.CustomCommand, CommandPropulsion.PropulsionFlags, payload)

#=======================================================================================================================

class ReplyErrorCounts(Vr.Reply) :
    """Error counts reply.
    """
    CsrOffset           = 172
    CountsSize          = 4
    #
    UnderVoltageOffset  = 0 * CountsSize
    OverVoltageOffset   = 1 * CountsSize
    OverCurrentOffset   = 2 * CountsSize
    OverTempOffset      = 3 * CountsSize
    StallOffset         = 4 * CountsSize
    MinimumLength       = 5 * CountsSize

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return super().isPayloadDataValid() and self.length() >= (self.MinimumLength +  self.deviceTypeSize())

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data.
        """
        return self.uIntAt(self.UnderVoltageOffset, self.CountsSize), \
                self.uIntAt(self.OverVoltageOffset, self.CountsSize), \
                self.uIntAt(self.OverCurrentOffset, self.CountsSize), \
                self.uIntAt(self.OverTempOffset, self.CountsSize), \
                self.uIntAt(self.StallOffset, self.CountsSize)

#=======================================================================================================================

class ReplyPropulsion(Vr.Reply) :
    """VideoRay thruster propulsion reply information and functions.
    """
    # Status reply packet offsets
    RpmOffset       = 0
    RpmSize         = 4
    VoltageOffset   = RpmSize + RpmOffset
    VoltageSize     = 4
    CurrentOffset   = VoltageSize + VoltageOffset
    CurrentSize     = 4
    TempOffset      = CurrentSize + CurrentOffset
    TempSize        = 4
    FaultsOffset    = TempSize + TempOffset
    FaultsSize      = 1
    MinimumLength   = FaultsSize + FaultsOffset
    ParseFloats     = 4
    ParseInts       = 1
    ParseSize       = ParseFloats + ParseInts
    #
    # Reply indexes
    ReplyRpm        = 0
    ReplyVoltage    = 1
    ReplyCurrent    = 2
    ReplyTemp       = 3
    ReplyFaults     = 4

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return super().isPayloadDataValid() and self.length() >= (self.MinimumLength +  self.deviceTypeSize())

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data: RPM, Bus Voltage, Bus Current, Temperature, Faults Flags
        """
        temp = self.float4At(self.TempOffset)
        return self.float4At(self.RpmOffset), \
                self.float4At(self.VoltageOffset), \
                self.float4At(self.CurrentOffset), \
                temp if _Config.KeepBadTemps or _checkTemp(temp) else _Config.BadTempReplacement, \
                self.uInt1At(self.FaultsOffset)

#=======================================================================================================================

class Gizmo(Vr.Gizmo) :
    """VideoRay thruster gizmo information and functions.
    """
    # ID limits
    MinMotorId          = 0
    MaxMotorId          = 59
    #
    MotorIdOffset       = 76
    MotorIdSize         = 2
    #
    _FieldInfo  = \
    [
    # Offset Size   Unpack          Width   Decimal Prefix  Format  Valid,      Name
        (0,     4,  Vr.Bytes.Real4, 10,     0,      " ",    "f",    None,       "rpm_target"),
        (4,     4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "pwr_target"),
        (8,     4,  Vr.Bytes.Real4, 10,     0,      " ",    "f",    None,       "rpm"),
        (12,    4,  Vr.Bytes.Real4, 10,     2,      " ",    "f",    None,       "bus_v"),
        (16,    4,  Vr.Bytes.Real4, 10,     3,      " ",    "f",    None,       "bus_i"),
        (20,    4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault"),
        (24,    4,  Vr.Bytes.Real4, 10,     1,      " ",    "f",    _checkTemp, "temp"),
        (28,    4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "pwr_actual"),
        (32,    4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_P"),
        (36,    4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_I"),
        (40,    4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_D"),
        (44,    4,  None,           None,   None,   None,   None,   None,       "reserved"),
        (48,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (56,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (64,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (72,    4,  None,           None,   None,   None,   None,   None,       "reserved"),
        (76,    2,  Vr.Bytes.UInt2, 5,      None,   "",     "d",    None,       "motor_ID"),
        (78,    2,  None,           None,   None,   None,   None,   None,       "reserved"),
        (80,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "system_flags"),
        (81,    3,  None,           None,   None,   None,   None,   None,       "reserved"),
        (84,    4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault_interlock"),
        (88,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (96,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "control_flags"),
        (97,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "motor_control_flags"),
        (98,    1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "poles"),
        (99,    1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "pwm_deadband"),
        (100,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "commutation_threshold"),
        (104,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "commutation_loss_timeout"),
        (108,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "startup_dutycycle"),
        (112,   2,  Vr.Bytes.UInt2, 5,      None,   "",     "d",    None,       "startup_initial_rpm"),
        (114,   2,  Vr.Bytes.UInt2, 5,      None,   "",     "d",    None,       "startup_final_rpm"),
        (116,   4,  Vr.Bytes.Real4, 10,     3,      " ",    "f",    None,       "startup_duration"),
        (120,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "deadband_neg"),
        (124,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "deadband_pos"),
        (128,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "dutycycle_limit_neg"),
        (132,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "dutycycle_limit_pos"),
        (136,   4,  Vr.Bytes.Real4, 10,     4,      " ",    "f",    None,       "acceleration_rate"),
        (140,   4,  Vr.Bytes.Real4, 10,     4,      " ",    "f",    None,       "deceleration_rate"),
        (144,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_kP"),
        (148,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_kI"),
        (152,   4,  Vr.Bytes.Real4, 10,     7,      " ",    "f",    None,       "rpm_kD"),
        (156,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "max_allowable_power"),
        (160,   4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault_control"),
        (164,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "undervoltage_trigger"),
        (165,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "overvoltage_trigger"),
        (166,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "overcurrent_trigger"),
        (167,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "temp_trigger"),
        (168,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "stall_count_max"),
        (169,   3,  None,           None,   None,   None,   None,   None,       "reserved"),
        (172,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "undervoltage_err_cnt"),
        (176,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "overvoltage_err_cnt"),
        (180,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "overcurrent_err_cnt"),
        (184,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "temp_err_cnt"),
        (188,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "stall_err_cnt"),
        (192,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (200,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (208,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (216,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_sync1_err_cnt"),
        (220,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_sync2_err_cnt"),
        (224,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_headerxsum_err_cnt"),
        (228,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_overrun_err_cnt"),
        (232,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_payloadxsum_err_cnt"),
        (236,   2,  Vr.Bytes.UInt2, 4,      None,   "0",    "X",    None,       "comms_err_flag"),
        (238,   2,  Vr.Bytes.UInt2, 4,      None,   "0",    "X",    None,       "save_settings"),
    ]

    #----------------------------------------------------------

    def __init__(self, nodeId = -1, groupId = -1, serialNumber = "", gizmoId = -1) :
        """Initialize a VRThruster.Gizmo object.

        Arguments:
            nodeId          Optional node ID.
            groupId         Optional group ID
            serialNumber    Optional serial number.
            gizmoId         Optional motor ID.
        """
        self.gizmoId = gizmoId
        #
        super().__init__(nodeId, groupId, serialNumber, Vr.Devices.Thruster)

    #----------------------------------------------------------

    def checkIfOkay(self) :
        """Check if the gizmo information is okay.

        Returns list of reasons why the device is not okay.
        """
        answer = super().checkIfOkay()
        if self.gizmoId < self.MinMotorId or self.MaxMotorId < self.gizmoId :
            answer.append("Motor ID")
        return answer

    #----------------------------------------------------------

    def getErrorCounts(self, port) :
        """Return the thruster error counts or None.
        """
        reply = ReplyErrorCounts()
        reply = self.csrRead(port, reply.CsrOffset, reply.MinimumLength, reply = reply)
        return reply.parse() if reply else None

    #----------------------------------------------------------

    def getFieldInfo(self) :
        """Return the thruster's CSR field info.
        """
        return self._FieldInfo

    #----------------------------------------------------------

    def getMotorId(self, port) :
        """Get the motor ID from the device.

        Arguments:
            port    Serial port.

        Return True if no errors else False.
        """
        reply = self.csrRead(port, self.MotorIdOffset, self.MotorIdSize)
        if reply is None  :
            VrUtil.errorBeep("No reply getting motor ID from nodeId={nodeId:d}, serialNumber='{serialNumber:s}' " \
                                    .format(nodeId = self.nodeId, serialNumber = self.serialNumber))
            return False
        if reply.length() < reply.deviceTypeSize() + self.MotorIdSize :
            VrUtil.errorBeep("Bad reply getting motor ID from nodeId={nodeId:d}, serialNumber='{serialNumber:s}'" \
                                    .format(nodeId = self.nodeId, serialNumber = self.serialNumber))
            return False
        self.gizmoId = reply.uIntAt(0, self.MotorIdSize)
        return True

    #----------------------------------------------------------

    def propulsion(self, port, percentage) :
        """Poll a thruster.

        Arguments:
            port        Serial port.
            percentage  Thruster power percentage.

        Returns the reply packet or None.
        """
        if self.ioError :
            return None
        command = CommandPropulsion(self.nodeId, self.nodeId, self.nodeId + 1, percentage)
        reply = ReplyPropulsion()
        retriesLeft = _Config.PropulsionRetry
        while retriesLeft :
            command.send(port)
            if reply.getReply(port, timeout = _Config.PropulsionTimeout, replyId = self.nodeId) :
                return reply
            retriesLeft -= 1
        self.ioError = True
        return None

    #----------------------------------------------------------

    def stopMotor(self, port) :
        """Stop the thruster.

        Arguments:
            port    Serial port.
        """
        if self.ioError :
            return
        retriesLeft = _Config.StopRetry
        while retriesLeft :
            if None is not self.propulsion(port, 0.0) :
                return
            retriesLeft -= 1
