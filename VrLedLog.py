import VrUtil
from VrLog import *

#=======================================================================================================================

SubModuleName           = "VrLedLog"

# TODO: Update version number.
SubModuleVersionMajor   = 1
SubModuleVersionMinor   = 4
SubModuleVersionMicro   = 0

#=======================================================================================================================

def formatTestData(appInf, test, prefixSize) :
    """Show the test information.

    Arguments:
        appInf     Application information.
        test        Test data.
        prefixSize  Size of the label prefix.

    Returns formatted text and list of fields processed.
    """
    gzmCfg = appInf.GizmoCfgModule
    text = ("{label:<" + str(prefixSize) + "s}{testSetpoint1:.1f}%, {testSetpoint2:.1f}%, {testSetpoint3:.1f}%") \
            .format(label = "Setpoints:",
                    testSetpoint1 = test[gzmCfg.Setpoint1],
                    testSetpoint2 = test[gzmCfg.Setpoint2],
                    testSetpoint3 = test[gzmCfg.Setpoint3])
    fields = [gzmCfg.Setpoint1, gzmCfg.Setpoint2, gzmCfg.Setpoint3]
    return text, fields

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(SubModuleName,
                                        __file__,
                                        SubModuleVersionMajor,
                                        SubModuleVersionMinor,
                                        SubModuleVersionMicro)

#=======================================================================================================================

class Log(VrLog) :

    #----------------------------------------------------------

    def __init__(self, appInf) :
        """Initialize a Log object.
        """
        self.appInf = appInf

    #----------------------------------------------------------

    def makeCsvFileHeader(self, gizmo) :
        """Return the file header text.

        Arguments:
            gizmo   Gizmo to the header text is for.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        text = [
                    "Timestamp, ",
                    "Number, ",
                    "Setpoint1, ",
                    "Setpoint2, ",
                    "Setpoint3, ",
                    "IsStartup, ",
                    "Actual1, ",
                    "Actual2, ",
                    "Actual3, ",
                    "Voltage, ",
                    "Current, ",
                    "Temperature, ",
                    "Faults"]
        if gzmCfg.config(gzmCfg.CsvErrorCounts) :
            text.extend([
                            ", UnderVoltage",
                            ", OverVoltage",
                            ", OverCurrent",
                            ", OverTemp"
                        ])
        text.append("\n")
        return "".join(text)

    #----------------------------------------------------------

    def makeCsvFileRecord(self, gizmo, time, isStartup, reply, counts, testNumber, test) :
        """Return the CSV record text.

        Arguments:
            gizmo       Gizmo.
            time        Time of the status reply.
            isStartup   True during test startup time period.
            reply       Propulsion command status reply.
            counts      Error counts.
            testNumber  Number [1, ...] of the test.
            test        Test info.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        csvRecord   = [
                        "{timestamp:s}",
                        "{number:d}",
                        "{setpoint1:.1f}",
                        "{setpoint2:.1f}",
                        "{setpoint3:.1f}",
                        "{isStartup:s}",
                        "{actual1:.1f}",
                        "{actual2:.1f}",
                        "{actual3:.1f}",
                        "{voltage:.2f}",
                        "{current:.3f}",
                        "{temperature:.1f}",
                        "{faults:d}",
                        ]
        if gzmCfg.config(gzmCfg.CsvErrorCounts) :
            csvRecord.extend([
                                "{underVoltage:d}",
                                "{overVoltage:d}",
                                "{overCurrent:d}",
                                "{overTemp:d}\n"
                            ])
        else:
            csvRecord.append("{faults:d}\n")
        voltage, current, temp, faults, actual1, actual2, actual3 = reply
        return ", ".join(csvRecord) \
                    .format(timestamp = (time.isoformat(sep = 'T') + ".000")[0 : 23],
                            number = testNumber,
                            setpoint1 = test[gzmCfg.Setpoint1],
                            setpoint2 = test[gzmCfg.Setpoint2],
                            setpoint3 = test[gzmCfg.Setpoint3],
                            duration = test[gzmCfg.Duration],
                            interval = formatSeconds(test[gzmCfg.Interval], 1),
                            pause = test[gzmCfg.PauseAfterTest],
                            isStartup = "Y" if isStartup else "N",
                            actual1 = actual1,
                            actual2 = actual2,
                            actual3 = actual3,
                            voltage = voltage,
                            current = current,
                            temperature = temp,
                            faults = faults,
                            underVoltage = counts[0],
                            overVoltage = counts[1],
                            overCurrent = counts[2],
                            overTemp = counts[3])

    #----------------------------------------------------------

    def makeLogFileCsrData(self, gizmo, csr) :
        """Return the formatted CSR text.

        Arguments:
            gizmo   Gizmo.
            csr     CSR data.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        showHex = gzmCfg.config(gzmCfg.LogCsrDataHex)
        nameSize, valueSize, csrText = gizmo.csrFormat(csr, showHex, showHex)
        text = [
                    "\n",
                    ("=" * 80), "\n",
                    "\n",
                    "CSR Data Dump\n",
                    "Off  ", "Name".ljust(nameSize), "  ", "Value".ljust(valueSize), "   ", "Hex",             "\n",
                    "---  ", "-" * nameSize,         "  ", "-" * valueSize,          "   ", "-" * (8 * 3 - 1), "\n",
                    csrText, "\n",
                ]
        return "".join(text)

    #----------------------------------------------------------

    def makeLogFileCsrHex(self, gizmo, csr) :
        """Return the formatted CSR text.

        Arguments:
            gizmo   Gizmo.
            csr     CSR data.
        """
        fmt = "".join([
                        "{address:02X}   ",
                        "{data[0]:02X} ",
                        "{data[1]:02X} ",
                        "{data[2]:02X} ",
                        "{data[3]:02X} . ",
                        "{data[4]:02X} ",
                        "{data[5]:02X} ",
                        "{data[6]:02X} ",
                        "{data[7]:02X} : ",
                        "{data[8]:02X} ",
                        "{data[9]:02X} ",
                        "{data[10]:02X} ",
                        "{data[11]:02X} . ",
                        "{data[12]:02X} ",
                        "{data[13]:02X} ",
                        "{data[14]:02X} ",
                        "{data[15]:02X}\n",
                    ])
        text = [
                    "\n",
                    ("=" * 80), "\n",
                    "\n",
                    "CSR Hex Dump\n",
                    "Off  00 01 02 03 . 04 05 06 07 : 08 09 0A 0B . 0C 0D 0E 0F\n",
                    "---  -- -- -- -- . -- -- -- -- : -- -- -- -- . -- -- -- --\n",
                ]
        #
        lineSize = 16
        for base in range(0, len(csr), lineSize) :
            text.append(fmt.format(address = base, data = csr[base : base + lineSize]))
        #
        text.extend([
                        "---- -- -- -- -- . -- -- -- -- : -- -- -- -- . -- -- -- --\n",
                        "Off  00 01 02 03 . 04 05 06 07 : 08 09 0A 0B . 0C 0D 0E 0F\n"
                    ])
        return "".join(text)

    #----------------------------------------------------------

    def makeLogFileErrorCount(self, gizmo) :
        """Return the gizmo error counts text.

        Arguments:
            gizmo   Gizmo to log the status for.
        """
        underVoltage, overVoltage, overCurrent, overTemp = gizmo.errorCounts
        fmt = [
                    "\n",
                    "Error counts\n",
                    "Condition              Count\n",
                    "Under voltage     {underVoltage:10d}\n",
                    "Over voltage      {overVoltage:10d}\n",
                    "Over current      {overCurrent:10d}\n",
                    "Over temperature  {overTemp:10d}\n",
                ]
        return "".join(fmt).format(underVoltage = underVoltage,
                                    overVoltage = overVoltage,
                                    overCurrent = overCurrent,
                                    overTemp = overTemp)

    #----------------------------------------------------------

    def makeLogFileHeader(self, gizmo, allTests) :
        """Return the file header text.

        Arguments:
            gizmo       Gizmo to the header text is for.
            allTests    Tests info.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        fmt = [
                    "VideoRay LED QA Test",
                    "",
                    "Started:           {startTime:s}",
                    "",
                    "{standardInfo:s}",
                    "",
                ]
        if gzmCfg.config(gzmCfg.LogPollCounts) :
            fmt.extend([
                            "{moduleVersions:s}",
                            ""
                        ])
        fmt.extend([
                        ("-" * 80),
                        "",
                        "Tests:",
                        " #     Setpoints                    Duration    Sample    Pause    Stop",
                        "",
                    ])
        fmt = "\n".join(fmt)
        text = [
                    fmt.format(startTime = gizmo.startTime,
                                standardInfo = self.makeLogStandardInfo(gizmo),
                                moduleVersions = formatModuleVersions(self.appInf, nameSize = 20)),
                ]
        fmt = "".join([
                            "{number:2d}   ",
                            "{setpoint1:6.1f}%   ",
                            "{setpoint2:6.1f}%   ",
                            "{setpoint3:6.1f}%   ",
                            "{duration:>8s}   ",
                            "{interval:>7s}    ",
                            "{pause:>7s}   ",
                            "{stop:>4s}\n",
                        ])
        for number, test in enumerate(allTests, start = 1) :
            text.append(fmt.format(number = number,
                                    setpoint1 = test[gzmCfg.Setpoint1],
                                    setpoint2 = test[gzmCfg.Setpoint2],
                                    setpoint3 = test[gzmCfg.Setpoint3],
                                    duration = formatSecondsAsHhMmSs(test[gzmCfg.Duration]),
                                    interval = formatSeconds(test[gzmCfg.Interval], 1),
                                    pause = formatSeconds(test[gzmCfg.PauseAfterTest], 1),
                                    stop = "Yes" if test[gzmCfg.StopAfterTest] else "No"))
        #
        return "".join(text)

    #----------------------------------------------------------

    def makeLogFileStatus(self, gizmo, testTime, time) :
        """Return the gizmo status text.

        Arguments:
            gizmo       Gizmo to log the status for.
            testTime    Time when the test started.
            time        Number of seconds since the start of the current test.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        voltageSum, currentSum, tempSum, faultsSum, actual1Sum, actual2Sum, actual3Sum = gizmo.sumInterval
        voltageCnt, currentCnt, tempCnt, faultsCnt, actual1Cnt, actual2Cnt, actual3Cnt = gizmo.cntInterval
        fmt = [
                    "{time:>8s}  ",
                    "{actual1:>9.1f} ",
                    "{actual2:>9.1f} ",
                    "{actual3:>9.1f} ",
                    "{voltage:>10.2f} ",
                    "{current:>10.3f} ",
                    "{temperature:>10.1f}           ",
                    "{faults:>02X}",
                ]
        if gzmCfg.config(gzmCfg.LogPollCounts) :
            fmt.append(" {count:11d}")
        fmt.append("\n")
        return "".join(fmt).format(time = formatSecondsAsHhMmSs(time),
                                    actual1 = safeDivide(actual1Sum, actual1Cnt),
                                    actual2 = safeDivide(actual2Sum, actual2Cnt),
                                    actual3 = safeDivide(actual2Sum, actual3Cnt),
                                    voltage = safeDivide(voltageSum, voltageCnt),
                                    current = safeDivide(currentSum, currentCnt),
                                    temperature = safeDivide(tempSum, tempCnt),
                                    faults = faultsSum,
                                    count = gizmo.pollCount)

    #----------------------------------------------------------

    def makeLogFileTestHeader(self, gizmo, testTime, testNumber, test) :
        """Return the test header text.

        Arguments:
            gizmo       Gizmo.
            testTime    Time when the test started.
            testNumber  Number [1, ...] of the test.
            test        Test data.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        fmt = [
                    "\n",
                    ("=" * 80), "\n",
                    "\n",
                    "Test #{number:d} started at {time:s}\n",
                    "Setpoints                      Duration    Sample    Pause    Stop\n",
                    "{setpoint1:6.1f}%   {setpoint2:6.1f}%   {setpoint3:6.1f}%   " \
                            "{duration:>8s}   {interval:>7s}    {pause:>7s}   {stop:>4s}\n",
                    "\n",
                    "    Time       Actuals                       Volts    Current       Temp       Faults",
                ]
        if gzmCfg.config(gzmCfg.LogPollCounts) :
            fmt.append("       Count")
        fmt.append("\n")
        return "".join(fmt).format(number = testNumber,
                                    time = testTime,
                                    setpoint1 = test[gzmCfg.Setpoint1],
                                    setpoint2 = test[gzmCfg.Setpoint2],
                                    setpoint3 = test[gzmCfg.Setpoint3],
                                    duration = formatSecondsAsHhMmSs(test[gzmCfg.Duration]),
                                    interval = formatSeconds(test[gzmCfg.Interval], 1),
                                    pause = formatSeconds(test[gzmCfg.PauseAfterTest], 1),
                                    stop = "Yes" if test[gzmCfg.StopAfterTest] else "No")

    #----------------------------------------------------------

    def makeLogFileTestTrailer(self, gizmo, testTime, testNumber, test) :
        """Return the test trailer text.

        Arguments:
            gizmo       Gizmo.
            testTime    Time when the test started.
            testNumber  Number [1, ...] of the test.
            test        Test data.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        statusFmt = "".join([
                                "{name:<9s} ",
                                "{actual1:>9.1f} ",
                                "{actual2:>9.1f} ",
                                "{actual3:>9.1f} ",
                                "{voltage:10.2f} ",
                                "{current:>10.3f} ",
                                "{temperature:>10.1f}           ",
                                "{faults:>02X} ",
                                "{count:11d}\n",
                            ])
        countFmt = "".join([
                                "{name:<9s} ",
                                "{actual1:>9} ",
                                "{actual2:>9} ",
                                "{actual3:>9} ",
                                "{voltage:>10d} ",
                                "{current:>10d} ",
                                "{temperature:>10d}   ",
                                "{faults:>10d} ",
                                "{count:11d}\n",
                            ])
        text = []
        if test[gzmCfg.LogAverages] and gizmo.totalPollCount:
            voltageSum, currentSum, tempSum, faultsSum, actual1Sum, actual2Sum, actual3Sum = gizmo.sumTest
            voltageCnt, currentCnt, tempCnt, faultsCnt, actual1Cnt, actual2Cnt, actual3Cnt = gizmo.cntTest
            text.append(statusFmt.format(name = "Average",
                                            actual1 = safeDivide(actual1Sum, actual1Cnt),
                                            actual2 = safeDivide(actual2Sum, actual2Cnt),
                                            actual3 = safeDivide(actual3Sum, actual3Cnt),
                                            voltage = safeDivide(voltageSum, voltageCnt),
                                            current = safeDivide(currentSum, currentCnt),
                                            temperature = safeDivide(tempSum, tempCnt),
                                            faults = faultsSum,
                                            count = gizmo.totalPollCount))
        if test[gzmCfg.LogBadCounts] :
            voltage, current, temp, faults, actual1, actual2, actual3 = gizmo.okayTest
            text.append(countFmt.format(name = "Bad Count",
                                        actual1 = gizmo.totalPollCount - actual1,
                                        actual2 = gizmo.totalPollCount - actual2,
                                        actual3 = gizmo.totalPollCount - actual3,
                                        voltage = gizmo.totalPollCount - voltage,
                                        current = gizmo.totalPollCount - current,
                                        temperature = gizmo.totalPollCount - temp,
                                        faults = gizmo.totalPollCount - faults,
                                        count = gizmo.totalPollCount))
        if test[gzmCfg.LogMinimums] :
            voltage, current, temp, faults, actual1, actual2, actual3 = gizmo.minimum
            text.append(statusFmt.format(name = "Minimum",
                                            actual1 = actual1,
                                            actual2 = actual2,
                                            actual3 = actual3,
                                            voltage = voltage,
                                            current = current,
                                            temperature = temp,
                                            faults = faults,
                                            count = gizmo.totalPollCount))
        if test[gzmCfg.LogMaximums] :
            voltage, current, temp, faults, actual1, actual2, actual3 = gizmo.maximum
            text.append(statusFmt.format(name = "Maximum",
                                            actual1 = actual1,
                                            actual2 = actual2,
                                            actual3 = actual3,
                                            voltage = voltage,
                                            current = current,
                                            temperature = temp,
                                            faults = faults,
                                            count = gizmo.totalPollCount))
        return "".join(text)

    #----------------------------------------------------------

    def makeLogFileTrailer(self, gizmo, allTests, endTime) :
        """Return the file trailer text.

        Arguments:
            gizmo       Gizmo the header text is for.
            allTests    Tests info.
            endTime     When testing stopped.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        fmt = [
                    ("=" * 80),
                    "",
                    "Ended:            {endTime:s}",
                    "",
                ]
        if gzmCfg.config(gzmCfg.LogRepeatInfoAtEnd) :
            fmt.extend([
                            "{standardInfo}",
                            "",
                        ])
        text = [
                "\n".join(fmt).format(endTime = endTime,
                                        standardInfo = self.makeLogStandardInfo(gizmo)),
                ]
        if gizmo.isFault :
            fmt = "\n".join([
                                "",
                                "FAILED at {failureTime:s}",
                                "Cause(s): {failureCause:s}",
                            ])
            text.append(fmt.format(failureTime = gizmo.failureTime.isoformat(sep = ' ')[0 : 19],
                                    failureCause = gizmo.failureCause))
        else:
            text.append("\nPassed all tests\n")
        #
        return "".join(text)

    #----------------------------------------------------------

    def makeLogStandardInfo(self, gizmo) :
        """Return the file header text.

        Arguments:
            gizmo       Gizmo to the header text is for.
            allTests    Tests info.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        fmt = "\n".join([
                            "Serial number:      {serialNumber:s}",
                            "Node ID:            {nodeId:d}",
                            "Group ID:           {groupId:d}",
                            "LED ID:             {gizmoId:d}",
                            "Firmware version:   {firmwareVersion:s}",
                            "",
                            "User:               {userName:s}",
                            "",
                            "Test regime:",
                            "   Version:         {testRegime:s}",
                            "   Source:          {testSource:s}",
                            "   Last modified:   {testTimeStamp:s}",
                            "   Hash:            {testHash:s}",
                        ])
        return fmt.format(userName = gizmo.userName,
                            testRegime = gzmCfg.config(gzmCfg.TestRegime),
                            testSource = gzmCfg.config(gzmCfg.TestSource),
                            testHash = gzmCfg.config(gzmCfg.TestHash),
                            testTimeStamp = gzmCfg.config(gzmCfg.TestTimeStamp),
                            nodeId = gizmo.nodeId,
                            groupId = gizmo.groupId,
                            gizmoId = gizmo.gizmoId,
                            serialNumber = gizmo.serialNumber,
                            firmwareVersion = gizmo.firmwareVersion)
