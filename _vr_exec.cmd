if ""=="%DbgEcho%" set DbgEcho=REM
set BaseDir=%~dp0
set AppFile=%1

%DbgEcho%  AppFile=%AppFile%
%DbgEcho%  BaseDir=%BaseDir%
%DbgEcho%  AppOptions=%AppOptions%

%DbgEcho% Looking for python:
set FoundFlag=0
for %%P in (python3.exe python.exe) do call :ChkExe %%P
if 0==%FoundFlag% (
	echo Unable to find python.
	goto :EOF
)
set PythonPath=%FoundPath%
%DbgEcho% PythonPath=%PythonPath%

%DbgEcho% Looking for application:
set FoundFlag=0
call :ChkFile %AppFile%
if 0==%FoundFlag% (
	echo Unable to find application file: %AppFile%
	goto :EOF
)
set AppPath=%FoundPath%
%DbgEcho% AppPath=%AppPath%

%DbgEcho% Running: %PythonPath% %AppPath% %AppOptions%
%PythonPath% %AppPath% %AppOptions%
pause
goto :EOF

:: Look for an EXE.
:ChkExe
if 1==%FoundFlag% goto :EOF
set FindFile=%1
%DbgEcho% ChkExe: %FindFile%
call :ChkStd
call :ChkDirs %ProgramFiles%\Python*
call :ChkDirs %ProgramFiles(x86)%\Python*
goto :EOF

:: Look for FindFile in the given directory pattern.
:ChkDirs
if 1==%FoundFlag% goto :EOF
set DirPattern=%*
%DbgEcho% ChkDirs: %DirPattern%
for /D %%D in ("%DirPattern%") do call :ChkDir %%D\
goto :EOF

:: Look for the given file in standard locations.
:ChkFile
set FindFile=%1

:: Look for FindFile in standard locations.
:ChkStd
if 1==%FoundFlag% goto :EOF
%DbgEcho% ChkStd: %FindFile%
call :ChkDir .\
call :ChkDir %BaseDir%
call :ChkPath %FindFile%
goto :EOF

:: Look for FindFile in the given directory.
:ChkDir
if 1==%FoundFlag% goto :EOF
set SubDir=%*
%DbgEcho% ChkDir: %SubDir%%FindFile%
if exist "%SubDir%%FindFile%" (
	set FoundFlag=1
	set FoundPath="%SubDir%%FindFile%"
	%DbgEcho% Found: !FoundPath!
)
goto :EOF

:: Look for FindFile in the PATH.
:ChkPath
if 1==%FoundFlag% goto :EOF
%DbgEcho% ChkPath: %1
if not ""=="%~$PATH:1" (
	set FoundFlag=1
	set FoundPath="%~$PATH:1"
	%DbgEcho% Found: !FoundPath!
)
goto :EOF
