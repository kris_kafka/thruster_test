import argparse
import atexit
import os
import sys
import time
import Vr
import VrUtil

if os.name == 'nt':
    from serial.tools.list_ports_windows import comports
elif os.name == 'posix':
    from serial.tools.list_ports_posix import comports
else:
    raise ImportError("Sorry: no implementation for your platform ('{osName:s}') available".format(osName = os.name))

#=======================================================================================================================

ModuleName  = "vr_enum_all"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 4
ModuleVersionMicro  = 0

AppName             = "Gizmo Enumeration Tool"
#
#   ProgramStartMessage = "\n{time:s} - Starting gizmo enumeration tool"
#   ProgramExitMessage  = "\n{time:s} - Exiting gizmo enumeration tool\n"
ProgramStartMessage = ""
ProgramExitMessage  = ""

#=======================================================================================================================

class _Config :
    """Application configuration values.
    """
    # Note:
    #   Times values are in given in seconds.
    #
    ResetPause      = 5.000     # How long to pause after resetting the gizmos.
    ResetGizmos     = False     # Reset gizmos after showing their information?
    #
    ShowSerialPorts = False     # Show available serial ports?
    PortIndex       = 0         # Index of default serial port (in sorted list).
    SortSerialPorts = False     # Sort the serial ports before selecting the default?

#=======================================================================================================================

class _CsrArgumentParser(argparse.ArgumentParser) :
    """Command line/parameter file argument parser.
    """

    def convert_arg_line_to_args(self, argLine) :
        """Split an indirect file line into separate fields.
        """
        return argLine.split()

#=======================================================================================================================

def appMmain() :
    portName = commandLineParse()
    #
    atexit.register(notifyExit)
    print(ProgramStartMessage.format(time = VrUtil.utcNowStr()))
    print("Serial port: {portName:s}\n".format(portName = portName))
    #
    port = Vr.Port(portName)
    status, gizmos = findGizmos(port)
    if not status :
        VrUtil.errorBeep("\nError finding gizmos\n")
        exit(1)
    #
    showGizmosInfo(gizmos)
    #
    if _Config.ResetGizmos :
        for gizmo in gizmos :
            gizmo.reboot(port)
        time.sleep(_Config.ResetPause)
    #
    exit(0)

#----------------------------------------------------------

def commandLineParse() :
    """Parse and extract command line options.
    """
    parser = _CsrArgumentParser(fromfile_prefix_chars = '@',
                                description = AppName)
    parser.add_argument('-p', '--port',
                        dest = 'portName',
                        help = 'Set serial port name',
                        metavar = 'portName',
                        default = getDefaultPortName())
    parser.add_argument('--version',
                        action = 'store_true',
                        dest = 'showVersion',
                        help = 'Show version information')
    options = parser.parse_args(sys.argv[1:])
    if options.showVersion :
        showVersion()
        exit(1)
    return options.portName

#----------------------------------------------------------

def findGizmos(port) :
    """Find all of the gizmos.

    Arguments:
        port    Serial port.

    Return (status, gizmos)
            status      True if okay.
            gizmos  List of the found gizmos.
    """
    # Find all devices.
    status, gizmos = Vr.enumerateGizmos(port)
    if not status :
        #VrUtil.errorBeep("Vr.enumerateGizmos failed")
        return False, gizmos
    # Get the firmware versions.
    for gizmo in gizmos :
        status = status and gizmo.getFirmwareVersion(port)
    return status, gizmos

#----------------------------------------------------------

def getDefaultPortName() :
    """Return the name of the serialPort.
    """
    # Get the first available serial port.
    serialPorts = comports()
    if not len(serialPorts) :
        VrUtil.errorBeep("\nNo serial ports found!\n")
        exit(1)
    if _Config.SortSerialPorts :
        serialPorts = sorted(serialPorts, key = lambda t : t[0])
    if _Config.ShowSerialPorts :
        for name, desc, hwId in serialPorts :
            print("Port='{port:s}', HwId={hwId:s}, Desc={desc:s}" \
                    .format(port = name, hwId = hwId, desc = desc))
    portName, portDesc, portHwId = serialPorts[_Config.PortIndex]
    return portName

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#----------------------------------------------------------

def notifyExit() :
    """Notify when the application exits.
    """
    VrUtil.messageBeep(ProgramExitMessage.format(time = VrUtil.utcNowStr()))

#----------------------------------------------------------

def showGizmosInfo(gizmos) :
    """Show the gizmos information.

    Arguments:
        gizmos  Gizmos.
    """
    if 0 == len(gizmos) :
        VrUtil.errorBeep("No gizmos found, aborting!", end = '', file = sys.stderr)
        exit(1)
    # Display the gizmos.
    print("Gizmos found:\n" \
            "Node  Group  Serial       Version")
    for gizmo in gizmos :
        print("{nodeId:>3d}    {groupId:>3d}    {serialNumber:<12s} {firmwareVersion:<s}\n" \
                .format(nodeId = gizmo.nodeId,
                        groupId = gizmo.groupId,
                        serialNumber = gizmo.serialNumber,
                        firmwareVersion = gizmo.firmwareVersion),
                end = '')
    #

#----------------------------------------------------------

def showVersion() :
    """Show the application version information.
    """
    print("{appName:s} {majorVersion:d}.{minorVersion:d}.{microVersion:d}" \
            .format(appName = AppName,
                        majorVersion = ModuleVersionMajor,
                        minorVersion = ModuleVersionMinor,
                        microVersion = ModuleVersionMicro))
    #
    modules = []
    modules.append(getModuleInformation())
    modules.append(Vr.getModuleInformation())
    modules.append(VrUtil.getModuleInformation())

    #
    fmt = "{Name:<" + str(max([len(module["Name"]) for module in modules ])) + "s}" + \
        "   {VersionMajor:d}.{VersionMinor:d}.{VersionMicro:d}" + \
        "  {FileHash:s}  {LastModified:s}  {FilePath:s}"
    print("Module versions:")
    for module in sorted(modules, key = lambda module : module["Name"]) :
        print(fmt.format(**module))

#=======================================================================================================================

if __name__ == "__main__":
    appMmain()
