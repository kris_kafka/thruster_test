import argparse
import atexit
import datetime
import fasteners
import os
import sys
import time
import Vr
import VrUtil

if os.name == 'nt':
    from serial.tools.list_ports_windows import comports
elif os.name == 'posix':
    from serial.tools.list_ports_posix import comports
else:
    raise ImportError("Sorry: no implementation for your platform ('{osName:s}') available".format(osName = os.name))

#=======================================================================================================================

ModuleName          = "VrApp"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 0

#=======================================================================================================================

class _Config :
    """Application configuration values.
    """
    # Note:
    #   Times values are in given in terms of seconds.
    #
    LockDelayDelta      = 0.10  # Delay between attempts to attempt to acquire the summary log lock.
    LockDelayMaximum    = 0.50  # Maximum delay between attempts to attempt to acquire the summary log lock.
    LockTimeout         = 15.0  # Maximum time in seconds to attempt to acquire the summary log lock.
    PortIndex           = 0     # Index of default serial port (in sorted list).
    SortSerialPorts     = False # Sort the serial ports before selecting the default?

#=======================================================================================================================

class _Debug :
    """Debugging configuration parameters.
    """
    LockShowAcquire     = False # Show when attempting to acquire the lock?
    ShowSerialPorts     = False # Show available serial ports?

#=======================================================================================================================

def getDefaultPortName() :
    """Return the name of the serialPort.
    """
    # Get the first available serial port.
    serialPorts = comports()
    if not len(serialPorts) :
        VrUtil.errorBeep("\nNo serial ports found!\n")
        exit(1)
    if _Config.SortSerialPorts :
        serialPorts = sorted(serialPorts, key = lambda t : t[0])
    if _Debug.ShowSerialPorts :
        for name, desc, hwId in serialPorts :
            print("Port='{port:s}', HwId={hwId:s}, Desc={desc:s}" \
                    .format(port = name, hwId = hwId, desc = desc))
    portName, portDesc, portHwId = serialPorts[_Config.PortIndex]
    return portName

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#----------------------------------------------------------

def notifyExit(programExitMessage) :
    """Notify when the application exits.
    """
    VrUtil.messageBeep(programExitMessage .format(time = VrUtil.utcNowStr()))

#=======================================================================================================================

class _CsrArgumentParser(argparse.ArgumentParser) :
    """Command line/parameter file argument parser.
    """

    def convert_arg_line_to_args(self, argLine) :
        """Split an indirect file line into separate fields.
        """
        return argLine.split()

#=======================================================================================================================

class VrApp :

    #----------------------------------------------------------

    def __init__(self, appInf) :
        """Initialize a VrApp object.
        """
        self.appInf = appInf

    #----------------------------------------------------------

    def commandLineParse(self) :
        """Parse and extract command line options.
        """
        appInf = self.appInf
        #
        parser = _CsrArgumentParser(fromfile_prefix_chars = '@',
                                    description = appInf.AppName)
        parser.add_argument('-p', '--port',
                            dest = 'portName',
                            help = 'Set serial port name',
                            metavar = 'portName',
                            default = getDefaultPortName())
        parser.add_argument('-u', '--user',
                            dest = 'userName',
                            help = 'Set user name',
                            metavar = 'userName',
                            default = os.getenv("VrUserName"))
        parser.add_argument('-f', '--file',
                            dest = 'controlFile',
                            help = 'Load configuration from file',
                            metavar = 'controlFile',
                            default = None)
        parser.add_argument('--version',
                            action = 'store_true',
                            dest = 'showVersion',
                            help = 'Show version information')
        #
        options = parser.parse_args(sys.argv[1:])
        #
        if options.showVersion :
            self.showVersion()
            exit(1)
        #
        return options

    #----------------------------------------------------------

    def doAllTests(self, userName) :
        """Perform all of the tests.

        Arguments:
            userName    Name of the user.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmLog = appInf.GizmoLogModule
        #
        startTime = VrUtil.utcNowStr()
        for gizmo in self.gizmos :
            gizmo.setLoggingInfo(startTime, userName)
        allTests = gzmCfg.tests()
        gzmLog.Log(appInf).createAllFiles(self.gizmos, allTests)
        gzmLog.Log(appInf).dumpCsrForAll(self.gizmos,
                                            gzmCfg.config(gzmCfg.LogCsrHexAtStart),
                                            gzmCfg.config(gzmCfg.LogCsrDataAtStart))
        if gzmCfg.config(gzmCfg.LogErrorCounts) :
            gzmLog.Log(appInf).recordErrorCountsForAll(self.gizmos)
        #
        for testNumber, test in enumerate(allTests, start = 1) :
            testTime = VrUtil.utcNowStr()
            if self.doOneTest(test, testNumber, testTime) :
                break
        #
        for gizmo in self.gizmos :
            gizmo.stopGizmo()
        #
        endTime = VrUtil.utcNowStr()
        gzmLog.Log(appInf).dumpCsrForAll(self.gizmos,
                                            gzmCfg.config(gzmCfg.LogCsrHexAtEnd),
                                            gzmCfg.config(gzmCfg.LogCsrDataAtEnd))
        gzmLog.Log(appInf).finishAllLogFiles(self.gizmos, allTests, endTime)

    #----------------------------------------------------------

    def doOneTest(self, test, testNumber, testTime) :
        """Perform a single test.

        Arguments:
            test        Test data.
            testNumber  Number [1, ...] of the test.
            testTime    Time when the test started.

        Returns True if all of the gizmos are faulted.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmLog = appInf.GizmoLogModule
        #
        self.showTestData(test, testNumber, testTime)
        interval = test[gzmCfg.Interval]
        for gizmo in self.gizmos :
            gizmo.resetTest()
            gizmo.initializeForTest(test)
        # Log the start of the test.
        gzmLog.Log(appInf).testStartForAll(self.gizmos, testTime, testNumber, test)
        #
        now = VrUtil.utcNow()
        startOfTestTime = now
        startUpTime = now + datetime.timedelta(milliseconds = 1000 * test[gzmCfg.TestStartupTime])
        endOfTestTime = now + datetime.timedelta(seconds = test[gzmCfg.Duration])
        nextLogTime = interval
        lastPollTime = None
        isAllFaulted = True
        forceFirst = test[gzmCfg.LogFirstPoll]
        while now <= endOfTestTime :
            if lastPollTime :
                sleepTime = test[gzmCfg.PollInterval] - (now - lastPollTime).total_seconds()
                if sleepTime > 0.0 :
                    time.sleep(sleepTime)
            lastPollTime = now
            # Poll and update all of the gizmos.
            isStartup = now < startUpTime
            isAllFaulted = True
            for gizmo in self.gizmos :
                gizmo.poll(isStartup, testNumber, test)
                if not gizmo.isFault :
                    isAllFaulted = False
            if not isStartup :
                if isAllFaulted :
                    break
                # Log the latest status values if needed.
                delta = (now - startOfTestTime).total_seconds()
                if forceFirst or delta >= nextLogTime :
                    gzmLog.Log(appInf).recordStatusForAll(self.gizmos, testTime, nextLogTime)
                    forceFirst = False
                    nextLogTime = interval * (1 + delta // interval)
            now = VrUtil.utcNow()
        gzmLog.Log(appInf).recordStatusForAll(self.gizmos,
                                                testTime,
                                                nextLogTime)
        # Stop all of the gizmos.
        for gizmo in self.gizmos :
            if test[gzmCfg.StopAfterTest] :
                gizmo.stopGizmo()
            gizmo.fixMinMax()
        # Log averages and counts.
        gzmLog.Log(appInf).testEndForAll(self.gizmos, testTime, testNumber, test)
        if gzmCfg.config(gzmCfg.LogErrorCounts) :
            gzmLog.Log(appInf).recordErrorCountsForAll(self.gizmos)
        # Pause after test.
        if test[gzmCfg.PauseAfterTest] :
            time.sleep(test[gzmCfg.PauseAfterTest])
        return isAllFaulted

    #----------------------------------------------------------

    def main(self) :
        """Application main function.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmTst = appInf.GizmoTstModule
        #
        options = gzmCfg.Options(appInf).process(gzmCfg.Parser, self.commandLineParse())
        self.summaryLogCheck()
        #
        if not options.userName :
            while not options.userName :
                options.userName = input("Enter user name: ").strip()
            print("")
        #
        atexit.register(notifyExit, appInf.ProgramExitMessage)
        print(appInf.ProgramStartMessage.format(time = VrUtil.utcNowStr()))
        self.showOptions(options)
        #
        port = Vr.Port(options.portName)
        status, self.gizmos = gzmTst.findGizmos(appInf, port)
        if not status :
            VrUtil.errorBeep("\nError finding {}\n".format(gzmTst.GizmoNamePluralLwr))
            exit(1)
        #
        self.showGizmosInfo()
        #
        if gzmCfg.config(gzmCfg.ResetGizmos) :
            for gizmo in self.gizmos :
                gizmo.reboot(port)
            time.sleep(gzmCfg.config(gzmCfg.ResetPause))
        #
        while True :
            answer = input("Okay? ").upper()
            if 'Y' == answer or 'YES' == answer :
                print("")
                break
            if 'N' == answer or 'NO' == answer :
                VrUtil.messageBeep("Goodbye\n")
                exit(1)
        #
        print("")
        self.doAllTests(options.userName)
        self.showGizmosResult()
        self.summaryLogUpdate()
        #
        exit(0)

    #----------------------------------------------------------

    def showOptions(self, options) :
        """Show the test information.

        Arguments:
            options     Program options.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        #
        fmt = [ "User name:        {userName:s}",
                "Serial port:      {portName:s}",
                "Test regime:",
                "  Version:        {testRegime:s}",
                "  Source:         {testSource:s}",
                "  Hash:           {testHash:s}",
                "  Last modified:  {testTimeStamp:s}",
                ""
                ]
        fmt = "\n".join(fmt)
        print(fmt.format(userName = options.userName,
                            portName = options.portName,
                            testRegime = gzmCfg.config(gzmCfg.TestRegime),
                            testSource = gzmCfg.config(gzmCfg.TestSource),
                            testHash = gzmCfg.config(gzmCfg.TestHash),
                            testTimeStamp = gzmCfg.config(gzmCfg.TestTimeStamp)))

    #----------------------------------------------------------

    def showTestData(self, test, testNumber, testTime) :
        """Show the test information.

        Arguments:
            test        Test data.
            testNumber  Number [1, ...] of the test.
            testTime    Time when the test started.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        gzmLog = appInf.GizmoLogModule
        #
        prefixSize = 2 + max([len(key) for key in test])
        labelFmt = "{label:<" + str(prefixSize) + "s}"
        text, processed = gzmLog.formatTestData(appInf, test, prefixSize)
        print("\n{time:s} - Test #{number:d}:" \
                .format(time = testTime,
                        number = testNumber))
        print("{text:s}" \
                .format(text = text))
        print((labelFmt + "{duration:s}") \
                .format(label = "Duration:",
                        duration = gzmLog.formatSeconds(test[gzmCfg.Duration], forceHours = True)))
        print((labelFmt + "{interval:s}") \
                .format(label = "Interval:",
                        interval = gzmLog.formatSeconds(test[gzmCfg.Interval], 1)))
        print((labelFmt + "{pause:s}") \
                .format(label = "PauseAfterTest:",
                        pause = gzmLog.formatSeconds(test[gzmCfg.PauseAfterTest], 1)))
        print((labelFmt + "{stop:s}") \
                .format(label = "StopAfterTest:", stop = "Yes" if test[gzmCfg.StopAfterTest] else "No"))
        # Show non-default test control values.
        if gzmCfg.config(gzmCfg.ShowNonDefault) :
            processed.extend([gzmCfg.Duration,
                                gzmCfg.Interval,
                                gzmCfg.PauseAfterTest,
                                gzmCfg.StopAfterTest])
            testDefaults = gzmCfg.testDefaults()
            fields = [field for field in sorted(testDefaults) \
                        if field not in processed and testDefaults[field] != test[field]]
            if fields :
                fmt = labelFmt + "{value}"
                for field in fields :
                    print(fmt.format(label = field + ":", value = test[field]))

    #----------------------------------------------------------

    def showGizmosInfo(self) :
        """Show the gizmos information.
        """
        appInf = self.appInf
        gzmTst = appInf.GizmoTstModule
        #
        if 0 == len(self.gizmos) :
            VrUtil.errorBeep("No {} found, aborting!".format(gzmTst.GizmoNamePluralLwr))
            exit(1)
        # Display the gizmos.
        badDevices = 0
        print(gzmTst.GizmoNamePluralCap + " found:")
        print("Node  Group    ID   Serial       Version")
        for gizmo in self.gizmos :
            deviceErrors = gizmo.checkIfOkay()
            print("{nodeId:>3d}    {groupId:>3d}  {gizmoId:>5d}   {serialNumber:<12s} {firmwareVersion:<s}" \
                    .format(nodeId = gizmo.nodeId,
                            groupId = gizmo.groupId,
                            gizmoId = gizmo.gizmoId,
                            serialNumber = gizmo.serialNumber,
                            firmwareVersion = gizmo.firmwareVersion))
            if deviceErrors :
                badDevices += 1
                print("    {errors:s}".format(errors = ", ".join(deviceErrors)))
        #
        print("")
        if badDevices :
            VrUtil.errorBeep("{count:d} bad devices!".format(count = badDevices, file = sys.stderr))
            exit(1)

    #----------------------------------------------------------

    def showGizmosResult(self) :
        """Show the gizmos status.
        """
        appInf = self.appInf
        gzmTst = appInf.GizmoTstModule
        #
        print("")
        print(gzmTst.GizmoNamePluralCap + " status:")
        print("Status  Node  Group    ID    Serial       Version       Failure Cause(s)")
        for gizmo in self.gizmos :
            fmt = "FAILED  {nodeId:>3d}    {groupId:>3d}  {gizmoId:>5d}    " \
                        "{serialNumber:<12s} {firmwareVersion:<11s}   {failureCause:s}" \
                    if gizmo.isFault \
                    else "Passed  {nodeId:>3d}    {groupId:>3d}  {gizmoId:>5d}    " \
                            "{serialNumber:<12s} {firmwareVersion:s}"
            print(fmt.format(nodeId = gizmo.nodeId,
                                groupId = gizmo.groupId,
                                gizmoId = gizmo.gizmoId,
                                serialNumber = gizmo.serialNumber,
                                firmwareVersion = gizmo.firmwareVersion,
                                failureCause = gizmo.failureCause))

    #----------------------------------------------------------

    def showVersion(self) :
        """Show the application version information.
        """
        appInf = self.appInf
        fmt = "{appName:s} {majorVersion:d}.{minorVersion:d}.{microVersion:d}"
        print(fmt.format(appName = appInf.AppName,
                            majorVersion = appInf.ModuleVersionMajor,
                            minorVersion = appInf.ModuleVersionMinor,
                            microVersion = appInf.ModuleVersionMicro))
        print(appInf.LogModule.formatModuleVersions(appInf))

    #----------------------------------------------------------

    def summaryLogCheck(self) :
        """Check access to the summary log file.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        # Do nothing if not updating the summary log file.
        if not gzmCfg.config(gzmCfg.SummaryLogUpdate) :
            return
        #
        self.summaryLogOpen()
        self.summaryLogClose()

    #----------------------------------------------------------

    def summaryLogClose(self) :
        """Close the summary log file.

        Assumptions:    summaryLogOpen() has been called.

        Note:           Aborts the program on errors.
        """
        self.sumLckLock.release()
        try:
            self.sumLogFile.close()
        except (IOError, OSError) as error :
            VrUtil.errorBeep("\nUnable to close summary log file: {}\n".format(self.sumLogPath))
            exit(1)
        del self.sumLckLock
        del self.sumLogFile
        del self.sumLogPath

    #----------------------------------------------------------

    def summaryLogOpen(self) :
        """Open the summary log file.

        Note:   Aborts the program on errors.
        """
        # Error if the summary log file does not exist.
        cfg = self.appInf.CfgModule
        self.sumLogPath = cfg.config(cfg.SummaryLogFile)
        if not os.path.exists(self.sumLogPath) :
            VrUtil.errorBeep("\nUnable to find summary log file: {}\n".format(self.sumLogPath))
            exit(1)
        # Attempt to acquire the summary log file lock.
        if _Debug.LockShowAcquire :
            print("Attempting to acquire the lock:")
        self.sumLckLock = fasteners.InterProcessLock(self.sumLogPath)
        if not self.sumLckLock.acquire(delay = _Config.LockDelayDelta,
                                        max_delay = _Config.LockDelayMaximum,
                                        timeout = _Config.LockTimeout) :
            VrUtil.errorBeep("\nUnable to lock summary log file: {}\n".format(self.sumLogPath))
            exit(1)
        # Attempt to open the summary log file.
        try:
            self.sumLogFile = open(self.sumLogPath, 'a')
        except (IOError, OSError) as error :
            VrUtil.errorBeep("\nUnable to open summary log file: {}\n".format(self.sumLogPath))
            exit(1)

    #----------------------------------------------------------

    def summaryLogUpdate(self) :
        """Show the gizmos status.
        """
        appInf = self.appInf
        gzmCfg = appInf.GizmoCfgModule
        # Do nothing if not updating the summary log file.
        if not gzmCfg.config(gzmCfg.SummaryLogUpdate) :
            return
        #
        testRegime = gzmCfg.config(gzmCfg.TestRegime)
        timeHash = gzmCfg.config(gzmCfg.TestHash)
        timeStamp = gzmCfg.config(gzmCfg.TestTimeStamp)
        #
        fmt = VrUtil.utcNowStr(length = 16) + \
            "  {serial:<12s} {version:<10s} {regime:<16s}  {timeStamp:<16s}  {hash:<16s}  {status:s}{cause:s}\n"
        text = []
        # Generate all of the status text.
        for gizmo in self.gizmos :
            if gizmo.isFault :
                status  = "FAIL"
                cause   = "  " + gizmo.failureCause
            else:
                status  = "Pass"
                cause   = ""
            text.append(fmt.format(serial = gizmo.serialNumber,
                                    version = gizmo.firmwareVersion,
                                    regime = testRegime,
                                    timeStamp = timeStamp,
                                    hash = timeHash,
                                    status = status,
                                    cause = cause))
        text = "".join(text)
        # Update the summary log file.
        self.summaryLogOpen()
        print(text, end = '', file = self.sumLogFile)
        self.summaryLogClose()
