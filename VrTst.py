import math
import shutil
import Vr
import VrUtil

#=======================================================================================================================

ModuleName          = "VrTst"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 5

#=======================================================================================================================

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#=======================================================================================================================

def _fixTimeForFileName(time) :
    """Return a time adjusted for a file name.
    """
    return time.replace(':', '-').replace(' ', '_')

#=======================================================================================================================

class VrGizmo :
    """Gizmo test information and functions.
    """
    GizmoFailure    =   "FAILURE of {serialNumber:s}, nodeId={nodeId:d}, groupId={groupId:d}, " \
                            "ID={gizmoId:d}, cause={cause:s}\n"

    #----------------------------------------------------------
    @staticmethod
    def _applyAll(subrs, *sequences) :
        """Apply each subr to the corresponding sequences.

        Arguments:
            subrs       List of functions.
            sequences   List of lists.
        """
        return list([subr(items) \
                    for subr, *items \
                    in zip(subrs, *sequences)])

    #----------------------------------------------------------
    @staticmethod
    def _applyChk(subrs, okay, *sequences) :
        """Apply each subr to the corresponding sequences if okay and a valid number.

        Arguments:
            subrs       List of functions.
            okay        List of control values.
            sequences   List of lists.
        """
        return list([subr(items) if ok and items[0] and not math.isnan(items[0]) else items[1] \
                        for subr, ok, *items \
                        in zip(subrs, okay, *sequences)])

    #----------------------------------------------------------
    @staticmethod
    def _applyIf(subrs, okay, *sequences) :
        """Apply each subr to the corresponding sequences if okay.

        Arguments:
            subrs       List of functions.
            okay        List of control values.
            sequences   List of lists.
        """
        return list([subr(items) if ok else items[0] \
                        for subr, ok, *items \
                        in zip(subrs, okay, *sequences)])

    #----------------------------------------------------------
    @staticmethod
    def _applyOne(subr, *sequences) :
        """Apply subr to all corresponding elements of the sequences.

        Arguments:
            subr        Function.
            sequences   List of lists.
        """
        return list([subr(items) \
                    for items \
                    in zip(*sequences)])

    #----------------------------------------------------------

    def __init__(self, appInf, port = None) :
        """Initialize a VRLedTest.Gizmo object.

        Arguments:
            port    Optional serial port associated with this device.
        """
        #
        # Misc
        self.appInf        = appInf
        self.port           = port
        self.startTime      = None
        self.userName       = None
        #
        # Log file specific
        self.logFile        = None
        self.logFileName    = None
        self.csvFile        = None
        self.csvFileName    = None
        self.fileError      = False
        #
        # Status
        self.failureCause   = ""
        self.failureTime    = 0
        self.isFault        = False
        #
        self.resetTest()

    #----------------------------------------------------------

    def checkIfOkay(self) :
        """Verify the CSR of a gizmo.

        Returns list of reasons why the device is not okay.
        """
        answer = super().checkIfOkay()
        csr0 = self.csrRead(self.port, 0x00, 0x78)
        csr1 = self.csrRead(self.port, 0x78, 0x78)
        if csr0 is not None or csr1 is not None :
            csr = csr0.payloadData() + csr1.payloadData()
            # ToDo: Add code to verify CSR fields.
        return answer

    #----------------------------------------------------------
    @staticmethod
    def findGizmos(port, deviceType, getGizmoId, fromGizmo) :
        """Find all of the LEDs.

        Arguments:
            port        Serial port.
            deviceType  Device type.
            getGizmoId  Function to get the gizmo ID.
            fromGizmo   Function to create a specific type of gizmo from a generic gizmo.

        Return (status, gizmos)
                status  True if okay.
                gizmos  List of the found gizmos.
        """
        # Find all devices.
        status, allGizmos = Vr.enumerateGizmos(port)
        if not status :
            return False, allGizmos
        # Filter out everything that is not a LED.
        gizmos = [fromGizmo(gizmo, port)
                        for gizmo in allGizmos
                        if gizmo.deviceType == deviceType]
        # Get the gizmo IDs.
        for gizmo in gizmos :
            status = status and gizmo.getFirmwareVersion(port)
            status = status and getGizmoId(gizmo, port)
        return status, gizmos

    #----------------------------------------------------------

    def fixMinMax(self) :
        """Fix up minimum and maximum values.
        """
        self.maximum = list([val or 0 for val in self.maximum])
        self.minimum = list([val or 0 for val in self.minimum])

    #----------------------------------------------------------

    def moveLogsToFinalDestination(self) :
        """Move log files to final destination.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        if gzmCfg.config(gzmCfg.LogGenerate) :
            self._moveFile(self.logFileName,
                            gzmCfg.config(gzmCfg.LogDestTemplate),
                            gzmCfg.config(gzmCfg.LogMoveToDest))
        if gzmCfg.config(gzmCfg.CsvGenerate) :
            self._moveFile(self.csvFileName,
                            gzmCfg.config(gzmCfg.CsvDestTemplate),
                            gzmCfg.config(gzmCfg.CsvMoveToDest))

    #----------------------------------------------------------

    def reportFileError(self, text, error) :
        """Fix up minimum and maximum values.

        Arguments:
            text    Error message.
            error   Exception data.
        """
        self.fileError = True
        VrUtil.errorBeep("\n{}\nError: {}\n" .format(text, error))

    #----------------------------------------------------------

    def resetInterval(self) :
        """Reset the gizmo interval status.
        """
        self.pollCount      = 0
        self.cntInterval    = self.ParseSize * [0]
        self.sumInterval    = self.ParseSize * [0]

    #----------------------------------------------------------

    def resetTest(self) :
        """Reset the gizmo test status.
        """
        now = VrUtil.utcNow()
        self.isFaultAtStart = self.isFault
        self.pollCount      = 0
        self.totalPollCount = 0
        self.lastReplyTime  = now
        #
        self.cntTest    = self.ParseSize * [0]
        self.lastOkTime = self.ParseSize * [now]
        self.maximum    = self.ParseSize * [None]
        self.minimum    = self.ParseSize * [None]
        self.okayTest   = self.ParseSize * [0]
        self.sumTest    = self.ParseSize * [0]
        #
        self.resetInterval()
        #
        self.updateErrorCounts()

    #----------------------------------------------------------

    def setLoggingInfo(self, startTime, userName) :
        """Set the logging information for this device.

        Arguments:
            startTime   When the application started.
            userName    Name of the user.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        #
        self.startTime      = startTime
        self.userName       = userName
        #
        fileTime            = _fixTimeForFileName(self.startTime)
        self.logFileName    = gzmCfg.config(gzmCfg.LogMakeTemplate) \
                                .format(serialNumber = self.serialNumber,
                                        startTime = fileTime,
                                        userName = self.userName)
        self.csvFileName    = gzmCfg.config(gzmCfg.CsvMakeTemplate) \
                                .format(serialNumber = self.serialNumber,
                                        startTime = fileTime,
                                        userName = self.userName)

    #----------------------------------------------------------

    def updateErrorCounts(self) :
        """Update the error counts.
        """
        if self.ioError :
            return
        #
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.CsvErrorCounts) \
            and not gzmCfg.config(gzmCfg.LogErrorCounts):
            return
        errorCounts = self.getErrorCounts(self.port)
        if errorCounts :
            self.errorCounts = errorCounts
        else:
            print("Error getting error counts")

    #----------------------------------------------------------

    def _moveFile(self, sourceFileName, destinationTemplate, moveToDest) :
        """Move a log file.

        Arguments:
            sourceFileName          Path/name of the source file.
            destinationTemplate     Template of destination path/name.
            moveToDest              Move (instead of copy) to destination.
        """
        if destinationTemplate :
            desinationFileName = destinationTemplate \
                                    .format(serialNumber = self.serialNumber,
                                            startTime = _fixTimeForFileName(self.startTime),
                                            userName = self.userName)
            if moveToDest :
                try:
                    shutil.move(sourceFileName, desinationFileName)
                except (IOError, OSError) as error :
                    self.reportFileError("Error moving '{}' to '{}'" \
                                            .format(sourceFileName, desinationFileName), error)
            else:
                try:
                    shutil.copy(sourceFileName, desinationFileName)
                except (IOError, OSError) as error :
                    self.reportFileError("Error copying '{}' to '{}'" \
                                            .format(sourceFileName, desinationFileName), error)

