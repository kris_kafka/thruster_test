import VrUtil
from VrCfg import *

#=======================================================================================================================

SubModuleName           = "VrThrCfg"

# TODO: Update version number.
SubModuleVersionMajor   = 1
SubModuleVersionMinor   = 5
SubModuleVersionMicro   = 5

#=======================================================================================================================

_defaultTests = """
#### Test control values defaults:

Duration              3600
Interval                60.0
PauseAfterTest           1.0
StopAfterTest          yes
TestStartupTime          0.500
FaultPeriod              3.000
KeepOutOfRange         yes
PollInterval             0.000

LogAverages            yes
LogBadCounts           yes
LogFirstPoll           yes
LogMaximums            yes
LogMinimums            yes

PidControlMax           100.0
PidControlMin          -100.0
PidDt                    0.5
PidIntegralMin      -20000.0
PidIntegralMax       20000.0
PidKd                    0.0
PidKi                    0.0
PidKp                    1.0
PidScale                 0.010
PidShowDebug            no
PidStartupDelay          0.000
SeekToTarget            no
; Target              None
; TargetLower         None
; TargetUpper         None

ValidFaultsErrorMask  0x00
ValidFaultsOkayValue  0x00
ValidCurrentMinimum      0.000
ValidCurrentMaximum      6.000
ValidRpmMinimum        100.0
ValidRpmMaximum      10000.0
ValidTempMinimum         5.0
ValidTempMaximum        75.0
ValidVoltageMinimum     35.0
ValidVoltageMaximum     50.0

SetDefaults

#### QA Tests

test
percentage               9.0
end

test
percentage              -9.0
end

test
percentage              60.0
end

test
percentage             -60.0
end

#### Configuration values

CsvDestTemplate     '/mnt/production/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest        no
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex        no
LogCsrHexAtEnd       no
LogCsrHexAtStart     no
LogDestTemplate     '/mnt/production/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/QA_LOGS/70503-Thruster/{serialNumber:s}_{startTime:s}.log'
CsvMoveToDest        no
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
PortName              ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration    no
ShowNonDefault      yes
ShowTests            no
SummaryLogFile      '/mnt/production/QA_LOGS/70503-Thruster/!thr_summary.txt'
UserName            ''
"""

# Use the module version information as the TestRegime value.
_defaultTests += "TestRegime '{major:d}.{minor:d}.{micro:d}'".format(major = SubModuleVersionMajor,
                                                                        minor = SubModuleVersionMinor,
                                                                        micro = SubModuleVersionMicro)

#=======================================================================================================================

# Test dictionary keys
Percentage          = "Percentage"
PidControlMax       = "PidControlMax"
PidControlMin       = "PidControlMin"
PidDt               = "PidDt"
PidIntegralMin      = "PidIntegralMin"
PidIntegralMax      = "PidIntegralMax"
PidKd               = "PidKd"
PidKi               = "PidKi"
PidKp               = "PidKp"
PidScale            = "PidScale"
PidShowDebug        = "PidShowDebug"
PidStartupDelay     = "PidStartupDelay"
SeekToTarget        = "SeekToTarget"
Target              = "Target"
TargetLower         = "TargetLower"
TargetUpper         = "TargetUpper"
ValidRpmMaximum     = "ValidRpmMaximum"
ValidRpmMinimum     = "ValidRpmMinimum"

#=======================================================================================================================

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(SubModuleName,
                                        __file__,
                                        SubModuleVersionMajor,
                                        SubModuleVersionMinor,
                                        SubModuleVersionMicro)

#=======================================================================================================================

class Options(VrOptions) :
    """Thruster test tool command line option processing.
    """

    #----------------------------------------------------------

    def __init__(self, appInf) :
        """Initialize a Options object.
        """
        super().__init__(appInf, __file__)

    #----------------------------------------------------------
    @staticmethod
    def getDefaultTests() :
        """Return the default tests text.
        """
        return _defaultTests

    #----------------------------------------------------------

    def getFormatedTest(self, number, test) :
        """Format a test for display.

        Return a list of lines.
        """
        gzmLog = self.appInf.GizmoLogModule
        text = [
                    "#{number:d}:" \
                        .format(number = number),
                    "    Percentage={Percentage:.1f}" \
                        .format(Percentage = test[Percentage]),
                    "    Duration={Duration:s}" \
                        .format(Duration = gzmLog.formatSecondsAsHhMmSs(test[Duration])),
                    "    FaultPeriod={FaultPeriod:.1f}" \
                        .format(FaultPeriod = test[FaultPeriod]),
                    "    Interval={Interval:s}" \
                        .format(Interval = gzmLog.formatSeconds(test[Interval], 1)),
                    "    KeepOutOfRange={KeepOutOfRange:1d}" \
                        .format(KeepOutOfRange = test[KeepOutOfRange]),
                    "    LogAverages={LogAverages:d}" \
                        .format(LogAverages = test[LogAverages]),
                    "    LogBadCounts={LogBadCounts:d}" \
                        .format(LogBadCounts = test[LogBadCounts]),
                    "    LogFirstPoll={LogFirstPoll:d}" \
                        .format(LogFirstPoll = test[LogFirstPoll]),
                    "    LogMaximums={LogMaximums:d}" \
                        .format(LogMaximums = test[LogMaximums]),
                    "    LogMinimums={LogMinimums:d}" \
                        .format(LogMinimums = test[LogMinimums]),
                    "    PauseAfterTest={PauseAfterTest:.1f}" \
                        .format(PauseAfterTest = test[PauseAfterTest]),
                ]
        if test[SeekToTarget] :
            text.extend([
                        "    PidControlMax={PidControlMax:.1f}" \
                            .format(PidControlMax = test[PidControlMax]),
                        "    PidControlMin={PidControlMin:.1f}" \
                            .format(PidControlMin = test[PidControlMin]),
                        "    PidDt={PidDt:.3f}" \
                            .format(PidDt = test[PidDt]),
                        "    PidIntegralMax={PidIntegralMax:.1f}" \
                            .format(PidIntegralMax = test[PidIntegralMax]),
                        "    PidIntegralMin={PidIntegralMin:.1f}" \
                            .format(PidIntegralMin = test[PidIntegralMin]),
                        "    PidKd={PidKd:.3f}" \
                            .format(PidKd = test[PidKd]),
                        "    PidKi={PidKi:.3f}" \
                            .format(PidKi = test[PidKi]),
                        "    PidKp={PidKp:.3f}" \
                            .format(PidKp = test[PidKp]),
                        "    PidScale={PidScale:.6f}" \
                            .format(PidScale = test[PidScale]),
                        "    PidShowDebug={PidShowDebug:d}" \
                            .format(PidShowDebug = test[PidShowDebug]),
                        "    PidStartupDelay={PidStartupDelay:.1f}" \
                            .format(PidStartupDelay = test[PidStartupDelay]),
                        ])
        text.extend([
                    "    PollInterval={PollInterval:s}" \
                        .format(PollInterval = gzmLog.formatSeconds(test[PollInterval], 3)),
                    "    StopAfterTest={Stop:s}" \
                        .format(Stop = "Yes" if test[StopAfterTest] else "No"),
                    ])
        if test[SeekToTarget] and test[Target] is not None :
            text.append("    Target={Target:.1f}"
                            .format(Target = test[Target]))
        if test[SeekToTarget] and test[TargetLower] is not None and test[TargetUpper] is not None :
            text.extend([
                            "    TargetLower={TargetLower:.1f}"
                                .format(TargetLower = test[TargetLower]),
                            "    TargetUpper={TargetUpper:.1f}"
                                .format(TargetUpper = test[TargetUpper]),
                        ])
        text.extend([
                    "    TestStartupTime={TestStartupTime:s}" \
                        .format(TestStartupTime = gzmLog.formatSeconds(test[TestStartupTime], 3)),
                    "    ValidCurrentMaximum={ValidCurrentMaximum:.3f}" \
                        .format(ValidCurrentMaximum = test[ValidCurrentMaximum]),
                    "    ValidCurrentMinimum={ValidCurrentMinimum:.3f}" \
                        .format(ValidCurrentMinimum = test[ValidCurrentMinimum]),
                    "    ValidFaultsErrorMask=0x{ValidFaultsErrorMask:02X}" \
                        .format(ValidFaultsErrorMask = test[ValidFaultsErrorMask]),
                    "    ValidFaultsOkayValue=0x{ValidFaultsOkayValue:02X}" \
                        .format(ValidFaultsOkayValue = test[ValidFaultsOkayValue]),
                    "    ValidRpmMaximum={ValidRpmMaximum:.1f}" \
                        .format(ValidRpmMaximum = test[ValidRpmMaximum]),
                    "    ValidRpmMinimum={ValidRpmMinimum:.1f}" \
                        .format(ValidRpmMinimum = test[ValidRpmMinimum]),
                    "    ValidTempMaximum={ValidTempMaximum:.1f}" \
                        .format(ValidTempMaximum = test[ValidTempMaximum]),
                    "    ValidTempMinimum={ValidTempMinimum:.1f}" \
                        .format(ValidTempMinimum = test[ValidTempMinimum]),
                    "    ValidVoltageMaximum={ValidVoltageMaximum:.1f}" \
                        .format(ValidVoltageMaximum = test[ValidVoltageMaximum]),
                    "    ValidVoltageMinimum={ValidVoltageMinimum:.1f}" \
                        .format(ValidVoltageMinimum = test[ValidVoltageMinimum]),
                    ])
        text.extend([""])
        return "\n".join(text)

#=======================================================================================================================

class Parser(VrParser) :
    """Thruster test tool configuration file parsing.
    """

    #----------------------------------------------------------

    def getFilePrefix(self) :
        """Return the CSV and log file name prefix.
        """
        return "thr_"

    #----------------------------------------------------------

    def makeSpecialIds(self) :
        """Define the thruster special IDs.
        """
        super().makeSpecialIds()
        #
        # Test values
        self.makeTestId(PidControlMax,       self.SidValSNum,   100.0)
        self.makeTestId(PidControlMin,       self.SidValSNum,   -100.0)
        self.makeTestId(PidDt,              self.SidValUNum,    0.500)
        self.makeTestId(PidIntegralMax,     self.SidValSNum,    20000.0)
        self.makeTestId(PidIntegralMin,     self.SidValSNum,    -20000.0)
        self.makeTestId(PidKd,              self.SidValUNum,    0.000)
        self.makeTestId(PidKi,              self.SidValUNum,    0.000)
        self.makeTestId(PidKp,              self.SidValUNum,    1.000)
        self.makeTestId(PidScale,           self.SidValSNum,    0.010)
        self.makeTestId(PidShowDebug,       self.SidValBoolean, False)
        self.makeTestId(PidStartupDelay,    self.SidValUNum,    0.0)
        self.makeTestId(Percentage,         self.SidValSNum,    10.0,       minLength = 3)
        self.makeTestId(SeekToTarget,       self.SidValBoolean, False)
        self.makeTestId(Target,             self.SidValSNum,    None)
        self.makeTestId(TargetLower,        self.SidValSNum,    None)
        self.makeTestId(TargetUpper,        self.SidValSNum,    None)
        self.makeTestId(ValidRpmMaximum,    self.SidValSNum,    10000.0,    minLength = 11)
        self.makeTestId(ValidRpmMinimum,    self.SidValSNum,    100.0,      minLength = 11)
