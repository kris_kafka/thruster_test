import VrUtil

#=======================================================================================================================

ModuleName          = "VrLog"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 4

#=======================================================================================================================

def formatModuleVersions(appInf, nameSize = None) :
    """Return the formatted version information.
    """
    text = [
                "Software module versions:",
            ]
    #
    modules = []
    modules.append(appInf.getModuleInformation())
    modules.append(appInf.AppModule.getModuleInformation())
    modules.append(appInf.CfgModule.getModuleInformation())
    modules.append(appInf.GizmoCfgModule.getModuleInformation())
    modules.append(appInf.GizmoModule.getModuleInformation())
    modules.append(appInf.GizmoLogModule.getModuleInformation())
    modules.append(appInf.GizmoTstModule.getModuleInformation())
    modules.append(appInf.LogModule.getModuleInformation())
    modules.append(appInf.TstModule.getModuleInformation())
    modules.append(appInf.VrModule.getModuleInformation())
    modules.append(appInf.VrUtilModule.getModuleInformation())
    if "VrPidModule" in dir(appInf) is not None :
        modules.append(appInf.VrPidModule.getModuleInformation())
    #
    minNameSize = 3 + max([len(module["Name"]) for module in modules ])
    if nameSize is None or nameSize < minNameSize :
        nameSize = minNameSize
    fmt = "{Name:<" + str(nameSize) + "s}" + \
        "{VersionMajor:d}.{VersionMinor:d}.{VersionMicro:d}" + \
        "  {LastModified:s}" + \
        "  {FileHash:s}" + \
        "  {FilePath:s}"
    for module in sorted(modules, key = lambda module : module["Name"]) :
        text.append(fmt.format(**module))
    #
    return "\n".join(text)

#----------------------------------------------------------

def formatSecondsAsHhMmSs(seconds) :
    """Format seconds in "HH:MM:SS" format.

    Arguments:
        seconds     Seconds to be formatted.
    """
    si  = int(seconds)
    s   = si % 60
    t   = int(si / 60)
    m   = t % 60
    h   = int(t / 60)
    return "{hh:>2d}:{mm:0>2d}:{ss:0>2d}".format(hh = h, mm = m, ss = s)

#----------------------------------------------------------

def formatSecondsAsMmSs(seconds) :
    """Format seconds in "MM:SS" format.

    Arguments:
        seconds     Seconds to be formatted.
        digits      Number of decimal digits.
    """
    si  = int(seconds)
    s   = si % 60
    m   = int(si / 60)
    return "{mm:>2d}:{ss:0>2d}".format(mm = m, ss = s)

#----------------------------------------------------------

def formatSeconds(seconds, digits = 0, forceHours = False, forceMinutes = False) :
    """Format seconds.

    Arguments:
        seconds         Seconds to be formatted.
        digits          Number of decimal digits.
        forceHours      Force inclusion of hours (and minutes).
        forceMinutes    Force inclusion of minutes.
    """
    s = seconds if digits else int(seconds)
    h = int(s / 3600)
    s -= 3600 * h
    m = int(s / 60)
    s -= 60 * m
    #
    if forceHours or h :
        fmt = "{hh:d}:{mm:02d}:"
    elif forceMinutes or m :
        fmt = "{mm:d}:"
    else:
        fmt = ""
    #
    if fmt :
        if digits :
            fmt += "{ss:0" + str(3 + digits) + "." + str(digits) + "f}"
        else:
            fmt += "{ss:02d}"
    else:
        if digits :
            fmt += "{ss:0" + str(digits + (3 if s >= 10.0 else 2)) + "." + str(digits) + "f}"
        else:
            fmt += "{ss:d}"
    return fmt.format(hh = h, mm = m, ss = s)

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#----------------------------------------------------------

def safeDivide(numerator, divisor) :
    """Return numerator / divisor.
    """
    return numerator / divisor if divisor and numerator else 0

#=======================================================================================================================

class VrLog :
    """Generic CSV and log file processing.
    """
    #----------------------------------------------------------

    def createAllFiles(self, gizmos, allTests) :
        """Create and initialize the log files.

        Arguments:
            gizmos      Gizmos.
            allTests    Tests info.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        doLog = gzmCfg.config(gzmCfg.LogGenerate)
        doCsv = gzmCfg.config(gzmCfg.CsvGenerate)
        #
        for gizmo in gizmos :
            if doLog :
                try:
                    gizmo.logFile = open(gizmo.logFileName, 'w')
                except (IOError, OSError) as error :
                    gizmo.reportFileError("Error opening '{}'".format(gizmo.logFileName), error)
                    exit(1)
                self.printLog(gizmo, self.makeLogFileHeader(gizmo, allTests))
            if doCsv :
                try:
                    gizmo.csvFile = open(gizmo.csvFileName, 'w')
                except (IOError, OSError) as error :
                    gizmo.reportFileError("Error opening '{}'".format(gizmo.csvFileName), error)
                self.printCsv(gizmo, self.makeCsvFileHeader(gizmo))

    #----------------------------------------------------------

    def dumpCsrForAll(self, gizmos, dumpHex, dumpData) :
        """Dump the contents of the CSR.

        Arguments:
            gizmos      Gizmos.
            dumpHex     Dump CSR in hex format?
            dumpData    Dump CSR in data format?
        """
        # Do nothing if not generating log files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.LogGenerate) :
            return
        #
        if not dumpHex and not dumpData :
            return
        for gizmo in gizmos :
            csr0 = gizmo.csrRead(gizmo.port, 0x00, 0x78)
            csr1 = gizmo.csrRead(gizmo.port, 0x78, 0x78)
            if csr0 is not None and csr1 is not None :
                csr = csr0.payloadData() + csr1.payloadData()
                if dumpHex :
                    self.printLog(gizmo, self.makeLogFileCsrHex(gizmo, csr))
                if dumpData :
                    self.printLog(gizmo, self.makeLogFileCsrData(gizmo, csr))

    #----------------------------------------------------------

    def finishAllLogFiles(self, gizmos, allTests, endTime) :
        """Terminate the log files.

        Arguments:
            gizmos      Gizmos.
            allTests    Tests info.
            endTime     Time when testing ended.
        """
        gzmCfg = self.appInf.GizmoCfgModule
        doLog = gzmCfg.config(gzmCfg.LogGenerate)
        doCsv = gzmCfg.config(gzmCfg.CsvGenerate)
        #
        for gizmo in gizmos :
            if doLog :
                self.printLog(gizmo, self.makeLogFileTrailer(gizmo, allTests, endTime))
                try:
                    gizmo.logFile.close()
                except (IOError, OSError) as error :
                    gizmo.reportFileError("Error closing '{}'".format(gizmo.logFileName), error)
            if doCsv :
                try:
                    gizmo.csvFile.close()
                except (IOError, OSError) as error :
                    gizmo.reportFileError("Error closing '{}'".format(gizmo.csvFileName), error)
            gizmo.moveLogsToFinalDestination()

    #----------------------------------------------------------

    def printCsv(self, gizmo, text) :
        """Write text to the CSV file.
            gizmo   Gizmo.
            text    Text to append.
        """
        try:
            print(text, end = '', file = gizmo.csvFile)
        except (IOError, OSError) as error :
            gizmo.reportFileError("Error writing to '{}'".format(gizmo.csvFileName), error)
            exit(1)

    #----------------------------------------------------------

    def printLog(self, gizmo, text) :
        """Write text to the log file.
            gizmo   Gizmo.
            text    Text to append.
        """
        try:
            print(text, end = '', file = gizmo.logFile)
        except (IOError, OSError) as error :
            gizmo.reportFileError("Error writing to '{}'".format(gizmo.logFileName), error)
            exit(1)

    #----------------------------------------------------------

    def recordDetailFor(self, gizmo, time, isStartup, reply, counts, testNumber, test) :
        """Record the polling status in the CSV file.
            gizmo       Gizmo.
            time        Time of the status reply.
            isStartup   True during test startup time period.
            reply       Propulsion command status reply.
            counts      Error counts.
            testNumber  Number [1, ...] of the test.
            test        Test info.
        """
        # Do nothing if not generating csv files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.CsvGenerate) :
            return
        #
        self.printCsv(gizmo, self.makeCsvFileRecord(gizmo, time, isStartup, reply, counts, testNumber, test))

    #----------------------------------------------------------

    def recordErrorCountsForAll(self, gizmos) :
        """Record the error counts in the log files.

        Arguments:
            gizmos      Gizmos.
        """
        # Do nothing if not generating log files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.LogGenerate) :
            return
        #
        for gizmo in gizmos :
            if gizmo.isFault :
                continue
            self.printLog(gizmo, self.makeLogFileErrorCount(gizmo))

    #----------------------------------------------------------

    def recordStatusForAll(self, gizmos, testTime, time) :
        """Record the status in the log files.

        Arguments:
            gizmos      Gizmos.
            testTime    Time when the test started.
            time        Test time.
        """
        # Do nothing if not generating log files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.LogGenerate) :
            return
        #
        for gizmo in gizmos :
            if gizmo.isFault or 0 == gizmo.pollCount :
                continue
            self.printLog(gizmo, self.makeLogFileStatus(gizmo, testTime, time))
            gizmo.resetInterval()

    #----------------------------------------------------------

    def testEndForAll(self, gizmos, testTime, testNumber, test) :
        """Start a new text in the log files.

        Arguments:
            gizmos      Gizmos.
            testTime    Time when the test started.
            testNumber  Number [1, ...] of the test.
            test        Test data.
        """
        # Do nothing if not generating log files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.LogGenerate) :
            return
        #
        for gizmo in gizmos :
            # Ignore faulted gizmos.
            if gizmo.isFaultAtStart :
                continue
            self.printLog(gizmo, self.makeLogFileTestTrailer(gizmo, testTime, testNumber, test))

    #----------------------------------------------------------

    def testStartForAll(self, gizmos, testTime, testNumber, test) :
        """Start a new text in the log files.

        Arguments:
            gizmos      Gizmos.
            testTime    Time when the test started.
            testNumber  Number [1, ...] of the test.
            test        Test data.
        """
        # Do nothing if not generating log files.
        gzmCfg = self.appInf.GizmoCfgModule
        if not gzmCfg.config(gzmCfg.LogGenerate) :
            return
        #
        for gizmo in gizmos :
            # Ignore faulted gizmos.
            if gizmo.isFaultAtStart :
                continue
            self.printLog(gizmo, self.makeLogFileTestHeader(gizmo, testTime, testNumber, test))
