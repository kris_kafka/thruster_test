vr_enum_all
===========
This tool enumerates all of the VideRay MSS type gizmos.

Usage
-----
The applications support the following command line options (in
order of precedence):

Option   Short  Parameter  Brief Description
-------  -----  ---------  ------------------------------------------
help       h               Show the usage message.
version                    Show the version information.
port       p    Port Name  Serial port.



vr_test_led and vr_test_thruster
================================
These tools performs tests of VideoRay MSS type LEDs and thrusters.

Usage
-----
The applications support the following command line options (in
order of precedence):

Option   Short  Parameter     Brief Description
-------  -----  ---------     ------------------------------------------
help       h                  Show the usage message.
version                       Show the version information.
port       p    Port Name     Serial port.
user       u    User Name     Define name of the user.
file       f    Control File  Read configuration and tests from file.

Control Files
-------------
See the led_sample.txt and thr_sample.txt files for information
about test files.



vr_make_summary
===============
This tool rebuilds the summary log file from the vr_test_led and
vr_test_thruster log files.

Usage
-----
The applications support the following command line options (in
order of precedence):

Option       Short  Parameter         Brief Description
-----------  -----  ------------      ----------------------------------------
help          h                       Show the usage message.
version                               Show the version information.
logPattern    l     Log File Pattern  Path/file pattern of source log files.
                                      The default is: *.log
summaryPath   p     Summary File      File/path of generated summary log file.
                                      The default is: summary.txt
sortBy        s     Field Name        Name of field used to sort the generated
                                      summary log file. The field name may be
                                      abbreviated.
                                      Valid values: FailCauses,
                                      FirmwareVersion, SerialNumber, StartTime,
                                      Status, TestHash, TestModified,
                                      or TestVersion.
                                      The default is: SerialNumber



Directory Structure
===================
The standard configuration for vr_test_thruster requires the following
directories and file exist:
    ~/QA_LOGS/70503-Thruster/
    /mnt/production/QA_LOGS/70503-Thruster/!thr_summary.txt

The thr_burn_in.txt configuration requires the following
directories and file exist:
    ~/QA_LOGS/70503-Thruster/Burn-in/
    /mnt/production/QA_LOGS/70503-Thruster/Burn-in/!thr_burn_in.txt

The standard configuration for vr_test_led requires the following
directories and file exist:
    ~/QA_Logs/70023-LED/
    /mnt/production/QA_LOGS/70023-LED/!led_summary.txt



Common Information
==================
Supported Environments
----------------------
This tool is supported in the following environments:
- Windows 7
- Windows 10
- Ubuntu 14.04 LTS
- Ubuntu 16.04 LTS

Know Issues / To Do
-------------------
1.  Define the tests.
2.  Define the minimum and maximum valid status values.
3.  Check firmware version support with updated firmware.
4.  Need code to verify CSR fields.
5.  Add support for HTML log files.



History
=======
1.5.5 (2018-04-26)
Added PidControlMin and PidControlMax configuration values to limit the
PID control value.

Corrected descriptions in sample configuration files.

Reworked test configuration output (ShowTests).

Include cause in initial failure messages.

1.5.4 (2018-04-25)
Changed so that the calculated PID percentage is recorded in the CSV
files; 2 decimal digits instead of 1 if SeekToTarget is true.

Added thr_burn_in_csv.txt to perform burn in tests while recording CSV
files.

1.5.3 (2018-04-10)
Correct problem in the low level gizmo I/O code which caused the CSR data
dump in the log files to be incorrect: CSR[0-119] had the contents of
CSR[120-239].

1.5.2 (2018-04-03)
Added support for environment variable expansion and home user (~ prefix)
expansion in string control values.

1.5.1 (2018-04-03)
Changed thruster burn-in control file to generate log and summary files.

1.5.0 (2018-04-02)
Added PID control to better support thruster burn-in.

1.4.2 (2018-03-16)
Changed initialization of error counts to be gizmo specific.

Changed thruster burn in power to be 3.5%.

1.4.1 (2018-03-16)
Added support to control if the log and summary log files are generated.

Added initial thruster burn in test configuration file that does not
generate/update any log or csv files.

Enhanced windows application batch (CMD) files to search for python and the
application files in standard locations.

1.4.0 (2018-03-14)
Added support for summary log files (SummaryLogFile) and created the
vr_make_summary tool.

Added support for testRegime configuration value. Added logging of test
regime and source module version information (version number, timestamp,
hash, and source file) to the generated log files. Display test regime
information before "Okay?" prompt.

Added support for CsvMoveToDest and LogMoveToDest configuration values to
determine if the generated log files (CsvMakeTemplate and LogMakeTemplate)
are copied or moved to the final destination (CsvDestTemplate and
LogDestTemplate).

Improved file and gizmo I/O error handling.

Changed handling of formatted CSR values that overflow their defined size.

Modified the defaults of several configuration values.

1.3.0 (2017-12-07)
Refactored the code to separate the gizmo specific code from the common code.
Fixed many copy/paste/edit errors.

Converted many global configuration values to be test specific.

Rewrote control file parser. No longer line oriented. Added test delimiter
commands, and the "stop" and "setDefault" commands.

The --version option now displays the version numbers of all VideoRay modules.

Removed restriction of whole seconds for all time values except test duration.

Added many new global configuration values (for example ShowConfiguration,
ShowNonDefault, and ShowTests).

1.0.1 (2017-11-16)
Renamed files to be more consistant for future test tools.

1.0.0 (2017-11-16)
Initial release.
