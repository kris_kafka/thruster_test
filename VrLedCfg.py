import VrLog
import VrUtil
from VrCfg import *

#=======================================================================================================================

SubModuleName           = "VrLedCfg"

# TODO: Update version number.
SubModuleVersionMajor   = 1
SubModuleVersionMinor   = 5
SubModuleVersionMicro   = 5

#=======================================================================================================================

_defaultTests = """
#### Default test control values:

Duration              3600
Interval                60.0
PauseAfterTest           1.0
StopAfterTest          yes
TestStartupTime          0.500
FaultPeriod              3.000
KeepOutOfRange         yes
PollInterval             0.000

LogAverages            yes
LogBadCounts           yes
LogMaximums            yes
LogMinimums            yes
LogFirstPoll           yes

ValidFaultsErrorMask  0x00
ValidFaultsOkayValue  0x00
ValidCurrentMaximum      1.000
ValidCurrentMinimum      0.000
ValidSetpointMaximum   100.0
ValidSetpointMinimum     0.0
ValidTempMaximum        75.0
ValidTempMinimum         5.0
ValidVoltageMaximum     50.0
ValidVoltageMinimum     45.0

SetDefaults

#### QA Tests

test
Setpoint                10.0
ValidSetpointMinimum     9.9
ValidSetpointMaximum    10.1
end

test
Setpoint                60.0
ValidSetpointMinimum    59.9
ValidSetpointMaximum    60.1
end

#### Configuration values

CsvDestTemplate     '/mnt/production/QA_LOGS/70023-LED/{serialNumber:s}_{startTime:s}.csv'
CsvErrorCounts      yes
CsvGenerate         yes
CsvMakeTemplate     '~/QA_Logs/70023-LED/{serialNumber:s}_{startTime:s}.csv'
CsvMoveToDest        no
LogCsrDataAtEnd      no
LogCsrDataAtStart   yes
LogCsrDataHex        no
LogCsrHexAtEnd       no
LogCsrHexAtStart     no
LogDestTemplate     '/mnt/production/QA_LOGS/70023-LED/{serialNumber:s}_{startTime:s}.log'
LogErrorCounts      yes
LogGenerate         yes
LogMakeTemplate     '~/QA_Logs/70023-LED/{serialNumber:s}_{startTime:s}.log'
LogMoveToDest        no
LogPollCounts       yes
LogRepeatInfoAtEnd   no
PauseAfterLastTest   no
PortName            ''
ResetGizmos          no
ResetPause            5.000
ShowConfiguration    no
ShowNonDefault      yes
ShowTests            no
SummaryLogFile      '/mnt/production/QA_LOGS/70023-LED/!led_summary.txt'
UserName            ''
"""

# Use the module version information as the TestRegime value.
_defaultTests += "TestRegime '{major:d}.{minor:d}.{micro:d}'".format(major = SubModuleVersionMajor,
                                                                        minor = SubModuleVersionMinor,
                                                                        micro = SubModuleVersionMicro)

#=======================================================================================================================

# Test dictionary keys
Setpoint                = "Setpoint"
Setpoint1               = "Setpoint1"
Setpoint2               = "Setpoint2"
Setpoint3               = "Setpoint3"
ValidSetpointMaximum    = "ValidSetpointMaximum"
ValidSetpointMinimum    = "ValidSetpointMinimum"

#=======================================================================================================================

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(SubModuleName,
                                        __file__,
                                        SubModuleVersionMajor,
                                        SubModuleVersionMinor,
                                        SubModuleVersionMicro)

#=======================================================================================================================

class Options(VrOptions) :
    """LED test tool command line option processing.
    """

    #----------------------------------------------------------

    def __init__(self, appInf) :
        """Initialize a Options object.
        """
        super().__init__(appInf, __file__)

    #----------------------------------------------------------
    @staticmethod
    def getDefaultTests() :
        """Return the default tests text.
        """
        return _defaultTests

    #----------------------------------------------------------

    def getFormatedTest(self, number, test) :
        """Format a test for display.

        Return a list of lines.
        """
        gzmLog = self.appInf.GizmoLogModule
        text = [
                    "#{number:d}:" \
                        .format(number = number),
                    "    Setpoints={Setpoint1:.1f}, {Setpoint2:.1f}, {Setpoint3:.1f}" \
                        .format(Setpoint1 = test[Setpoint1],
                                Setpoint2 = test[Setpoint2],
                                Setpoint3 = test[Setpoint3]),
                    "    Duration={Duration:s}" \
                        .format(Duration = gzmLog.formatSeconds(test[Duration])),
                    "    FaultPeriod={FaultPeriod:.1f}" \
                        .format(FaultPeriod = test[FaultPeriod]),
                    "    Interval={Interval:s}" \
                        .format(Interval = gzmLog.formatSeconds(test[Interval], 1)),
                    "    KeepOutOfRange={KeepOutOfRange:1d}" \
                        .format(KeepOutOfRange = test[KeepOutOfRange]),
                    "    LogAverages={LogAverages:d}" \
                        .format(LogAverages = test[LogAverages]),
                    "    LogBadCounts={LogBadCounts:d}" \
                        .format(LogBadCounts = test[LogBadCounts]),
                    "    LogFirstPoll={LogFirstPoll:d}" \
                        .format(LogFirstPoll = test[LogFirstPoll]),
                    "    LogMaximums={LogMaximums:d}" \
                        .format(LogMaximums = test[LogMaximums]),
                    "    LogMinimums={LogMinimums:d}" \
                        .format(LogMinimums = test[LogMinimums]),
                    "    PauseAfterTest={PauseAfterTest:.1f}" \
                        .format(PauseAfterTest = test[PauseAfterTest]),
                    "    PollInterval={PollInterval:s}" \
                        .format(PollInterval = gzmLog.formatSeconds(test[PollInterval], 3)),
                    "    StopAfterTest={Stop:s}" \
                        .format(Stop = "Yes" if test[StopAfterTest] else "No"),
                    "    TestStartupTime={TestStartupTime:s}" \
                        .format(TestStartupTime = gzmLog.formatSeconds(test[TestStartupTime], 3)),
                    "    ValidCurrentMaximum={ValidCurrentMaximum:.3f}" \
                        .format(ValidCurrentMaximum = test[ValidCurrentMaximum]),
                    "    ValidCurrentMinimum={ValidCurrentMinimum:.3f}" \
                        .format(ValidCurrentMinimum = test[ValidCurrentMinimum]),
                    "    ValidFaultsErrorMask=0x{ValidFaultsErrorMask:02X}" \
                        .format(ValidFaultsErrorMask = test[ValidFaultsErrorMask]),
                    "    ValidFaultsOkayValue=0x{ValidFaultsOkayValue:02X}" \
                        .format(ValidFaultsOkayValue = test[ValidFaultsOkayValue]),
                    "    ValidSetpointMaximum={ValidSetpointMaximum:.1f}" \
                        .format(ValidSetpointMaximum = test[ValidSetpointMaximum]),
                    "    ValidSetpointMinimum={ValidSetpointMinimum:.1f}" \
                        .format(ValidSetpointMinimum = test[ValidSetpointMinimum]),
                    "    ValidTempMaximum={ValidTempMaximum:.1f}" \
                        .format(ValidTempMaximum = test[ValidTempMaximum]),
                    "    ValidTempMinimum={ValidTempMinimum:.1f}" \
                        .format(ValidTempMinimum = test[ValidTempMinimum]),
                    "    ValidVoltageMaximum={ValidVoltageMaximum:.1f}" \
                        .format(ValidVoltageMaximum = test[ValidVoltageMaximum]),
                    "    ValidVoltageMinimum={ValidVoltageMinimum:.1f}" \
                        .format(ValidVoltageMinimum = test[ValidVoltageMinimum]),
                    ""
                ]
        return "\n".join(text)

#=======================================================================================================================

class Parser(VrParser) :
    """LED test tool configuration file parsing.
    """

    #----------------------------------------------------------

    def getFilePrefix(self) :
        """Return the CSV and log file name prefix.
        """
        return "led_"

    #----------------------------------------------------------

    def makeSpecialIds(self) :
        """Define the LED special IDs.
        """
        super().makeSpecialIds()
        #
        # Test values
        self.makeTestId(Setpoint,               self.SidValUNum,    10.0)
        self.makeTestId(Setpoint,               self.SidValUNum,    10.0,   minLength = 3, suffix = "1")
        self.makeTestId(Setpoint,               self.SidValUNum,    10.0,   minLength = 3, suffix = "2")
        self.makeTestId(Setpoint,               self.SidValUNum,    10.0,   minLength = 3, suffix = "3")
        self.makeTestId(ValidSetpointMaximum,   self.SidValUNum,    2.000,  minLength = 16)
        self.makeTestId(ValidSetpointMinimum,   self.SidValUNum,    0.010,  minLength = 16)

    #----------------------------------------------------------

    def setValue(self, data, name, value) :
        """Set a symbol's value.
        """
        if name == Setpoint :
            # Special processing for "Setpoint".
            data[Setpoint1] = value
            data[Setpoint2] = value
            data[Setpoint3] = value
        else:
            data[name] = value
        return data
