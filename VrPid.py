import VrUtil

#=======================================================================================================================

ModuleName          = "VrPid"

# TODO: Update version number.
ModuleVersionMajor  = 1
ModuleVersionMinor  = 5
ModuleVersionMicro  = 5

#=======================================================================================================================

class _Debug :
    """Debugging configuration parameters.
    """
    ShowGet             = False
    ShowLimited         = False
    ShowNone            = False
    ShowSet             = False
    ShowStart           = False
    ShowUpdatesDefault  = False
    ShowUpdatesForce    = False
    #
    MinWidth            = 10
    NameWidth           = 17

#=======================================================================================================================

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(ModuleName,
                                        __file__,
                                        ModuleVersionMajor,
                                        ModuleVersionMinor,
                                        ModuleVersionMicro)

#=======================================================================================================================

class VrPid :
    """PID implementation.
    """

    #----------------------------------------------------------

    def __init__(self, control, id = "") :
        """Initialize a VrPid object.

        Arguments:
            control   Initial control value.
        """
        self.controlMaximum     = None
        self.controlMinimum     = None
        self.dt                 = 0.0
        self.goal               = None
        self.isControlLimited   = False
        self.isNoRange          = True
        self.control            = control
        self.id                 = id
        self.integralMaximum    = None
        self.integralMinimum    = None
        self.isEnabled          = False
        self.isOkay             = False
        self.kd                 = 0.0
        self.ki                 = 0.0
        self.kp                 = None
        self.scale              = 1.0
        self.startupDelay       = 0.0
        self.target             = None
        self.targetLower        = None
        self.targetUpper        = None
        self.showDebug          = _Debug.ShowUpdatesDefault

    #----------------------------------------------------------

    def _show(self, prefix, name, decimals, fmtStr, value) :
        """Show a single PID status value.

        Arguments:
            prefix      Prefix text.
            name        Name of field.
            Decimals    Number of decimal digits or None.
            fmtStr      Format string.
            value       Value to format.
        """
        fp = "{}{:<" + str(_Debug.NameWidth) + "s}{:"
        fs = "}\n"
        if value is not None :
            vw = str(_Debug.MinWidth) if decimals is None \
                    else str(_Debug.MinWidth + 1 + decimals) + "." + str(decimals)
            VrUtil.message((fp + vw + fmtStr + fs).format(prefix, name, value))
        elif _Debug.ShowNone :
            VrUtil.message((fp + ">" + str(_Debug.MinWidth) + "s" + fs).format(prefix, name, "None"))

    #----------------------------------------------------------

    def errors(self) :
        """Check control values.

        Return error conditions as a list of strings.
        """
        answer = []
        if self.target is None and (self.targetLower is None or self.targetUpper is None) :
            answer.append("Target not set")
        if self.dt < 0.0 :
            answer.append("dt invalid")
        if self.kp <= 0.0 :
            answer.append("Kp invalid")
        if self.ki < 0.0 :
            answer.append("Ki invalid")
        if self.kd < 0.0 :
            answer.append("Kd invalid")
        if self.startupDelay < 0.0 :
            answer.append("Startup delay invalid")
        # Ensure limits are valid.
        if self.targetLower is not None and self.targetUpper is not None :
            if self.targetLower >= self.targetUpper :
                answer.append("Target limits invalid (target lower limit >= target upper limit)")
            elif self.target is not None :
                if self.target < self.targetLower :
                    answer.append("Target invalid (target < target lower limit)")
                if self.target > self.targetUpper :
                    answer.append("Target invalid (target > target upper limit)")
        return answer

    #----------------------------------------------------------

    def get(self) :
        """Get the control value.
        """
        if _Debug.ShowGet : VrUtil.message("PID get: {}\n".format(self.control))
        return self.control

    #----------------------------------------------------------

    def isLimited(self) :
        """Return if the control value is limited.
        """
        if _Debug.ShowLimited : VrUtil.message("PID isControlLimited: {}\n".format(self.isControlLimited))
        return self.isControlLimited

    #----------------------------------------------------------

    def set(self,
            control = None,
            controlMaximum = None,
            controlMinimum = None,
            dt = None,
            enabled = None,
            integralMaximum = None,
            integralMinimum = None,
            kp = None,
            ki = None,
            kd = None,
            scale = None,
            showDebug = None,
            startupDelay = None,
            target = None,
            targetLower = None,
            targetUpper = None) :
        """Set one or more control values.

        Arguments:
            control             Optional control control value.
            controlMaximum      Optional maximum control value.
            controlMinimum      Optional minimum control value.
            dt                  Optional minimum dt time in seconds.
            enable              Optional enable/disable status.
            integralMaximum     Optional maximum integral value.
            integralMinimum     Optional minimum integral value.
            kd                  Optional Kd (derivative) constant.
            ki                  Optional Ki (integral) constant.
            kp                  Optional Kp (error) constant.
            scale               Optional scaling constant.
            showDebug           Optional show update debugging information.
            startupDelay        Optional startup delay in seconds.
            target              Optional new target.
            targetLower         Optional new target lower limit.
            targetUpper         Optional new target upper limit.
        """
        if controlMaximum is not None :
            self.controlMaximum = controlMaximum
            if _Debug.ShowSet : VrUtil.message("PID set: controlMaximum={}\n".format(self.controlMaximum))
        if controlMinimum is not None :
            self.controlMinimum = controlMinimum
            if _Debug.ShowSet : VrUtil.message("PID set: controlMinimum={}\n".format(self.controlMinimum))
        if dt is not None :
            self.dt = dt
            if _Debug.ShowSet : VrUtil.message("PID set: dt={}\n".format(self.dt))
        if enabled is not None :
            self.isEnabled = enabled
            if _Debug.ShowSet : VrUtil.message("PID set: enabled={}\n".format(self.enabled))
        if control is not None :
            self.control = control
            if _Debug.ShowSet : VrUtil.message("PID set: control={}\n".format(self.control))
        if integralMaximum is not None :
            self.integralMaximum = integralMaximum
            if _Debug.ShowSet : VrUtil.message("PID set: integralMaximum={}\n".format(self.integralMaximum))
        if integralMinimum is not None :
            self.integralMinimum = integralMinimum
            if _Debug.ShowSet : VrUtil.message("PID set: integralMinimum={}\n".format(self.integralMinimum))
        if kd is not None :
            self.kd = kd
            if _Debug.ShowSet : VrUtil.message("PID set: kd={}\n".format(self.kd))
        if ki is not None :
            self.ki = ki
            if _Debug.ShowSet : VrUtil.message("PID set: ki={}\n".format(self.ki))
        if kp is not None :
            self.kp = kp
            if _Debug.ShowSet : VrUtil.message("PID set: kp={}\n".format(self.kp))
        if scale is not None :
            self.scale = scale
            if _Debug.ShowSet : VrUtil.message("PID set: scale={}\n".format(self.scale))
        if showDebug is not None :
            self.showDebug = showDebug or _Debug.ShowUpdatesForce
            if _Debug.ShowSet : VrUtil.message("PID set: showDebug={}\n".format(self.showDebug))
        if startupDelay is not None :
            self.startupDelay = startupDelay
            if _Debug.ShowSet : VrUtil.message("PID set: startupDelay={}\n".format(self.startupDelay))
        if target is not None :
            self.target = target
            if _Debug.ShowSet : VrUtil.message("PID set: target={}\n".format(self.target))
        if targetLower is not None :
            self.targetLower = targetLower
            if _Debug.ShowSet : VrUtil.message("PID set: targetLower={}\n".format(self.targetLower))
        if targetUpper is not None :
            self.targetUpper = targetUpper
            if _Debug.ShowSet : VrUtil.message("PID set: targetUpper={}\n".format(self.targetUpper))

    #----------------------------------------------------------

    def show(self, prefix = "PID show: ") :
        """Show the PID status.

        Arguments:
            prefix  Optional prefix text.
        """
        self._show(prefix, "isOkay",            None,   "d", self.isOkay)
        self._show(prefix, "isEnabled",         None,   "d", self.isEnabled)
        self._show(prefix, "isLimited",         None,   "d", self.isControlLimited)
        self._show(prefix, "isNoRange",         None,   "d", self.isNoRange)
        self._show(prefix, "inStartup",         None,   "d", self.inStartup)
        self._show(prefix, "controlMaximum",    1,      "f", self.controlMaximum)
        self._show(prefix, "controlMinimum",    1,      "f", self.controlMinimum)
        self._show(prefix, "dt",                3,      "f", self.dt)
        self._show(prefix, "goal",              1,      "f", self.goal)
        self._show(prefix, "control",           2,      "f", self.control)
        self._show(prefix, "integral",          1,      "f", self.integral)
        self._show(prefix, "integralMaximum",   1,      "f", self.integralMaximum)
        self._show(prefix, "integralMinimum",   1,      "f", self.integralMinimum)
        self._show(prefix, "lastError",         1,      "f", self.lastError)
        self._show(prefix, "kd",                12,     "f", self.kd)
        self._show(prefix, "ki",                9,      "f", self.ki)
        self._show(prefix, "kp",                6,      "f", self.kp)
        self._show(prefix, "scale",             6,      "f", self.scale)
        self._show(prefix, "startupDelay",      3,      "f", self.startupDelay)
        self._show(prefix, "target",            1,      "f", self.target)
        self._show(prefix, "targetLower",       1,      "f", self.targetLower)
        self._show(prefix, "targetUpper",       1,      "f", self.targetUpper)

    #----------------------------------------------------------

    def start(self) :
        """Start the PID processing.
        """
        answer = self.errors()
        self.isOkay = 0 == len(answer)
        if self.isOkay :
            self.isNoRange  = self.targetLower is None or self.targetUpper is None
            self.goal       = (self.targetLower + self.targetUpper) / 2.0 if self.target is None else self.target
            self.inStartup  = self.startupDelay > 0.0
            self.integral   = 0.0
            self.lastError  = None
            self.lastTime   = VrUtil.utcNow()
        if _Debug.ShowStart :
            self.show("PID start: ")
        return answer

    #----------------------------------------------------------

    def update(self, output, currentTime) :
        """Updated the control value.

        Arguments:
            output          Current output (i.e RPM) value.
            currentTime     Current time.
        """
        # Do nothing if not okay or not enabled.
        if not self.isOkay or not self.isEnabled :
            return
        timeDelta = (currentTime - self.lastTime).total_seconds()
        # Do nothing during startup delay.
        if self.inStartup :
            if timeDelta >= self.startupDelay :
                self.inStartup  = False
                self.lastTime   = currentTime
            return
        # Do nothing if not enough time since last update.
        if timeDelta < self.dt :
            return
        #
        if self.isNoRange or output < self.targetLower or self.targetUpper < output :
            curError    = self.goal - output
        else:
            curError    = 0.0
        #
        if self.lastError is None :
            derivative      = 0.0
        else:
            derivative      = (curError - self.lastError) / timeDelta
            self.integral   += curError * timeDelta
        # Limit the integral if requested.
        if self.integralMaximum is not None :
            self.integral = min(self.integralMaximum, self.integral)
        if self.integralMinimum is not None :
            self.integral = max(self.integralMinimum, self.integral)
        #
        self.control += self.scale * (self.kp * curError + self.ki * self.integral + self.kd * derivative)
        # Limit the result if requested.
        if self.controlMaximum is not None and self.control > self.controlMaximum :
            self.control = self.controlMaximum
            self.isControlLimited = True
        elif self.controlMinimum is not None and self.control < self.controlMinimum :
            self.control = self.controlMinimum
            self.isControlLimited = True
        else:
            self.isControlLimited = False
        #
        if self.showDebug :
            if self.lastError is None :
                VrUtil.message("PID {:s}     Output     Error    Time    Integral  Derivative     Control\n"
                                .format(self.id))
            VrUtil.message("PID {:s} {:10.1f}  {:+8.1f}  {:6.3f}  {:+10.3f}  {:+10.3f}  {:10.3f}\n"
                            .format(self.id, output, curError, timeDelta, self.integral, derivative, self.control))
        #
        self.lastTime   = currentTime
        self.lastError  = curError
