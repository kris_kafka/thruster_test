import struct
import Vr
import VrUtil

#=======================================================================================================================

GizmoModuleName         = "VrLed"

# TODO: Update version number.
GizmoModuleVersionMajor = 1
GizmoModuleVersionMinor = 4
GizmoModuleVersionMicro = 0

#=======================================================================================================================

class _Config :
    """LED configuration values.
    """
    # Note:
    #   Times values are in given in seconds.
    #   Temperature values are in given in degrees celsius.
    #
    # Bad temperature processing.
    KeepBadTemps        = False         # Keep invalid temperatures?
    BadTempReplacement  = float("nan")  # Replacement for invalid temperatures.
    Mcp9800MinTemp      = -55.0         # Minimum temperature from Microchip MCP9800 data sheet.
    Mcp9800MaxTemp      = 125.0         # Maximum temperature from  Microchip MCP9800 data sheet.
    Mcp9800Resolution   = 0.0625        # Temperature resolution from Microchip MCP9800 data sheet.
    #
    SetLevelRetry       = Vr.Gizmo.CsrWriteRetry    # Maximum number of attempts to perform a propulsion command.
    SetLevelTimeout     = Vr.Gizmo.CsrWriteTimeout  # Maximum time to wait for a reply to a set level command.
    StopRetry           = 5                         # Maximum number of times to attempt to stop a LED.

#=======================================================================================================================

def fromGizmo(gizmo) :
    """Return a VrLed.Gizmo created from a generic Vr.Gizmo
    """
    return Gizmo(gizmo.nodeId, gizmo.groupId, gizmo.serialNumber)

#----------------------------------------------------------

def getModuleInformation() :
    """Return the module information.
    """
    return VrUtil.makeModuleInformation(GizmoModuleName,
                                        __file__,
                                        GizmoModuleVersionMajor,
                                        GizmoModuleVersionMinor,
                                        GizmoModuleVersionMicro)

#=======================================================================================================================

def _checkTemp(value) :
    """Check if value is a valid temperature.
    """
    # Is the temperature something we can get from the Microchip MCP9800?
    return _Config.Mcp9800MinTemp <= value \
        and value <= _Config.Mcp9800MaxTemp \
        and 0 == value % _Config.Mcp9800Resolution

#=======================================================================================================================

class ReplyErrorCounts(Vr.Reply) :
    """Error counts reply.
    """
    CsrOffset           = 172
    CountsSize          = 4
    #
    UnderVoltageOffset  = 0 * CountsSize
    OverVoltageOffset   = 1 * CountsSize
    OverCurrentOffset   = 2 * CountsSize
    OverTempOffset      = 3 * CountsSize
    MinimumLength       = 4 * CountsSize

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return super().isPayloadDataValid() and self.length() >= (self.MinimumLength +  self.deviceTypeSize())

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data.
        """
        return self.uIntAt(self.UnderVoltageOffset, self.CountsSize), \
                self.uIntAt(self.OverVoltageOffset, self.CountsSize), \
                self.uIntAt(self.OverCurrentOffset, self.CountsSize), \
                self.uIntAt(self.OverTempOffset, self.CountsSize)

#=======================================================================================================================

class ReplySetLevel(Vr.Reply) :
    """VideoRay LED update reply information and functions.
    """
    # Status reply packet offsets.
    LevelCount      = 3
    TargetOffset    = 0
    TargetSize      = 4
    AllTargetSize   = LevelCount * TargetSize
    VoltageOffset   = AllTargetSize + TargetOffset
    VoltageSize     = 4
    CurrentOffset   = VoltageSize + VoltageOffset
    CurrentSize     = 4
    TempOffset      = CurrentSize + CurrentOffset
    TempSize        = 4
    FaultsOffset    = TempSize + TempOffset
    FaultsSize      = 4
    ActualOffset    = FaultsSize + FaultsOffset
    ActualSize      = 4
    AllActualSize   = LevelCount * ActualSize
    StatusSize      = AllActualSize + ActualOffset
    #
    ParseSize       = LevelCount + 4
    #
    # Reply indexes
    ReplyVoltage    = 0
    ReplyCurrent    = 1
    ReplyTemp       = 2
    ReplyFaults     = 3
    ReplyActuals    = 4

    #----------------------------------------------------------

    def isPayloadDataValid(self) :
        """Determine if the packet's payload data is valid.
        """
        return super().isPayloadDataValid() and self.length() >= (self.StatusSize +  self.deviceTypeSize())

    #----------------------------------------------------------

    def parse(self) :
        """Return the parsed reply packet data:
            Bus voltage, Bus current, Temperature, Faults flags,
            Setpoint 1 actual, Setpoint 2 actual, Setpoint 3 actual
        """
        temp = self.float4At(self.TempOffset)
        return self.float4At(self.VoltageOffset), \
                self.float4At(self.CurrentOffset), \
                temp if _Config.KeepBadTemps or _checkTemp(temp) else _Config.BadTempReplacement, \
                self.uInt1At(self.FaultsOffset), \
                100.0 * self.float4At(self.ActualOffset + 0 * self.ActualSize), \
                100.0 * self.float4At(self.ActualOffset + 1 * self.ActualSize), \
                100.0 * self.float4At(self.ActualOffset + 2 * self.ActualSize)

#=======================================================================================================================

class Gizmo(Vr.Gizmo) :
    """VideoRay LED gizmo information and functions.
    """
    #
    LedIdOffset     = 76
    LedIdSize       = 2
    # ID limits
    MinLedId        = 0
    MaxLedId        = 59
    #
    _FieldInfo  = \
    [
    # Offset Size   Unpack          Width   Decimal Prefix  Format  Valid,      Name
        (0,     4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_1_setpoint_target"),
        (4,     4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_2_setpoint_target"),
        (8,     4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_3_setpoint_target"),
        (12,    4,  Vr.Bytes.Real4, 9,      2,      "",     "f",    None,       "bus_v"),
        (16,    4,  Vr.Bytes.Real4, 9,      3,      " ",    "f",    None,       "bus_i"),
        (20,    4,  Vr.Bytes.Real4, 10,     1,      " ",    "f",    _checkTemp, "temp"),
        (24,    4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault"),
        (28,    4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_1_setpoint_actual"),
        (32,    4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_2_setpoint_actual"),
        (36,    4,  Vr.Bytes.Real4, 10,     7,      "",     "f",    None,       "LED_bank_3_setpoint_actual"),
        (40,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (48,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (56,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (64,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (72,    4,  None,           None,   None,   None,   None,   None,       "reserved"),
        (76,    2,  Vr.Bytes.UInt2, 5,      None,   "",     "d",    None,       "led_ID"),
        (78,    2,  None,           None,   None,   None,   None,   None,       "reserved"),
        (80,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "system_flags"),
        (81,    3,  None,           None,   None,   None,   None,   None,       "reserved"),
        (84,    4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault_interlock"),
        (88,    8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (96,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "control_flags"),
        (97,    1,  Vr.Bytes.UInt1, 2,      None,   "0",    "X",    None,       "led_bank_count"),
        (98,    2,  None,           None,   None,   None,   None,   None,       "reserved"),
        (100,   4,  Vr.Bytes.Real4, 9,      3,      "",     "f",    None,       "LED_bank_1_power_rating"),
        (104,   4,  Vr.Bytes.Real4, 9,      3,      "",     "f",    None,       "LED_bank_2_power_rating"),
        (108,   4,  Vr.Bytes.Real4, 9,      3,      "",     "f",    None,       "LED_bank_3_power_rating"),
        (112,   4,  Vr.Bytes.Real4, 9,      1,      "",     "f",    None,       "LED_bank_1_pwm_freq"),
        (116,   4,  Vr.Bytes.Real4, 9,      1,      " ",    "f",    None,       "RESERVED_LED_bank_2_pwm_freq"),
        (120,   4,  Vr.Bytes.Real4, 9,      1,      " ",    "f",    None,       "RESERVED_LED_bank_3_pwm_freq"),
        (124,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_1_setpoint_limit_min"),
        (125,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_2_setpoint_limit_min"),
        (126,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_3_setpoint_limit_min"),
        (127,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_1_setpoint_limit_max"),
        (128,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_2_setpoint_limit_max"),
        (129,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "LED_bank_3_setpoint_limit_max"),
        (130,   6,  None,           None,   None,   None,   None,   None,       "reserved"),
        (136,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (144,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (152,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (160,   4,  Vr.Bytes.UInt4, 8,      None,   "0",    "X",    None,       "fault_control"),
        (164,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "undervoltage_trigger"),
        (165,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "overvoltage_trigger"),
        (166,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "overcurrent_trigger"),
        (167,   1,  Vr.Bytes.UInt1, 3,      None,   "",     "d",    None,       "temp_trigger"),
        (168,   4,  None,           None,   None,   None,   None,   None,       "reserved"),
        (172,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "undervoltage_err_cnt"),
        (176,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "overvoltage_err_cnt"),
        (180,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "overcurrent_err_cnt"),
        (184,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "temp_err_cnt"),
        (188,   4,  None,           None,   None,   None,   None,   None,       "reserved"),
        (192,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (200,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (208,   8,  None,           None,   None,   None,   None,   None,       "reserved"),
        (216,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_sync1_err_cnt"),
        (220,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_sync2_err_cnt"),
        (224,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_headerxsum_err_cnt"),
        (228,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_overrun_err_cnt"),
        (232,   4,  Vr.Bytes.UInt4, 10,     None,   "",     "d",    None,       "comms_payloadxsum_err_cnt"),
        (236,   2,  Vr.Bytes.UInt2, 4,      None,   "0",    "X",    None,       "comms_err_flag"),
        (238,   2,  Vr.Bytes.UInt2, 4,      None,   "0",    "X",    None,       "save_settings"),
    ]

    #----------------------------------------------------------

    def __init__(self, nodeId = -1, groupId = -1, serialNumber = "", gizmoId = -1) :
        """Initialize a VRLed.Gizmo object.

        Arguments:
            nodeId          Optional node ID.
            groupId         Optional group ID
            serialNumber    Optional serial number.
            gizmoId         Optional LED ID.
        """
        self.gizmoId = gizmoId
        #
        super().__init__(nodeId, groupId, serialNumber, Vr.Devices.Led)

    #----------------------------------------------------------

    def checkIfOkay(self) :
        """Check if the gizmo information is okay.

        Returns list of reasons why the device is not okay.
        """
        answer = super().checkIfOkay()
        if self.gizmoId < self.MinLedId or self.MaxLedId < self.gizmoId :
            answer.append("LED ID")
        return answer

    #----------------------------------------------------------

    def getErrorCounts(self, port) :
        """Return the LED error counts or None.
        """
        reply = ReplyErrorCounts()
        reply = self.csrRead(port, reply.CsrOffset, reply.MinimumLength, reply = reply)
        return reply.parse() if reply else None

    #----------------------------------------------------------

    def getFieldInfo(self) :
        """Return the LED's CSR field info.
        """
        return self._FieldInfo

    #----------------------------------------------------------

    def getLedId(self, port) :
        """Get the LED ID from the device.

        Arguments:
            port    Serial port.

        Return True if no errors else False.
        """
        reply = self.csrRead(port, self.LedIdOffset, self.LedIdSize)
        if reply is None  :
            VrUtil.errorBeep("No reply getting LED ID from nodeId={nodeId:d}, serialNumber='{serialNumber:s}' " \
                                    .format(nodeId = self.nodeId, serialNumber = self.serialNumber))
            return False
        if reply.length() < reply.deviceTypeSize() + self.LedIdSize :
            VrUtil.errorBeep("Bad reply getting LED ID from nodeId={nodeId:d}, serialNumber='{serialNumber:s}'" \
                                    .format(nodeId = self.nodeId, serialNumber = self.serialNumber))
            return False
        self.gizmoId = reply.uIntAt(0, self.LedIdSize)
        return True

    #----------------------------------------------------------

    def setLevel(self, port, setpoint1, setpoint2 = None, setpoint3 = None) :
        """Set the target level of a LED.

        Arguments:
            port        Serial port.
            setpoint1   LED power setpoint #1.
            setpoint2   LED power setpoint #2.
            setpoint3   LED power setpoint #3.

        Returns ReplySetLevel reply packet if okay else None.
        """
        data    = struct.pack(Vr.Bytes.Real4, setpoint1 / 100.0) \
                + struct.pack(Vr.Bytes.Real4, (setpoint2 if setpoint2 else setpoint1) / 100.0) \
                + struct.pack(Vr.Bytes.Real4, (setpoint3 if setpoint3 else setpoint1) / 100.0)
        return self.csrWrite(port,
                                ReplySetLevel.TargetOffset,
                                ReplySetLevel.StatusSize,
                                data,
                                reply = ReplySetLevel())

    #----------------------------------------------------------

    def stopLed(self, port) :
        """Stop the LED (set the target level to zero).

        Arguments:
            port    Serial port.
        """
        retriesLeft = _Config.StopRetry
        while retriesLeft :
            if None is not self.setLevel(port, 0.0) :
                return
            retriesLeft -= 1
